<?php

/*
 * E-Model / R-Factor (Version 2008)
 *
 * copyright (c) 2006/2007 by x-fabric.com, Roman Puls
 * 
 * License:  The script is licensed to run free of charge on any ITU webserver.
 *           Distribution only allowed with written notice from x-fabric.com
 *
 * README:   This php script under php4 and php5, register_globals should
 *           should be disabled for the sake of security.
 *
 *			 The following javascript Libraries are needed in the same path:
 *           - wz_tooltip.js
 *           - tip_followscroll.js
 *           - tip_centerwindow.js
 * 
 * Changelog:
 * 2007-09-01 puls@x-fabric.com		initial release
 * 2007-09-04 puls@x-fabric.com		fixed log/log10 fun
 * 2007-09-19 puls@x-fabric.com		added T autocalc handling (javascript)
 * 									added tooltips
 * 									fixed MOS description
 * 2007-10-04 pomy					added BurstR
 * 2008-01-22 puls@x-fabric.com		changed font-size for IE parameter table
 *									changed popup delay to 400 ms (2000 is way too much)
 *									changed popup area to parameters column
 * 2008-05-29 pomy                  changed order of formulae for Idtes
 * 			  						changed name Submit to lowercase submit
 * 2008-06-01 pomy					inserted alternative code to replace
 * 									$_POST by $HTTP_POST_VARS to accommodate
 *									for php version 4.0.6 on ITU server
 *									runs still also under php5
 */

// default values for input paramters
$defaults = array(
	"Nc" 		=> -70,
	"Nfor"		=> -64,
	"Ps"		=> 35.0,
	"Pr"		=> 35.0,
	"SLR"		=> 8,
	"RLR"		=> 2,
	"STMR"		=> 15,
	"Dr"		=> 3,
	"LSTR"		=> 18,
	"Ds"		=> 3,
	"T"			=> 0,
	"Ta"		=> 0,
	"Tr"		=> 0,
	"TELR"		=> 65,
	"WEPL"		=> 110,
	"qdu"		=> 1.0,
	"Ie"		=> 0.0,
	"Bpl"		=> 1.0,
	"Ppl"		=> 0.0,
	"BurstR"    => 1.0,
	"A"			=> 0.0,
	"select_t"  => 0);


/* 
 * load defaults 
 * sets default values if POST does not include the parameter
 *
 * php version 4.0.6  runs currently on the ITU server
 * $_POST etc. was introduced from version 4.1.0 on to replace $HTTP_*_VARS
 * therefore different syntax is provided based on the php version returned by the server
 */
 
$version_critical = 410; // set this value to 410
$critical_0 = 4;
$critical_1 = 1;
$critical_2 = 0;

$version_dot = PHP_VERSION;
$version_array = explode(".", $version_dot);

/*
* this code does not rund under php 3 and below and it does not run on php above 5
*/
if ($version_array[0] <= 3)
   echo "This programme cannot be executed for versions below PHP4. 
   Current PHP version:" . phpversion().
   ". Please contact your webmaster."; 

if ($version_array[0] >= 6)
   echo "This programme cannot be executed for versions above PHP5.
   Current PHP version: ' . phpversion().
   Please contact your webmaster";  
 
 
/*
* 
*/ 
if (
$version_array[0] >= $critical_0
and
$version_array[1] >= $critical_1
and
$version_array[2] >= $critical_2
) {
       $warnings = "";
       foreach ($defaults as $key => $value) {
       	if (!isset($_POST["reset"]) && (isset($_POST[$key]))) {
       		if (!is_numeric($_POST[$key])) {
       			$$key = $defaults[$key];
       			$warnings .= "parameter <b>$key</b> is not a numeric value ('$_POST[$key]'), setting default ($defaults[$key])<br>";
       		} else {
       			$$key = $_POST[$key];
       		}
       	} else {
       		$$key = $defaults[$key];
       	}
       }
 
} else {
      $warnings = "";
      foreach ($defaults as $key => $value) {
      	if (!isset($HTTP_POST_VARS["reset"]) && (isset($HTTP_POST_VARS[$key]))) {
      		if (!is_numeric($HTTP_POST_VARS[$key])) {
      			$$key = $defaults[$key];
      			$warnings .= "parameter <b>$key</b> is not a numeric value ('$HTTP_POST_VARS[$key]'), setting default ($defaults[$key])<br>";
      		} else {
      			$$key = $HTTP_POST_VARS[$key];
      		}
      	} else {
      		$$key = $defaults[$key];
      	}
      }
}

/*
 * Pre-calculations
 */
 
// we do calculate LSTR everytime, regardless of client javascript 
$LSTR = $Dr + $STMR;

// we calculate Delay values regardless of client javascript
if ($select_t == 0) {
	$Ta = $T;
	$Tr = 2 * $T;
	$t_tag = "disabled ";
} else {
	$t_tag = " ";
}

/* *************************************************************************************
 * R-Factor calculus
 */

/*
 * Eq 
 *
 * Basic signal-to-noise ratio, Ro    
 */

// Remark: re-ordered the calculation compared to excel

$OLR = $SLR + $RLR;
$Nfo = $Nfor+$RLR;
$Nos = $Ps-$SLR-$Ds-100+0.004*pow(($Ps-$OLR-$Ds-14),2);
$Pre = $Pr+10*log10(1+pow(10,((10-$LSTR)/10)) )/log10(10);
$Nor = $RLR-121+$Pre+0.008*pow($Pre-35,2);
$No = 10*log10(pow(10,($Nc/10))+pow(10,($Nos/10))+pow(10,($Nor/10))+pow(10,($Nfo/10)));
$Ro = 15-(1.5*($SLR+$No));

// Simultaneous impairment factor, Is

if ($qdu < 1) {
  $Q = 37-15*log10(1)/log10(10);
} else {
  $Q = 37-15*log10($qdu)/log10(10);
}
$G = 1.07+0.258*$Q+0.0602*pow($Q,2);
$Z = 46/30-$G/40;
$Y = ($Ro-100)/15+46/8.4-$G/9;
$Iq = 15 * log10(1+pow(10,$Y)+ pow(10,$Z));
$Xolr = $OLR+0.2*(64+$No-$RLR);
$Iolr = 20*( pow(1+pow($Xolr/8,8), 1/8) - $Xolr/8 );
$STMRo = -10*log10(pow(10,-$STMR/10) + exp(-$T/4) * pow(10,-$TELR/10));
$Ist = 12*pow(1+pow(($STMRo-13)/6,8),1/8)-28*pow(1+pow(($STMRo+1)/19.4,35), 1/35)-13*pow(1+pow(($STMRo-3)/33,13),1/13)+29;
$Iq = 15*log10(1+pow(10,$Y)+pow(10,$Z));
$Is = $Iolr + $Ist + $Iq;

/* Eq #
 *
 * Delay impairment factor, Id   
 */

$Rle = 10.5*($WEPL+7)*pow($Tr+1,-0.25);
if ($Ta == 0) {
   $X = 0;
} else {
   $X = (log10($Ta/100))/(log10(2));
};
if ($Ta <= 100) {
   $Idd = 0;
} else {
   $Idd = 25*(pow(1+pow($X,6), 1/6) - 3 * pow(1+pow($X/3,6), 1/6)+2);
}
$Idle = ($Ro-$Rle)/2 + sqrt((pow($Ro-$Rle,2))/4+169);
$TERV = $TELR-40*log10((1+$T/10)/(1+$T/150))+6*exp(-0.3*pow($T,2));
$TERVs = $TERV + ($Ist/2);
$Roe = -1.5*($No-$RLR);
if ($STMR < 9) {
   $Re = 80+2.5*($TERVs-14);
} else {
   $Re = 80+2.5*($TERV-14);
};
if ($T < 1) {
   $Idte = 0;
} else {
   $Idte = (($Roe-$Re)/2+sqrt(pow($Roe-$Re,2)/4+100)-1)*(1-exp(-$T));
};
if ($STMR > 20) {
   $Idtes = sqrt((pow($Idte,2))+(pow($Ist,2)));
   $Id = $Idtes+$Idle+$Idd;
} else {
  $Id = $Idte+$Idle+$Idd;
}



/* 
 * Equipment impairment factor, Ie
 */
$Ieeff = $Ie+(95-$Ie)*($Ppl/($Ppl/$BurstR+$Bpl));

/*
 * Results 
 */
$R_ = $Ro-$Is-$Id-$Ieeff+$A;

if ($R_ > 100) {
	$MOS = 4.5;
} else {
	if ($R_ < 0) {
		$MOS = 1;
	} else {
		$MOS = 1 + $R_*0.035 + $R_*($R_-60) * (100-$R_)*7*pow(10,-6);
	}
}
/* 
 * R-Factor calculus ends here, now HTML 
 * *************************************************************************************/
?>
<html>
<head>
<meta name="Author" content="Roman Puls, www.x-fabric.com" />
<meta name="Copyright" content="&copy; x-fabric.com / Roman Puls" />
<meta name="Revisit-after" content="14" />
<meta name="Keywords" content="E-Model, R-factor, rfactor, calculation, formula, G.107">
<meta name="Description" content="E-Model Version 2006 calculation of the R-Factor / MOS value" />
<meta name="Audience" content="All" />

<style type="text/css">
<!--
.emodel_text {
	font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}

stable {
	font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}

.inp {
	font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif ;
	font-size: 12px;

	width: 50px;
}
.lightblue {
	color: #0066CC;
}
.style1 {
	color: #FFFFFF;
	font-weight: bold;
}
.style2 {color: #0066CC; font-weight: bold; }
.style5 {font-size: 8px}
.style6 {font-size: small}
.style14 {font-size: 10px; font-weight: bold; }
.hidden {visibility:hidden; display:none; }
-->
</style>
<script type="text/JavaScript">
<!--
function change_lstr(form) {
	form.LStr.value=eval(form.STMR.value) + eval(form.Dr.value);
	form.LStr.style.backgroundColor = "#dadada";
}
function change_t(form) {
	if(form.select_t.value==0) {
		// disable inputs
		form.Ta.disabled = true;		
		form.Tr.disabled = true;
		form.Tr.value=eval(form.T.value)*2;
		form.Ta.value=eval(form.T.value);
		form.Tr.style.backgroundColor = "#dadada";
		form.Ta.style.backgroundColor = "#dadada";
	} else {
		// enable inputs
		form.Ta.disabled = false;
		form.Tr.disabled = false;
		form.Tr.style.backgroundColor = "#ffffff";
		form.Ta.style.backgroundColor = "#ffffff";
	}
	//alert("T changed");
}
//-->
</script>
</head>
<body>
<script type="text/javascript" src="wz_tooltip.js"></script>

<!-- 

Remark: next dozen spans are invisbile and used as tooltip for input parameters 

//-->

<span class="hidden" id="A">
<div style="width:400px">
  <p><strong><em>Advantage Factor</em></strong> </p>
  <p> The "advantage factor A" represents an "advantage of access",
   introduced into transmission planning for the first time via the E-Model.
   This factor enables the planner to take into account the fact that users
   may accept some decrease in quality for access advantage, e.g. mobility
   or connections into hard-to-reach regions.<br>
    </p>
  <p>Provisional maximum values of Advantage Factor :</p>
  <ul>
    <li>Conventional (wirebound): 0</li>
    <li>Mobility by cellular networks in a building: 5</li>
    <li>Mobility in a geographical area or moving in a vehicle: 10</li>
    <li>Access to hard-to-reach locations, e.g. via multi-hop satellite connections: 20 <br>
    </li>
  </ul>
</div>
</span> <span class="hidden" id="Bpl">
<div style="width:400px"> <strong>Packet Loss Robustness Factor</strong>
  <p>Bpl is the robustness of a codec to random packet loss. 
  Tabulated values to be found in ITU-T Recommendation G.113.</p>
</div>
</span> <span class="hidden" id="Nc">
<div style="width:400px"> <strong>Circuit Noise</strong>
  <p>Sources for circuit noise historically were associated with analogue networks using 
  FDM systems and with certain switching systems. Only in special cases, e.g. interference 
  to analogue cable sections by power lines or other noise sources, should noise be part of the planning calculation.</p>
  <br>
  </p>
</div>
</span> <span class="hidden" id="Ds">
<div style="width:400px"> <strong>D-Factor, Send Side</strong>
  <p>The D-factor is the difference in sensitivity (in dB)
   for the handset microphone between direct (voice) sounds and diffuse (room) sounds.
   D= STMR � LSTR. </p>
</div>
</span> <span class="hidden" id="Nfor">
<div style="width:400px"> <strong>Noise Floor</strong>
  <p>This value shall not be modified.<br>
    </p>
</div>
</span> <span class="hidden" id="Ps">
<div style="width:400px"> <strong>Room Noise, Send Side </strong>
  <p>The ambient noise at the talker's side.<br>
    </p>
</div>
</span> <span class="hidden" id="Pr">
<div style="width:400px"> <strong>Room Noise, Receive Side</strong>
  <p>The ambient noise at the listener's side.<br>
  </p>
</div>
</span> <span class="hidden" id="SLR">
<div style="width:400px"> <strong>Send Loudness Rating </strong>
  <p>Send Loudness Rating (for the E-model) is the Loudness Rating between the the mouthpiece of the talker's handset
  and the 0 dBr point (middle of the network) of the connection.</p>
</div>
</span> <span class="hidden" id="RLR">
<div style="width:400px"> <strong>Receive Loudness Rating </strong>
  <p>Receive Loudness Rating (for the E-model) is the Loudness Rating between the 0 dBr point (middle of the network) of the connection 
  and the the earpiece of the listener's handset.</p>
</div>
</span> <span class="hidden" id="STMR">
<div style="width:400px"> <strong>Side Tone Masking Rating</strong>
  <p>The loudness of a telephone sidetone path compared with
  the loudness of the Intermediate Reference System (IRS) overall
  in which the comparison is made incorporating the speech signal
   heard via the human sidetone path as a masking threshold.</p>
</div>
</span> <span class="hidden" id="Dr">
<div style="width:400px"> <strong>D-Factor, Receive Side </strong>
  <p>The D-factor is the difference in sensitivity (in dB)
   for the handset microphone between direct (voice) sounds and diffuse (room) sounds.
   D= STMR � LSTR. </p>
</div>
</span> <span class="hidden" id="LStr">
<div style="width:400px"> <strong>Listener Side Tone masking Rating </strong>
  <p>The loudness loss between a (diffuse) room noise source 
  and the earpiece of the handset (at the listener�s side) via the electric sidetone path.<br>
  </p>
</div>
</span> <span class="hidden" id="T">
<div style="width:400px"> <strong>Mean One-Way Delay </strong>
  <p>T is the mean (average of send & receive direction) one-way transmission time
  along the echo path, i.e. from the talker to the reflection point ("echo source").<br>
  </p>
</div>
</span> <span class="hidden" id="Ta">
<div style="width:400px"> <strong>Absolute Delay (Mean One-Way)</strong>
  <p>Ta is the mean (average of send & receive direction) end-to-end one-way transmission time,
  i.e from the talker's mouth to the listener's ear.<br>
   </p>
</div>
</span> <span class="hidden" id="Tr">
<div style="width:400px"> <strong>Round-Trip Delay </strong>
  <p>Tr is the round-trip transmission time along the whole echo path.
  </p>
</div>
</span> <span class="hidden" id="TELR">
<div style="width:400px"> <strong>Talker Echo Loudness Rating </strong>
  <p>Talker Echo Loudness Rating is the the loudness loss between
  the talker's mouth and his ear via the echo path.<br>
   Note, that in the E-model this refers to the listener's side.<br>
  </p>
</div>
</span> <span class="hidden" id="WEPL">
<div style="width:400px"> <strong>Weighted Echo Path Loss </strong>
  <p>WEPL is weighted mean value of listener echo loss,
   i.e. the level difference between the talker's direct voice signal and the listener echo.
    </p>
</div>
</span>
<span class="hidden" id="qdu">
<div style="width:400px"> <strong>Quantizing Distortion Unit </strong>
  <p>Analogue/Digital and Digital/Analogue conversions involve a distortion of the signal, perceived as a quantization noise.
    1 qdu is defined as the quantization noise resulting from a complete A/D encoding and D/A decoding following the 8 bit A-law (or mu-law), according to ITU-T Recommendation G.711.<br>
    Note, that the qdu concept is no longer used for coders other than G.711. <br>
  </p>
</div>
</span>
<span class="hidden" id="Ppl">
<div style="width:400px"> <strong>Packet-loss Probability </strong>
  <p>Probability of the occurence of packet loss. <br>
  </p>
</div>
</span>
<span class="hidden" id="BurstR">
<div style="width:400px"> <strong>Burst Ratio </strong>
  <p>BurstR is the so-called Burst Ratio, which is defined as:<br>
     <br>
	 BurstR = Av_observed / Av_expected<br>
	 <br>
	 where:<br>
	 Av_observed= Average lenght of observed bursts in an arrival sequence<br>
	 Av_expected= Average length of bursts expected for the network under "random" loss<br>
	 <br>
	   When packet loss is random (i.e., independent) BurstR = 1; and<br>
	   when packet loss is bursty (i.e., dependent) BurstR > 1. <br>
  </p>
</div>
</span> 

<span class="hidden" id="Ie">
<div style="width:400px"> 
<strong>Equipment Impairment Factor </strong>
<p>A scalar number allocated to a specific type of impairment (codec),
 indicating the anticipated incremental value of impairment
 (decrease of the transmission rating factor R)
 resulting from the type of impairment (codec). 
 Tabulated values to be found in ITU-T Recommendation G.113.
</p>
<p> Examples of Equipment Impairment Factor : </p>

  <p>
  
  <table border="1" class="emodel_text">
    <tbody>
      <tr>
        <td>Equipment/Codec type</td>
        <td>Ie</td>
      </tr>
      <tr>
        <td>ADPCM 32 kbps</td>
        <td>7</td>
      </tr>
      <tr>
        <td>ADPCM 16 kbps</td>
        <td>50</td>
      </tr>
      <tr>
        <td>LD-CELP 16 kbps(G.728)</td>
        <td>7</td>
      </tr>
      <tr>
        <td>CS-ACELP 8 kbps(G.729)</td>
        <td>10</td>
      </tr>
      <tr>
        <td>GSM 6.10 13 kbps, Full rate</td>
        <td>20</td>
      </tr>
    </tbody>
  </table>
  </p>
</div>
</span> 

<span class="hidden" id="R">
<div style="width:400px"> <strong>Transmission Rating Factor
</strong>
  <p>R is the result of the E-model, 
  estimating users' satisfaction for transmission planning purposes.
</p>
</div>
</span> 

<span class="hidden" id="MOS-CQEn">
<div style="width:400px"> <strong>Mean Opinion Score - Communication Quality Estimated (Narrowband)</strong>
  <p>An estimate of the result of a subjective test. Higher values indicate better quality.<br>
</p>
</div>
</span> 

<span class="hidden" id="submit">
<div style="width:400px"> <strong>Calculate button</strong>
  <p>Press this button to calculate the R-Factor with the above settings.</p>
</div>
</span> 

<span class="hidden" id="reset">
<div style="width:400px"> <strong>Reset button</strong>
  <p>Press this button set all values back to default values.</p>
</div>
</span> 

<span class="hidden" id="delay">
<div style="width:400px"> 
<strong>Delay Calculation</strong>
  <p>Select one mode from the list.</p>
  <ul>
  <li>T=Ta=Tr/2: let the software calculate delay values for Ta and Tr
  <li>T separate: enter each value independent of T
  </ul>
</div>
</span> 

<span class="hidden" id="xfabric">
<div style="width:400px"> 
<strong>x-fabric.com</strong>
  <p>Services for the network industry [software, consulting, testing]. Click to check</p>
</div>
</span> 

<div name="vspacer" style="height:20px">&nbsp;</div>
<form id="form" name="form" method="post" action="" >
  <table align="center" border="0" cellpadding="0" cellspacing="2" class="emodel_text" id="inputs" style="border-color: #dadada; border-style:solid; border-width: 1px">
    <col width="276" />
    <col width="45" />
    <col width="88" />
    <col width="39" />
    <col width="47" />
    <tr height="17" style="border-bottom:thick; border-bottom-color: #000066;">
      <td height="25" colspan="5" bgcolor="#000099"><span class="style1" style="font-family: Verdana, Arial, Helvetica, sans-serif, Tahoma;">&nbsp;E-Model (Version March 2005)</span></td>
    </tr>
    <?php if ($warnings != "") : ?>
    <tr height="17">
      <td height="17" colspan="5"><div style="border-width:5px; border-color:red; border-style:solid; background-color:#fafafa"> <b>Warnings</b>
          <blockquote><small> <?php echo $warnings; ?> </small> </blockquote>
        </div></td>
    </tr>
    <?php endif; ?>
    <tr height="17" style="border-bottom:thick; border-bottom-color: #000066;">
      <td height="17" bgcolor="#dadada"><span class="style14">Parameter</span></td>
      <td bgcolor="#dadada"><span class="style14">ID</span></td>
      <td bgcolor="#dadada"><span class="style14">Default </span></td>
      <td bgcolor="#dadada"><span class="style14">Value</span></td>
      <td bgcolor="#dadada"><span class="style14">Dimension</span></td>
    </tr>
    <tr height="17">
      <td height="17">Electric Circuit Noise</td>
      <td class="lightblue" onMouseOver="TagToTip('Nc')">Nc</td>
      <td>(-70)</td>
      <td><input name="Nc" type="text" class="inp" value="<?php echo $Nc ?>" />      </td>
      <td>dBm0p</td>
    </tr>
    <tr height="17">
      <td height="17">Noise Floor</td>
      <td class="lightblue" onMouseOver="TagToTip('Nfor')">Nfor</td>
      <td>(-64)</td>
      <td><input name="Nfor" type="text" class="inp" id="Nfor" value="<?php echo $Nfor ?>" />      </td>
      <td>dBmp</td>
    </tr>
    <tr height="17" >
      <td height="17">Room Noise (Send)</td>
      <td class="lightblue" onMouseOver="TagToTip('Ps')">Ps</td>
      <td>(35)</td>
      <td><input name="Ps" type="text" class="inp" id="Ps" value="<?php echo $Ps ?>" />      </td>
      <td>dB(A)</td>
    </tr>
    <tr height="17">
      <td height="17">Room Noise (Receive)</td>
      <td class="lightblue" onMouseOver="TagToTip('Pr')">Pr</td>
      <td>(35)</td>
      <td><input name="Pr" type="text" class="inp" id="Pr" value="<?php echo $Pr ?>" />      </td>
      <td>dB(A)</td>
    </tr>
    <tr height="17">
      <td height="17">Send Loudness Rating</td>
      <td class="lightblue" onMouseOver="TagToTip('SLR')">SLR</td>
      <td>(8)</td>
      <td><input name="SLR" type="text" class="inp" id="SLR" value="<?php echo $SLR ?>" />      </td>
      <td>dB</td>
    </tr>
    <tr height="17">
      <td height="17">Receive Loudness Rating</td>
      <td class="lightblue" onMouseOver="TagToTip('RLR')">RLR</td>
      <td>(2)</td>
      <td><input name="RLR" type="text" class="inp" id="RLR" value="<?php echo $RLR ?>" />      </td>
      <td>dB</td>
    </tr>
    <tr height="17">
      <td height="17">Sidetone Masking Rating</td>
      <td class="lightblue" onMouseOver="TagToTip('STMR')">STMR</td>
      <td>(15)</td>
      <td><input name="STMR" type="text" class="inp" id="STMR" onChange="change_lstr(this.form)" value="<?php echo $STMR ?>" />      </td>
      <td>dB</td>
    </tr>
    <tr height="17">
      <td height="17">D-factor (Receive)</td>
      <td class="lightblue" onMouseOver="TagToTip('Dr')">Dr</td>
      <td>(3)</td>
      <td><input name="Dr" type="text" class="inp" id="Dr" onChange="change_lstr(this.form)" value="<?php echo $Dr ?>" />      </td>
      <td>&nbsp;</td>
    </tr>
    <tr height="17">
      <td height="17">Listener's Sidetone Rating</td>
      <td class="lightblue" onMouseOver="TagToTip('LStr')">LSTR</td>
      <td>STMR+Dr</td>
      <td><input name="LStr" type="text" disabled="disabled" class="inp" id="LStr" value="<?php echo $LSTR ?>" />      </td>
      <td>dB</td>
    </tr>
    <tr height="17">
      <td height="17">D-factor (Send)</td>
      <td class="lightblue" onMouseOver="TagToTip('Ds')">Ds</td>
      <td>(3)</td>
      <td><input name="Ds" type="text" class="inp" id="Ds" value="<?php echo $Ds ?>" />      </td>
      <td>&nbsp;</td>
    </tr>
    <tr height="17">
      <td height="17">Mean One-Way Delay</td>
      <td class="lightblue" onMouseOver="TagToTip('T')">T</td>
      <td>(0)</td>
      <td><input name="T" type="text" class="inp" id="T" onChange="change_t(this.form)" value="<?php echo $T; ?>" />      </td>
      <td>ms</td>
    </tr>
    <tr height="17">
      <td height="17">Absolute Delay from (S) to (R)&nbsp;</td>
      <td class="lightblue" onMouseOver="TagToTip('Ta')">Ta</td>
      <td>(=T)</td>
      <td><input name="Ta" type="text" class="inp" id="Ta" <?php echo $t_tag; ?> value="<?php echo $Ta ?>" />      </td>
      <td>ms</td>
    </tr>
    <tr height="17">
      <td height="17">Round-Trip Delay</td>
      <td class="lightblue" onMouseOver="TagToTip('Tr')">Tr</td>
      <td>(=2T)</td>
      <td><input name="Tr" type="text" class="inp" <?php echo $t_tag; ?> id="Tr" value="<?php echo $Tr ?>" />      </td>
      <td>ms</td>
    </tr>
    <tr height="17">
      <td height="17">Talker Echo Loudness Rating</td>
      <td class="lightblue" onMouseOver="TagToTip('TELR')">TELR</td>
      <td>(65)</td>
      <td><input name="TELR" type="text" class="inp" id="TELR" value="<?php echo $TELR ?>" />      </td>
      <td>dB</td>
    </tr>
    <tr height="17">
      <td height="17">Weighted Echo Path Loss</td>
      <td class="lightblue" onMouseOver="TagToTip('WEPL')">WEPL</td>
      <td>(110)</td>
      <td><input name="WEPL" type="text" class="inp" id="WEPL" value="<?php echo $WEPL ?>" />      </td>
      <td>dB</td>
    </tr>
    <tr height="17">
      <td height="17">Quantizing Distortion Units</td>
      <td class="lightblue" onMouseOver="TagToTip('qdu')">qdu</td>
      <td>(1)</td>
      <td><input name="qdu" type="text" class="inp" id="qdu" value="<?php echo $qdu ?>" />      </td>
      <td>&nbsp;</td>
    </tr>
    <tr height="17">
      <td height="17">Equipment Impairment Factor</td>
      <td class="lightblue" onMouseOver="TagToTip('Ie')">Ie</td>
      <td>(0)</td>
      <td><input name="Ie" type="text" class="inp" id="Ie" value="<?php echo $Ie ?>" />      </td>
      <td>&nbsp;</td>
    </tr>
    <tr height="17">
      <td height="17">Packet-loss Robustness Factor</td>
      <td class="lightblue" onMouseOver="TagToTip('Bpl')">Bpl</td>
      <td>(1)</td>
      <td><input name="Bpl" type="text" class="inp" id="Bpl" value="<?php echo $Bpl ?>" />      </td>
      <td>&nbsp;</td>
    </tr>
    <tr height="17">
      <td height="17">Packet-loss Probability</td>
      <td class="lightblue" onMouseOver="TagToTip('Ppl')">Ppl</td>
      <td>(0)</td>
      <td><input name="Ppl" type="text" class="inp" id="Ppl" value="<?php echo $Ppl ?>" />      </td>
      <td>%</td>
       </tr>
    <tr height="17">
	  <td height="17">Burst Ratio</td>
      <td class="lightblue" onMouseOver="TagToTip('BurstR')">BurstR</td>
      <td>(1)</td>
      <td><input name="BurstR" type="text" class="inp" id="BurstR" value="<?php echo $BurstR ?>" />      </td>
      <td>&nbsp;</td>
	  </tr>
    <tr height="17">
      <td height="17">Advantage Factor</td>
      <td class="lightblue" onMouseOver="TagToTip('A')">A</td>
      <td>(0)</td>
      <td><input name="A" type="text" class="inp" id="A" value="<?php echo $A ?>" />      </td>
      <td>&nbsp;</td>
    </tr>
    <tr height="17">
      <td height="17" colspan="5" bgcolor="#000066"><span class="style1">Results</span></td>
    </tr>
    <?php 
		/* background color for $ */
		if ($R_ >= 80) {
			$r_color = "lightgreen";
		} else {
			if ($R_ > 70) {
				$r_color = "yellow";
			} else {
				$r_color = "red";
			}
		}
	?>
    <tr height="17">
      <td height="17">Calculated R-Factor</td>
      <td class="style2" onMouseOver="TagToTip('R')">R</td>
      <td bgcolor="<?php echo $r_color; ?>"><b>
        <?php 
			printf("%0.1f", "$R_");
		?>
        &nbsp;</b> </td>
      <td colspan="2" align="right"><div align="left">
          <input style="width:90px; background-color:#dadada; font-weight:bold" name="submit" type="submit" value="calculate"  onMouseOver="TagToTip('submit')"/>
        </div></td>
    </tr>
    <tr height="17" >
      <td height="17">Mean Opinion Score</td>
      <td class="style2" onMouseOver="TagToTip('MOS-CQEn')">MOS<span class="style5">CQEn</span></td>
      <td><b>
        <?php 
			printf("%0.2f", "$MOS");
		?>
        </b></td>
      <td colspan="2"><input name="reset" type="submit" id="reset" style="width:90px; background-color:#dadada;" value="reset"  onMouseOver="TagToTip('reset')" /></td>
    </tr>
    <tr height="17">
      <td height="17">&nbsp;</td>
      <td colspan="2" class="style2">Options</td>
      <td colspan="2"><span class="style2">
        <select name="select_t" id="select_t" style="width:90px" onChange="change_t(this.form)"  onMouseOver="TagToTip('delay')">
          <option value="0" <?php if ($select_t) echo "selected "; ?>>T=Ta=Tr/2</option>
          <option value="1" <?php if ($select_t) echo "selected "; ?>>T separate</option>
        </select>
        </span></td>
    </tr>
    <tr height="17">
      <td height="17" colspan="5" bgcolor="#dadada" align="center"><small> This E-Model web front-end was brought to you by <a
	  href="http://x-fabric.com" target="_blank" onMouseOver="TagToTip('xfabric')">x-fabric.com</a> </small> </td>
    </tr>
  </table>
  <!-- enable/disable fields as required on load //-->
  <script type="text/JavaScript">
  	<!--
		change_t(this.form);
		change_lstr(this.form);		
	//-->
	</script>
</form>
<p class="emodel_text">&nbsp;</p>
</body>
</html>
<!-- EOF //-->