<?php  if (! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    /* --------------------------------------------------------------
     * VARIABLES
     * ------------------------------------------------------------ */

    /**
     * This is the list of messages to be displayed on the current view
     */

	public $messages = "";

/*******************************************************************************/
/**
 * Constructor
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	function __construct()
	{
		parent::__construct();

		$template = $this->session->userdata('template') ? $this->session->userdata('template') : $this->config->item('template');
		$this->twiggy->set('template', array(
				'name' 		=> $template,
				'template_path' 	=> "/application/themes/$template/_layouts/",
				'project_path'	=> '/',
				'css_path' => "/res/$template/css",
				'image_path' => "/res/$template/img/"
				), true);
				
		//  This is for JQuery
// JQuery is manually added in the header
//		$this->load->library('javascript');  
//		$this->javascript->external();
//		$this->javascript->compile();
		
//		$this->twiggy->set('library_src', $library_src);
//		$this->twiggy->set('script_foot', $script_foot);
				
				
	if (strlen($this->session->flashdata('messages')) > 0):
		$this->messages = $this->session->flashdata('messages');
	endif;			
	}
	
/*******************************************************************************/
/**
 * Sets up the menu and sub menu on each page
 *
 * @access	public
 * @param	string
 * @return	string
 */	
protected function set_menus($menu_key, $menu_sub_key) {
	$this->load->model('menu_model');
	$this->menu_model->order_by('sort_order');
	$rows = $this->menu_model->get_many_by('id_menu_group', $this->session->userdata('id_menu_group') ? $this->session->userdata('id_menu_group') : 1);
	$this->twiggy->set('menu', $rows, true);

	$this->load->model('menu_sub_model');
	$this->menu_sub_model->order_by('sort_order');
	$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
	$this->twiggy->set('submenu', $rows);

	$this->twiggy->set('menu_current', $menu_key);
	$this->twiggy->set('submenu_current', $menu_sub_key);
		
	if (strlen($this->messages) > 0):
		$this->twiggy->set('messages',  $this->messages );
	endif;
	
}

/*******************************************************************************/
/**
 * Determines if a variable is a guid.
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function is_guid($guid){
	if (preg_match('/^\{?[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}\}?$/i', $guid)) {
	  return true;
	} else {
	  return false;
	}
}



/*******************************************************************************/
/**
 * Determines if a variable is a mac.  Each Mac is stored in HelloPBX without delimiters
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function is_mac($mac){
	if (preg_match('/^\{?[A-F0-9]{12}\}?$/i', $mac)) {
	  return true;
	} else {
	  return false;
	}
}


/*******************************************************************************/
/**
 * Determines if a variable is a NANPA Phone number.  Each phone number is stored in HelloPBX without delimiters
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function is_nanpa($phone){
	if (preg_match('/^(\+1|1)?([2-9]\d\d[2-9]\d{6})$/', $phone)) {
	  return true;
	} else {
	  return false;
	}
}


/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function format_mac($mac){
	return substr($mac, 0,2) . ":" . substr($mac, 2,2) . ":" . substr($mac, 4,2) . ":" . substr($mac, 6,2) . ":" . substr($mac, 8,2) . ":" . substr($mac, 10,2) ;
}

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function format_nanpa($number){
	return "+" . substr($number,0,1) . " (" . substr($number, 1,3) . ") " . substr($number, 4,3) . "-" . substr($number, 7,4) ;
}

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function clean_nanpa($number){
	$number = trim($number);
	$number = str_replace("(", "", $number);
	$number = str_replace(")", "", $number);
	$number = str_replace("-", "", $number);
	$number = str_replace(" ", "", $number);	
	
	
	if (preg_match('/^(\+1|1)([2-9]\d\d[2-9]\d{6})$/', $number)) :
	  return $number;
	elseif (preg_match('/^([2-9]\d\d[2-9]\d{6})$/', $number)) :
	  return "1" . $number;
	else :
	  return false;
	endif;

}

/*******************************************************************************/
/**
 * Add a message to the session message queue for display
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function persist_messages(){
	
	
	$this->session->set_flashdata('messages',  $this->messages);
}


/*******************************************************************************/
/**
 * Add a message to the session message queue for display
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function add_message($message){
	$this->messages = $this->messages . "<li>" . $message . "</li>";
}

}
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */