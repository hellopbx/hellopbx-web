<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provision extends MY_Controller {

    function __construct()
    {
        parent::__construct();
	}

	public function index($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$this->summary($sort_by, $sort_order, $offset);
		
	}
	public function summary($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$title = "Provisioning Overview";
		$current_url = "provision/summary";
		$menu_key = "provision/summary";	// Identifies the main menu to be shown
		
		$this->load->model('provision_sip_model');

		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$limit = 20;

		$this->provision_sip_model->limit($limit, $offset);
		$this->provision_sip_model->order_by($sort_by, $sort_order);
		$rows = $this->provision_sip_model->get_all();
		$num_rows = $this->provision_sip_model->count_all();
		$this->twiggy->set('rows', $rows);
			
		//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	
		$this->twiggy->set('current_url', '/'.$current_url . '/');
		
		$this->load->library('pagination');
		$config = array();
		$config['base_url'] = "/$current_url/$sort_by/$sort_order";
		$config['total_rows'] = $num_rows;  
		$config['per_page'] = $limit;
		$config['uri_segment'] = 5; 
		$this->pagination->initialize($config); 
		$pagination = $this->pagination->create_links();
		if(strlen($pagination) > 0)	{$this->twiggy->set('pagination', $pagination);	}
		
		$this->twiggy->set('fields', array(
			'country' 		=> 'Country',
			'alpha2' 	=> 'Abbr',
			'numeric'	=> 'Numeric'
			));

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->template($current_url)->display();
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */