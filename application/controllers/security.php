	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Security extends MY_Controller {

	public function index($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$this->summary($sort_by, $sort_order, $offset);
		
	}
	public function summary($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$title = "Security and Fraud Protection Overview";
		$current_url = "security/summary";
		$menu_key = "security/summary";	// Identifies the main menu to be shown
		
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$limit = 20;

		$this->twiggy->set('current_url', '/'.$current_url . '/');
		
		$this->twiggy->set('fields', array(
			'country' 		=> 'Country',
			'alpha2' 	=> 'Abbr',
			'numeric'	=> 'Numeric'
			));

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */