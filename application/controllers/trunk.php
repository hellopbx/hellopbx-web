<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trunk extends MY_Controller {

    /**
     * Initialise the model, tie into the CodeIgniter superobject and
     * try our best to guess the table name.
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('trunk_sip_model');
    }
	
	
/*******************************************************************************/
/**
 * Display a default screen - list
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function index($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$this->summary($sort_by, $sort_order, $offset);
	}

/*******************************************************************************/
/**
 * Display a List of Trunk screen
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function summary($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$title = "SIP Trunk Overview";
		$page_explanation = "Trunks are gateways between HelloPBX and the public telephone network (PSTN), FXO gateways, or public services like Google Voice.";
		$current_url = "trunk/summary";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		$this->set_menus($menu_key, $current_url); 

		
		$this->load->model('trunk_sip_template_model');
		$template_list = $this->trunk_sip_template_model->dropdown("name");
		$template_list_keys = $this->trunk_sip_template_model->dropdown("view_key");
		$this->twiggy->set("template_list", $template_list);
		
		$rows = $this->trunk_sip_model->get_paged($sort_by, $sort_order, $offset);
		$num_rows = $this->trunk_sip_model->count_all();
		
		foreach ($rows as &$row):
			$row->id_template_name = $template_list[$row->id_template];
			$row->id_template_view_key = $template_list_keys[$row->id_template];
		endforeach;
		$this->twiggy->set('rows', $rows);
		
		$columns = array(
			'id_template' 		=> 'Template',
			'gateway' 			=> 'Trunk',
			'description' 		=> 'Description'
			);
			
		$this->twiggy->set('columns', $columns);

		//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	

		$this->load->library('pagination');
		$config = array();
		$config['base_url'] = "/$current_url/$sort_by/$sort_order";
		$config['total_rows'] = $num_rows;  
		$config['per_page'] = $this->trunk_sip_model->limit;
		$config['uri_segment'] = 5; 
		$this->pagination->initialize($config); 
	
		$pagination = $this->pagination->create_links();
		if(strlen($pagination) > 0)	:
			$this->twiggy->set('pagination', $pagination);	
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->twiggy->set('current_url', '/'.$current_url . '/');
		$this->twiggy->set("add_url", "/trunk/sip/add/");

		$this->twiggy->template($current_url)->display();
	}


/*******************************************************************************/
/**
 * Display a list of gateways filtered to SIP only
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function summary_sip($sort_by = 'id', $sort_order='asc', $offset = 0) {
	$success_message = "Delete Successful";

//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				foreach ($_POST as $name => $value):
					if(substr($name,0,4) == "del_"):
						$delete_id = substr($name, 4);

						if ($this->trunk_sip_model->delete($delete_id)):
							$this->add_message($success_message );
						else:
							$fail_insert = "Unsuccessful: " . validation_errors();
							echo $fail_insert;
							$this->add_message($fail_insert);
						endif;
					endif;
				endforeach;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
			// Data was not posted - first time to the screen and no update necessary
		endif;		
//  ******************* END Commit DB Changes ****************************

		$title = "SIP Trunk Overview";
		$page_explanation = "Trunks are gateways between HelloPBX and the public telephone network (PSTN), FXO gateways, or public services like Google Voice.";
		$current_url = "trunk/sip";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		$this->set_menus($menu_key, $current_url); 
		
		$this->load->model('trunk_sip_template_model');
		$template_list = $this->trunk_sip_template_model->dropdown("name");
		$template_list_keys = $this->trunk_sip_template_model->dropdown("view_key");
		$this->twiggy->set("template_list", $template_list);
		
		$rows = $this->trunk_sip_model->get_paged($sort_by, $sort_order, $offset);
		$num_rows = $this->trunk_sip_model->count_all();
		
		foreach ($rows as &$row):
			$row->id_template_name = $template_list[$row->id_template];
			$row->id_template_view_key = $template_list_keys[$row->id_template];
		endforeach;
		$this->twiggy->set('rows', $rows);
		
		$columns = array(
			'id_template' 		=> 'Template',
			'gateway' 			=> 'Trunk',
			'description' 		=> 'Description'
			);
			
		$this->twiggy->set('columns', $columns);

		//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	

		$this->load->library('pagination');
		$config = array();
		$config['base_url'] = "/$current_url/$sort_by/$sort_order";
		$config['total_rows'] = $num_rows;  
		$config['per_page'] = $this->trunk_sip_model->limit;
		$config['uri_segment'] = 5; 
		$this->pagination->initialize($config); 
	
		$pagination = $this->pagination->create_links();
		if(strlen($pagination) > 0)	:
			$this->twiggy->set('pagination', $pagination);	
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->twiggy->set('current_url', '/'.$current_url . '/');
		$this->twiggy->set("add_url", "/trunk/sip/add/");

		$this->twiggy->template($current_url)->display();
	}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function import() {

		$title = "Trunk Import";
		$page_explanation = 'Import trunk settings using text files.  Text files are in FreeSWITCH format.  You have the option of pasting the settings text or opening a file that contains the settings. For more information on FreeSWITCH format, see <a href="http://wiki.freeswitch.org/wiki/Clarification:gateways" target="_blank">FreeSWITCH: Gateways</a>';
		$current_url = "trunk/import";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
	
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}

/*******************************************************************************/
/**
 * Provides a page where the user can select a trunk template when adding a trunk
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 public function choose_provider($trunk_type="sip") {
		$title = "SIP Trunk Add: Choose Provider";
		$page_explanation = "Below is a list of various SIP providers.  Choosing a provider on the list ensure you're using a tested provider and the fields are specific for what is provided by your provider";
		$current_url = "trunk/choose_provider";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		
		$this->load->model('trunk_sip_template_model');
		$rows = $this->trunk_sip_template_model->get_available();
		$this->twiggy->set('rows', $rows);
				
		$this->twiggy->set('add_url', "/trunk/sip/add");
		$this->twiggy->set('back_url', "/trunk/" . $trunk_type);

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}

/*******************************************************************************/
/**
 * direct SIP trunk forms
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 	public function sip($view_type="view", $id=null, $form_template=null) {
		switch ($view_type) {
		case "edit":	
			if (!isset($id)){
				$this->summary_sip();
				return;
				} 
			elseif (!$this->is_guid($id)){
				$this->summary_sip();
				return;
				}
			else {
				$this->sip_edit($id, $form_template);
				return;
			}
			break;
		case "add":
			if (!isset($id)){
				$this->choose_provider("sip");
				return;
				} 
			elseif ($this->is_guid($id)){
				$this->sip_view($id);
				return;
				}
			else {
				$this->sip_add($id); // Id parameter actually hold form_template because id parameter is excluded for add.
				return;
			}
			break;
		default:
			if (!isset($id)){
				$this->summary_sip();
				return;
				} 
			elseif (!$this->is_guid($id)){
				$this->summary_sip();
				return;
				}
			else {
				$this->sip_view($id, $form_template);
				return;
			}
			break;	
		}
	}


/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	public function sip_view($id=null, $type=null) {
		$title = "Trunk: Advanced";
		$page_explanation = "";
		$current_url = "trunk/sip";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		
		$twig_view = $current_url;
		if (!isset($id)):
			// Same as summary
			$title = "Trunk: SIP Overview";
			$twig_view = "trunk/sip"; 
		elseif (!$this->is_guid($id)):
			// value supplied is not a guid
			$title = "Trunk: SIP Overview";
			$twig_view = "trunk/sip"; 
		else :
			$this->load->model('trunk_sip_template_model');
			$sip_template_list = $this->trunk_sip_template_model->get_by_key($type);
			if (count($sip_template_list) > 0):
			else:
				$sip_template_list = $this->trunk_sip_template_model->get_by_key("advanced");		//default to advanced
			endif;
	
			// Template header information is here
			$title = "View Trunk: " . $sip_template_list[0]->name;
			$twig_view = "trunk/sip_cru";
			$page_explanation = $sip_template_list[0]->description;
			$id_template = $sip_template_list[0]->id;
				
			$form_value_list = $this->trunk_sip_model->get_form_items($id);
			
			$this->load->model('trunk_sip_template_item_model');
			$this->trunk_sip_template_item_model->get_form_data($id_template, $form_value_list);
	
			$this->twiggy->set("fvl", $form_value_list);	// for update, create date
			$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
	
			$this->twiggy->set("edit_url", "/"."trunk/sip/edit/".$id."/".$type);
			$this->twiggy->set("back_url", "/".	"trunk/sip");
			$this->twiggy->set("view_type", "view");
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}


/*******************************************************************************/
/**
 * Display a Add Trunk screen
 *
 * @access	public
 * @param	string
 * @return	string
  */	
  public function sip_add($type=null) {
		$title = "Trunk Add: Advanced";
		$page_explanation = "We recommend you use a trunk template for adding a trunk or gateway.  If your provider is not listed, feel free to submit a feature request to have your provider added.";
		$current_url = "trunk/sip";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		$success_message = "SIP Trunk has been added";  // Best to extend this to include the trunk name that has been added.


//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				if ($this->trunk_sip_model->insert_advanced()):
					$this->add_message($success_message);
					redirect('/trunk/sip/');
				else:
					$fail_insert = "Unsuccessful: " . validation_errors();
					echo $fail_insert;
					$this->add_message($fail_insert);
				endif;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		endif;
//  ******************* END Commit DB Changes ****************************
		
		$twig_view = "trunk/sip_cru";
		$this->load->model('trunk_sip_template_model');
		$sip_template_list = $this->trunk_sip_template_model->get_by_key($type);

		$id_template = 1;
		if (count($sip_template_list) > 0):
			$title = "Add Trunk: " . $sip_template_list[0]->name;
			$id_template = $sip_template_list[0]->id;
			$page_explanation = $sip_template_list[0]->description;
		else :
			$title = "Add Trunk: Choose Provider";
			$twig_view = "trunk/choose_provider"; 
		endif;
			
		$form_value_list = $this->trunk_sip_model->get_form_items();
		$this->load->model('trunk_sip_template_item_model');
		$this->trunk_sip_template_item_model->get_form_data($id_template, $form_value_list);


		$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
		$this->twiggy->set("back_url", "/".	"trunk/sip");
		$this->twiggy->set("view_type", "add");

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}


/*******************************************************************************/
/**
 * Display a Trunk edit screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 public function sip_edit($id=null, $type=null) {
		$title = "Trunk: Advanced";
		$page_explanation = "";
		$current_url = "trunk/sip";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		$success_message = "SIP Trunk has been updated successfully.";  // Best to extend this to include the trunk name that has been added.

//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				if ($this->trunk_sip_model->update_advanced($id)):
					$this->add_message($success_message );
					redirect('/trunk/sip/');
				else:
					$fail_insert = "Unsuccessful: " . validation_errors();
					echo $fail_insert;
					$this->add_message($fail_insert);
				endif;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
			// Data was not posted - first time to the screen and no update necessary
		endif;		
//  ******************* END Commit DB Changes ****************************
		
		$twig_view = $current_url;
		if (!isset($id)):
			// Same as summary
			$title = "Trunk: SIP Overview";
			$twig_view = "trunk/sip"; 
		elseif (!$this->is_guid($id)):
			// value supplied is not a guid
			$title = "Trunk: SIP Overview";
			$twig_view = "trunk/sip"; 
		else :
			$twig_view = "trunk/sip_cru";
			$this->load->model('trunk_sip_template_model');
			$sip_template_list = $this->trunk_sip_template_model->get_by_key($type);
			if (count($sip_template_list) > 0):
			else:
				$sip_template_list = $this->trunk_sip_template_model->get_by_key("advanced");		//default to advanced
			endif;
	
			$title = "Edit Trunk: " . $sip_template_list[0]->name;
			$id_template = $sip_template_list[0]->id;
			$page_explanation = $sip_template_list[0]->description;
				
			$form_value_list = $this->trunk_sip_model->get_form_items($id);
			
			$this->load->model('trunk_sip_template_item_model');
			$this->trunk_sip_template_item_model->get_form_data($id_template, $form_value_list);
			$this->twiggy->set("fvl", $form_value_list);	// for update, create date
			
			$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
			if ($id_template != 1): 
				$this->twiggy->set("advanced_url", "/"."trunk/sip/edit/".$id."/advanced/");		
			endif;
			$this->twiggy->set("back_url", "/".	"trunk/sip");
			$this->twiggy->set("view_type", "edit");
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	
	}


/*******************************************************************************/
/**
 * Display a list of gateways filtered to XMPP only
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function xmpp($sort_by = 'id', $sort_order='asc', $offset = 0) {

		$title = "XMPP Trunk Overview";
		$page_explanation = "";
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$this->load->model('trunk_sip_model');
		$current_url = "trunk/xmpp";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}


/*******************************************************************************/
/**
 * Display a list of gateways filtered to XMPP only
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function googlevoice($sort_by = 'id', $sort_order='asc', $offset = 0) {

		$title = "Google Voice Trunk Overview";
		$page_explanation = "Trunks are gateways between HelloPBX and the public telephone network (PSTN), FXO gateways, or public services like Google Voice.";
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$this->load->model('trunk_sip_model');
		$limit = 20;
		$current_url = "trunk/googlevoice";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}

/*******************************************************************************/
/**
 * Display a list of gateways filtered to XMPP only
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function skype($sort_by = 'id', $sort_order='asc', $offset = 0) {

		$title = "Skype Trunk Overview";
		$page_explanation = "";
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$this->load->model('trunk_sip_model');
		$limit = 20;
		$current_url = "trunk/skype";
		$menu_key = 'trunk/summary';	// Identifies the main menu to be shown
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}



}
/* End of file timezone.php */
/* Location: ./application/controllers/timezone.php */