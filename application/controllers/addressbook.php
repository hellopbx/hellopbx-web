<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addressbook extends MY_Controller {
	
	
	public $menu_key = "addressbook/summary";

/*******************************************************************************/
   /**
     * Initialise the model, tie into the CodeIgniter superobject and
     * try our best to guess the table name.
     */
    function __construct()
    {
        parent::__construct();
		$this->load->model('addressbook_entry_model');
	}

/*******************************************************************************/
/**
 * Display a default screen - list
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function index($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$this->summary($sort_by, $sort_order, $offset);
		
	}
	
/*******************************************************************************/
/**
 * Display a default screen - list
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function summary($sort_by = 'id', $sort_order='asc', $offset = 0) {
		
		$success_message = "Delete Successful";
//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$all_successful = true;
				foreach ($_POST as $name => $value):
					if(substr($name,0,4) == "del_"):
						$delete_id = substr($name, 4);

						if ($this->addressbook_entry_model->delete($delete_id)):
							$this->add_message($success_message );
						else:
							$fail_insert = "Unsuccessful: " . validation_errors();
							echo $fail_insert;
							$this->add_message($fail_insert);
							$all_successful = false;
						endif;
					endif;
				endforeach;
				if ($all_successful):
//					$this->persist_messages();
//					redirect('/addressbook/summary');
				endif;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
			// Data was not posted - first time to the screen and no update necessary
		endif;		
//  ******************* END Commit DB Changes ****************************		
	
		
		$title = "Address Book Summary";
		$current_url = "addressbook/summary";
		$menu_key = $this->menu_key;	// Identifies the main menu to be shown


		
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$limit = 20;

		$this->addressbook_entry_model->limit($limit, $offset);
		$this->addressbook_entry_model->order_by($sort_by, $sort_order);
		$rows = $this->addressbook_entry_model->get_all();
		$num_rows = $this->addressbook_entry_model->count_all();
		$this->twiggy->set('rows', $rows);
			
		
		$this->load->library('pagination');
		$config = array();
		$config['base_url'] = "/$current_url/$sort_by/$sort_order";
		$config['total_rows'] = $num_rows;  
		$config['per_page'] = $limit;
		$config['uri_segment'] = 5; 
		$this->pagination->initialize($config); 
		$pagination = $this->pagination->create_links();
		if(strlen($pagination) > 0)	{$this->twiggy->set('pagination', $pagination);	}
		
		$this->twiggy->set('columns', array(
			'name' 		=> 'Name',
			'pstn' 	=> 'PSTN'
			));

	//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	
		$this->twiggy->set('current_url', '/'.$current_url . '/');
		
		$this->twiggy->set('add_url', "/addressbook/add");
		$this->twiggy->set('back_url', "/addressbook/summary");
		
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();

	}
	
	
/*******************************************************************************/
/**
 * Display a Trunk edit screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 public function edit($id=null, $type=null) {
		$title = "Address Book: Advanced";
		$page_explanation = "";
		$current_url = "addressbook/summary";
		$menu_key = $this->menu_key;	// Identifies the main menu to be shown
		$success_message = "Address book entry has been updated successfully.";  // Best to extend this to include the trunk name that has been added.

//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$is_all_successful = true;
				
				$number = $this->clean_nanpa($_POST["pstn"]);
				if($this->is_nanpa($number)):
					if ($this->addressbook_entry_model->update_advanced($id)):
						$this->add_message($success_message  . ": " . $_POST["name"] . " <i>(" . $this->format_nanpa($number) .")</i>" );
					else:
						$is_all_successful = false;
						$fail_insert = "Unsuccessful: " . validation_errors();
						$this->add_message($fail_insert);
					endif;
				else:
					$is_all_successful = false;
					$fail_insert = "Unsuccessful: " . $_POST["pstn"] . " is not a NANPA phone number";
					$this->add_message($fail_insert);
				endif;

				if ($is_all_successful):
					$this->persist_messages();
					redirect('/addressbook/summary');
				endif;
			
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		endif;
//  ******************* END Commit DB Changes ****************************

	$twig_view = $current_url;
		if (!isset($id)):
			// Same as summary
			$title = "Address Book: Overview";
			$twig_view = "trunk/sip"; 
		elseif (!$this->is_guid($id)):
			// value supplied is not a guid
			$title = "Address Book: Overview";
			$twig_view = "addressbook/summary"; 
		else :
		
			//  With some of the other modules, this array is database driven and can be customized for templates
			$sip_template_list = array(
			(object)array(
				"name"=>"Generic", 
				"description"=>"Here are the base data required to provision your phone.  For devices that have templates defined, additional parameters may be available", 
				"id"=>"")
			);
			$title = "Edit Address Book Entry: " . $sip_template_list[0]->name;
			$twig_view = "addressbook/cru";
			$page_explanation = $sip_template_list[0]->description;
			$id_template = $sip_template_list[0]->id;

			$form_value_list = $this->addressbook_entry_model->get_form_items($id);
			$this->addressbook_entry_model->get_form_data($id_template, $form_value_list);
			$this->twiggy->set("fvl", $form_value_list);	// for update, create date
	
	
			
			$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
			if ($id_template != 1): 
				$this->twiggy->set("advanced_url", "/"."addressbook/edit/".$id."/advanced/");		
			endif;
			$this->twiggy->set("back_url", "/".	"trunk/sip");
			$this->twiggy->set("view_type", "edit");
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}	
	
	
/*******************************************************************************/
/**
 * Display a Address book view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	public function view($id=null, $type=null) {
		$title = "Address Book: Advanced";
		$page_explanation = "";
		$current_url = "addressbook/summary";
		$menu_key = $this->menu_key;	// Identifies the main menu to be shown
		
		$twig_view = $current_url;
		if (!isset($id)):
			// Same as summary
			$title = "Address Book: Overview";
			$twig_view = "addressbook/summary"; 
		elseif (!$this->is_guid($id)):
			// value supplied is not a guid
			$title = "Address Book: Overview";
			$twig_view = "addressbook/summary"; 
		else :

			//  With some of the other modules, this array is database driven and can be customized for templates
			$sip_template_list = array(
			(object)array(
				"name"=>"Generic", 
				"description"=>"Here are the base data required to provision your phone.  For devices that have templates defined, additional parameters may be available", 
				"id"=>"")
			);
			$title = "Edit Address Book Entry: " . $sip_template_list[0]->name;
			$twig_view = "addressbook/cru";
			$page_explanation = $sip_template_list[0]->description;
			$id_template = $sip_template_list[0]->id;

			$form_value_list = $this->addressbook_entry_model->get_form_items($id);
			$this->addressbook_entry_model->get_form_data($id_template, $form_value_list);

	
			$this->twiggy->set("fvl", $form_value_list);	// for update, create date
			$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
	
			$this->twiggy->set("edit_url", "/"."addressbook/edit/".$id."/".$type);
			$this->twiggy->set("back_url", "/".	"addressbook/summary");
			$this->twiggy->set("view_type", "view");
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}


/*******************************************************************************/
/**
 * Display a Add Trunk screen
 *
 * @access	public
 * @param	string
 * @return	string
  */	
  public function add($type=null) {
		$title = "Address Book Entry Add: Advanced";
		$page_explanation = "We recommend you use a trunk template for adding a trunk or gateway.  If your provider is not listed, feel free to submit a feature request to have your provider added.";
		$current_url = "addressbook/summary";
		$menu_key = $this->menu_key;	// Identifies the main menu to be shown
		$success_message = "An address book entry has been added";  // Best to extend this to include the trunk name that has been added.

//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				if ($this->trunk_sip_model->insert_advanced()):
					$this->add_message($success_message);
					redirect('/addressbook/summary/');
				else:
					$fail_insert = "Unsuccessful: " . validation_errors();
					echo $fail_insert;
					$this->add_message($fail_insert);
				endif;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		endif;
//  ******************* END Commit DB Changes ****************************
		
		$twig_view = "addressbook/cru";
//		$this->load->model('trunk_sip_template_model');
//		$sip_template_list = $this->trunk_sip_template_model->get_by_key($type);

//		$id_template = 1;
//		if (count($sip_template_list) > 0):
//			$title = "Add Entry: " . $sip_template_list[0]->name;
//			$id_template = $sip_template_list[0]->id;
//			$page_explanation = $sip_template_list[0]->description;
//		else :
//			$title = "Add Address Book Entry";
//			$twig_view = "addressbook/add"; 
//		endif;
	
	
//		if (count($sip_template_list) > 0):
//		else:
			$sip_template_list = array(
			(object)array(
				"name"=>"Generic", 
				"description"=>"Here are the base data required to provision your phone.  For devices that have templates defined, additional parameters may be available", 
				"id"=>"")
			);
//		endif;	


		$title = "Edit Address Book Entry: " . $sip_template_list[0]->name;
		$twig_view = "addressbook/cru";
		$page_explanation = $sip_template_list[0]->description;
		$id_template = $sip_template_list[0]->id;

		$form_value_list = $this->addressbook_entry_model->get_form_items();
		$this->addressbook_entry_model->get_form_data($id_template, $form_value_list);


		$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
		$this->twiggy->set("back_url", "/".	"addressbook/summary");
		$this->twiggy->set("view_type", "add");

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}



/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function import() {
		$title = "Address Book Import";
		$page_explanation = "Import entries using text files.  Enter one entry per line. Phone number then the name separated with a comma";
		$current_url = "addressbook/import";
		$menu_key = 'addressbook/summary';	// Identifies the main menu to be shown
		$success_message = "An entry has been added: ";  // Best to extend this to include the trunk name that has been added.


//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$is_all_successful = true;
				$lines = explode("\n", $_POST["endpoint_import"]);
				foreach($lines as $line):
					$item = explode(",", $line);
				
					$number = $this->clean_nanpa($item[0]);
					if($this->is_nanpa($number)):
						if ($this->addressbook_entry_model->insert_free_text($number, $item[1])):
							$this->add_message($success_message  . $item[1] . " <i>(" . $this->format_nanpa($number) .")</i>" );
						else:
							$is_all_successful = false;
							$fail_insert = "Unsuccessful: " . validation_errors();
							$this->add_message($fail_insert);
						endif;
					else:
						$is_all_successful = false;
						$fail_insert = "Unsuccessful: " . $item[0] . " is not a NANPA phone number";
						$this->add_message($fail_insert);
					endif;
				endforeach;
				if ($is_all_successful):
					$this->persist_messages();
					redirect('/addressbook/summary');
				endif;
			
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		endif;
//  ******************* END Commit DB Changes ****************************
		
		$twig_view = "trunk/import";
		
		$mac_list = array("5878804322, Helia Support", "7804634492, Zuch Edmonton");
		$mac_imploded = implode("\n", $mac_list);
		$this->twiggy->set('mac_defaults', $mac_imploded);
		
		$this->twiggy->set("back_url", "/".	"trunk/sip");
		$this->twiggy->set("view_type", "add");

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}
	
	
	
}
/* End of file addressbook.php */
/* Location: ./application/controllers/addressbook.php */