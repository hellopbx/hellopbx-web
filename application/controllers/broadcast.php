<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broadcast extends MY_Controller {
	
	
	public $menu_key = "broadcast/summary";
	public $upload_folder = "/uploads";
	public $asset_folder = "/application/assets/broadcast";

/*******************************************************************************/
   /**
     * Initialise the model, tie into the CodeIgniter superobject and
     * try our best to guess the table name.
     */
    function __construct()
    {
        parent::__construct();
		$this->load->model('broadcast_model');
		$this->load->library('form_validation');

	}

/*******************************************************************************/
/**
 * Display a default screen - list
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function index($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$this->summary($sort_by, $sort_order, $offset);
		
	}
	
/*******************************************************************************/
/**
 * Display a default screen - list
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function summary($sort_by = 'id', $sort_order='asc', $offset = 0) {
		
		$success_message = "Delete Successful";
//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$all_successful = true;
				foreach ($_POST as $name => $value):
					if(substr($name,0,4) == "del_"):
						$delete_id = substr($name, 4);

						if ($this->broadcast_model->delete($delete_id)):
							$this->add_message($success_message );
						else:
							$fail_insert = "Unsuccessful: " . validation_errors();
							echo $fail_insert;
							$this->add_message($fail_insert);
							$all_successful = false;
						endif;
					endif;
				endforeach;
				if ($all_successful):
//					$this->persist_messages();
//					redirect('/broadcast/summary');
				endif;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
			// Data was not posted - first time to the screen and no update necessary
		endif;		
//  ******************* END Commit DB Changes ****************************		
	
		$title = "Broadcast Job Summary";
		$current_url = "broadcast/summary";
		$menu_key = $this->menu_key;	// Identifies the main menu to be shown


		
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$limit = 20;

		$this->broadcast_model->limit($limit, $offset);
		$this->broadcast_model->order_by($sort_by, $sort_order);
		$rows = $this->broadcast_model->get_all();
		$num_rows = $this->broadcast_model->count_all();
		$this->twiggy->set('rows', $rows);
			
		
		
		$this->load->library('pagination');
		$config = array();
		$config['base_url'] = "/$current_url/$sort_by/$sort_order";
		$config['total_rows'] = $num_rows;  
		$config['per_page'] = $limit;
		$config['uri_segment'] = 5; 
		$this->pagination->initialize($config); 
		$pagination = $this->pagination->create_links();
		if(strlen($pagination) > 0)	{$this->twiggy->set('pagination', $pagination);	}
		
		$this->twiggy->set('columns', array(
			'name' 		=> 'Name',
			'channels_max' 		=> 'Channels',
			'broadcast_type' 	=> 'Type',
			'start_at' 		=> 'Status'
			));

	//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	
		$this->twiggy->set('current_url', '/'.$current_url . '/');
		
		
		
		// This add is unique as a id needs to be assigned immediately so that files can be uploaded prior to the actual db entry being created.
		$this->twiggy->set('add_url', "/broadcast/add/" . $this->broadcast_model->new_guid());
		$this->twiggy->set('back_url', "/broadcast/summary");
		
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();

	}
	
	
/*******************************************************************************/
/**
 * Display a Trunk edit screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 public function edit($id=null, $type=null) {
		$title = "Broadcast Job: Advanced";
		$page_explanation = "";
		$current_url = "broadcast/summary";
		$menu_key = $this->menu_key;	// Identifies the main menu to be shown
		$success_message = "Broadcast Job has been updated successfully.";  // Best to extend this to include the trunk name that has been added.

//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$is_all_successful = true;
				
				$number = $this->clean_nanpa($_POST["pstn"]);
				if($this->is_nanpa($number)):
					if ($this->broadcast_model->update_advanced($id)):
						$this->add_message($success_message  . ": " . $_POST["name"] . " <i>(" . $this->format_nanpa($number) .")</i>" );
					else:
						$is_all_successful = false;
						$fail_insert = "Unsuccessful: " . validation_errors();
						$this->add_message($fail_insert);
					endif;
				else:
					$is_all_successful = false;
					$fail_insert = "Unsuccessful: " . $_POST["pstn"] . " is not a NANPA phone number";
					$this->add_message($fail_insert);
				endif;

				if ($is_all_successful):
					$this->persist_messages();
					redirect('/broadcast/summary');
				endif;
			
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		endif;
//  ******************* END Commit DB Changes ****************************

	$twig_view = $current_url;
		if (!isset($id)):
			// Same as summary
			$title = "Broadcast Job: Overview";
			$twig_view = "broadcast/summary"; 
		elseif (!$this->is_guid($id)):
			// value supplied is not a guid
			$title = "Broadcast Job: Overview";
			$twig_view = "broadcast/summary"; 
		else :
		
			//  With some of the other modules, this array is database driven and can be customized for templates
			$sip_template_list = array(
			(object)array(
				"name"=>"Generic", 
				"description"=>"Here are the base data required to provision your phone.  For devices that have templates defined, additional parameters may be available", 
				"id"=>"")
			);
			$title = "Edit Broadcast Job Entry: " . $sip_template_list[0]->name;
			$twig_view = "broadcast/cru";
			$page_explanation = $sip_template_list[0]->description;
			$id_template = $sip_template_list[0]->id;

			$form_value_list = $this->broadcast_model->get_form_items($id);
			$this->broadcast_model->get_form_data($id_template, $form_value_list, $id);
			$this->twiggy->set("fvl", $form_value_list);	// for update, create date
	
	
			
			$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
			if ($id_template != 1): 
				$this->twiggy->set("advanced_url", "/"."broadcast/edit/".$id."/advanced/");		
			endif;
			$this->twiggy->set("back_url", "/".	"broadcast/summary");
			$this->twiggy->set("view_type", "edit");
		endif;
		
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}	
	
	
/*************************************************************************`******/
/**
 * Display a Broadcast Job view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	public function view($id=null, $type=null) {
		$title = "Broadcast Job: Advanced";
		$page_explanation = "";
		$current_url = "broadcast/summary";
		$menu_key = $this->menu_key;	// Identifies the main menu to be shown
		
		$twig_view = $current_url;
		if (!isset($id)):
			// Same as summary
			$title = "Broadcast Job: Overview";
			$twig_view = "broadcast/summary"; 
		elseif (!$this->is_guid($id)):
			// value supplied is not a guid
			$title = "Broadcast Job: Overview";
			$twig_view = "broadcast/summary"; 
		else :

			//  With some of the other modules, this array is database driven and can be customized for templates
			$sip_template_list = array(
			(object)array(
				"name"=>"Generic", 
				"description"=>"Here are the base data required to provision your phone.  For devices that have templates defined, additional parameters may be available", 
				"id"=>"")
			);
			$title = "Edit Broadcast Job Entry: " . $sip_template_list[0]->name;
			$twig_view = "broadcast/cru";
			$page_explanation = $sip_template_list[0]->description;
			$id_template = $sip_template_list[0]->id;

			$form_value_list = $this->broadcast_model->get_form_items($id);
			$this->broadcast_model->get_form_data($id_template, $form_value_list, $id);
			
	
			

	
			$this->twiggy->set("fvl", $form_value_list);	// for update, create date
			$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
	
			$this->twiggy->set("edit_url", "/"."broadcast/edit/".$id."/".$type);
			$this->twiggy->set("back_url", "/".	"broadcast/summary");
			$this->twiggy->set("view_type", "view");
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}


/*******************************************************************************/
/**
 * Display a Add Trunk screen
 *
 * @access	public
 * @param	string
 * @return	string
  */	
  public function add($id=null, $type=null) {
		$title = "Broadcast Job Entry Add: Advanced";
		$page_explanation = "We recommend you use a trunk template for adding a trunk or gateway.  If your provider is not listed, feel free to submit a feature request to have your provider added.";
		$current_url = "broadcast/summary";
		$menu_key = $this->menu_key;	// Identifies the main menu to be shown
		$success_message = "A Broadcast Job entry has been added";  // Best to extend this to include the trunk name that has been added.

//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$is_all_successful = true;
				
						if ($this->broadcast_model->insert_advanced($id)):

							$this->add_message($success_message . ": " . $_POST["name"] );
						else:
							$is_all_successful = false;
							$fail_insert = "Unsuccessful: " . validation_errors();
							$this->add_message($fail_insert);
						endif;


				if ($is_all_successful):
					$this->persist_messages();
					redirect('/broadcast/summary');
				endif;
			
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		endif;
//  ******************* END Commit DB Changes ****************************


		
		$twig_view = "broadcast/cru";
		$sip_template_list = array(
			(object)array(
				"name"=>"Generic", 
				"description"=>"Here are the base data required to provision your phone.  For devices that have templates defined, additional parameters may be available", 
				"id"=>"")
			);

		$title = "Add Broadcast Job Entry: " . $sip_template_list[0]->name;
		$twig_view = "broadcast/cru";
		$page_explanation = $sip_template_list[0]->description;
		$id_template = $sip_template_list[0]->id;

		$form_value_list = $this->broadcast_model->get_form_items();
		$this->broadcast_model->get_form_data($id_template, $form_value_list, $id);
	
		$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
		$this->twiggy->set("back_url", "/".	"broadcast/summary");
		$this->twiggy->set("view_type", "add");

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}



/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function import() {
		$title = "Broadcast Job Import";
		$page_explanation = "Import entries using text files.  Enter one entry per line. Phone number then the name separated with a comma";
		$current_url = "broadcast/import";
		$menu_key = 'broadcast/summary';	// Identifies the main menu to be shown
		$success_message = "An entry has been added: ";  // Best to extend this to include the trunk name that has been added.


//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$is_all_successful = true;
				$lines = explode("\n", $_POST["endpoint_import"]);
				foreach($lines as $line):
					$item = explode(",", $line);
				
					$number = $this->clean_nanpa($item[0]);
					if($this->is_nanpa($number)):
						if ($this->broadcast_model->insert_free_text($number, $item[1])):
							$this->add_message($success_message  . $item[1] . " <i>(" . $this->format_nanpa($number) .")</i>" );
						else:
							$is_all_successful = false;
							$fail_insert = "Unsuccessful: " . validation_errors();
							$this->add_message($fail_insert);
						endif;
					else:
						$is_all_successful = false;
						$fail_insert = "Unsuccessful: " . $item[0] . " is not a NANPA phone number";
						$this->add_message($fail_insert);
					endif;
				endforeach;
				if ($is_all_successful):
					$this->persist_messages();
					redirect('/broadcast/summary');
				endif;
			
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		endif;
//  ******************* END Commit DB Changes ****************************
		
		$twig_view = "broadcast/import";
		
		$mac_list = array("5878804322, Helia Support", "7804634492, Zuch Edmonton");
		$mac_imploded = implode("\n", $mac_list);
		$this->twiggy->set('mac_defaults', $mac_imploded);
		
		$this->twiggy->set("back_url", "/".	"broadcast/summary");
		$this->twiggy->set("view_type", "add");

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}
	
	
/*******************************************************************************/
/**
 * Displays an asset like a fax page or voice broadcast message
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function asset($id = NULL){

// parameter can either be an id for pbx_broadcast.id to retreive the filename or the filename

// We are doing it this way so that we can check permissions before serving the file
	$this->auth->check_auth_view("");	// Check for view permissions
	
	if($id != NULL):
		if($this->is_guid($id)):
			$rows = $this->broadcast_model->get($id);
			$file_name = $rows->send_item_location;
			if(strlen($form_value_list[$item->name]["value"]) > 0):
				$file_json = json_decode($form_value_list[$item->name]["value"]);
				$mime = $file_json->mime;
				$file_name = $file_json->file_name;
			endif;
		else:
			$file_name = $id;
			switch($extension) {
					case "tiff":
						$mime = "image/tiff";
						break;
					case "tif":
						$mime='image/tiff';
						break;
					case "png":
						$mime = 'image/png';
						break;
					case "pdf":
						$mime = "application/pdf";
						break;
					case "wav":
						$mime = "audio/wav";
						break;
					case "mp3":
						$mime = "audio/mpeg";
						break;
					case "txt":
						$mime = "text/plain";
						break;
					default:
						$mime = "application/octet-stream";		
						break;
				}
		endif;
	
		$target_file = $_SERVER['DOCUMENT_ROOT'] . $this->asset_folder . "/" . $file_name;
		
		if (file_exists($target_file)) :
	//		header('Content-Description: File Transfer');
			$fileParts = pathinfo($file_name);
			$extensions = strtolower($fileParts['extension']);
			header('Content-Type: ' . $mime);
	
	//		header('Content-Disposition: attachment; filename='.basename($target_file));
			header('Content-Disposition: inline; filename='.basename($target_file));		
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($target_file));
			ob_clean();
			flush();
			readfile($target_file);
		else :
			
		 // disable download
			header ("HTTP/1.0 404 Not Found");	
		endif;
	else:
		header ("HTTP/1.0 404 Not Found");	
	endif;
	exit;

}
	
	
	
/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */		
public function check_exists ($id = null) {
//	$targetFolder = '/uploads'; // Relative to the root and should match the upload folder in the uploader script
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . $this->upload_folder . '/' . $_POST['filename'])) :
		echo 1;
	else :
		echo 0;
	endif;
}	


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */		
public function upload_content_all($id = null) {
	$file_types = array('tiff', 'tif', 'wav', 'txt', 'png'); // Allowed file extensions
	$this->_upload_content($id, $file_types);
}
/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */		
public function upload_content_fax($id = null) {
	$file_types = array('tiff', 'tif','png'); // Allowed file extensions
	$this->_upload_content($id, $file_types);
}
/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */		
public function upload_content_voice($id = null) {
	$file_types = array('wav'); // Allowed file extensions
	$this->_upload_content($id, $file_types);
}

/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */		
public function upload_content_text($id = null) {
	$file_types = array('txt'); // Allowed file extensions
	$this->_upload_content($id, $file_types);

}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */		
public function _upload_content($id, $file_types) {
	$verifyToken = md5('unique_salt' . $_POST['timestamp']);

	if (!empty($_FILES) && $_POST['token'] == $verifyToken) :
		$tempFile   = $_FILES['Filedata']['tmp_name'];
//		$targetFile = $_SERVER['DOCUMENT_ROOT'] . $this->upload_folder . "/" . $_FILES['Filedata']['name'];

		// Validate the filetype
		$fileParts = pathinfo($_FILES['Filedata']['name']);
		if (in_array(strtolower($fileParts['extension']), $file_types)) :
		// Save the file
			$new_name = $id . "." . strtolower($fileParts['extension']);
			$target_file = $_SERVER['DOCUMENT_ROOT'] . $this->asset_folder . "/" . $new_name;
	
			move_uploaded_file($tempFile, $target_file);


			$fileParts = pathinfo($target_file);
			$extension = strtolower($fileParts['extension']);
	
	
	//  If mime types are added, ensure they are also added to the twig m_form macro lib
			switch($extension) {
				case "tiff":
					$mime = 'image/tiff';
					break;
				case "tif":
					$mime = 'image/tiff';
					break;
				case "png":
					$mime = 'image/png';
					break;
				case "pdf":
					$mime = 'application/pdf';
					break;
				case "wav":
					$mime = 'audio/wav';
					break;
				case "mp3":
					$mime = 'audio/mpeg';
					break;
				case "txt":
					$mime = 'text/plain';
					break;
				default:
					$mime = 'application/octet-stream';		
					break;
			}
			
			$json = '{"file_name":"' . $new_name . '","mime":"' . $mime . '"}';
			

			$this->broadcast_model->update_send_item_location($id, $json, $extension);	
			
			echo 1;
		else :
			// The file type wasn't allowed
			echo 'Invalid file type.';
		endif;
	endif;
}

	
}
/* End of file broadcast.php */
/* Location: ./application/controllers/broadcast.php */