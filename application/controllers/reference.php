<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reference extends MY_Controller {

    function __construct()
    {
        parent::__construct();

	}

	public function index() {
		 $this->summary();
	}
	public function summary() {
		$title = "Reference Tables"
		$current_url = "reference/summary";
		$menu_key = 'reference/summary';	// Identifies the main menu to be shown
		$title = "Reference Summary";


		//  Variables used for table headings and sorting
		
		
		$this->twiggy->set('title', $title);
		$this->twiggy->set('current_url', '/'.$current_url . '/');
			
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */