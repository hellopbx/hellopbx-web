a<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Endpoint extends MY_Controller {
	
/*******************************************************************************/
   /**
     * Initialise the model, tie into the CodeIgniter superobject and
     * try our best to guess the table name.
     */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('endpoint_sip_model');
		$this->load->library('form_validation');
    }
	
	
/*******************************************************************************/
/**
 * Display a default screen - list
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function index($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$this->summary($sort_by, $sort_order, $offset);
	}

/*******************************************************************************/
/**
 * Display a List of Trunk screen
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	
	public function summary($sort_by = 'id', $sort_order='asc', $offset = 0) {
		
$success_message = "Delete Successful";

//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				foreach ($_POST as $name => $value):
					if(substr($name,0,4) == "del_"):
						$delete_id = substr($name, 4);

						if ($this->endpoint_sip_model->delete($delete_id)):
							$this->add_message($success_message );
						else:
							$fail_insert = "Unsuccessful: " . validation_errors();
							echo $fail_insert;
							$this->add_message($fail_insert);
						endif;
					endif;
				endforeach;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
			// Data was not posted - first time to the screen and no update necessary
		endif;		
//  ******************* END Commit DB Changes ****************************		
			
		
		$title = "Endpoint Summary";
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$this->load->model('endpoint_xmpp_model');
		$this->load->model('endpoint_pstn_model');
		$limit = 20;
		$current_url = "endpoint/summary";
		$menu_key = 'endpoint/summary';	// Identifies the main menu to be shown


		$this->endpoint_sip_model->limit($limit, $offset);
		$this->endpoint_sip_model->order_by($sort_by, $sort_order);
		$rows_sip = $this->endpoint_sip_model->get_all();
		$num_rows_sip = $this->endpoint_sip_model->count_all();

		$this->endpoint_xmpp_model->limit($limit, $offset);
		$this->endpoint_xmpp_model->order_by($sort_by, $sort_order);
		$rows_xmpp = $this->endpoint_xmpp_model->get_all();
		$num_rows_xmpp = $this->endpoint_xmpp_model->count_all();

		$this->endpoint_pstn_model->limit($limit, $offset);
		$this->endpoint_pstn_model->order_by($sort_by, $sort_order);
		$rows_pstn = $this->endpoint_pstn_model->get_all();
		$num_rows_pstn = $this->endpoint_pstn_model->count_all();
		
		$this->twiggy->set('columns', array(
			'name' 		=> 'Location',
			'phone_name' 	=> 'Phone Type',
			'mac' 	=> 'MAC Address'
			));

			
		//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	
		$this->twiggy->set('current_url', '/'.$current_url . '/');
		
		$this->twiggy->set('add_url', "/endpoint/sip/add");
		$this->twiggy->set('back_url', "/endpoint/sip");
		
		$this->twiggy->set('rows_sip', $rows_sip);
		$this->twiggy->set('rows_xmpp', $rows_xmpp);
		$this->twiggy->set('rows_pstn', $rows_pstn);
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}


/*******************************************************************************/
/**
 * Display a List of SIP screen
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	
	public function summary_sip($sort_by = 'id', $sort_order='asc', $offset = 0) {
		
$success_message = "Delete Successful";

//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				foreach ($_POST as $name => $value):
					if(substr($name,0,4) == "del_"):
						$delete_id = substr($name, 4);

						if ($this->endpoint_sip_model->delete($delete_id)):
							$this->add_message($success_message );
						else:
							$fail_insert = "Unsuccessful: " . validation_errors();
							echo $fail_insert;
							$this->add_message($fail_insert);
						endif;
					endif;
				endforeach;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
			// Data was not posted - first time to the screen and no update necessary
		endif;		
//  ******************* END Commit DB Changes ****************************		
		
		$title = "Endpoint SIP Summary";
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$this->load->model('endpoint_xmpp_model');
		$this->load->model('endpoint_pstn_model');
		$limit = 20;
		$current_url = "endpoint/sip";
		$menu_key = 'endpoint/summary';	// Identifies the main menu to be shown


		$this->endpoint_sip_model->limit($limit, $offset);
		$this->endpoint_sip_model->order_by($sort_by, $sort_order);
		$rows_sip = $this->endpoint_sip_model->get_all();
		$num_rows_sip = $this->endpoint_sip_model->count_all();

		$this->twiggy->set('columns', array(
			'name' 		=> 'Location',
			'phone_name' 	=> 'Phone Type',
			'mac' 	=> 'MAC Address'
			));
  			
		//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	
		$this->twiggy->set('current_url', '/'.$current_url . '/');
			
		$this->twiggy->set('add_url', "/endpoint/sip/add");
		$this->twiggy->set('back_url', "/endpoint/sip");

			$this->twiggy->set('rows', $rows_sip);
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}


/*******************************************************************************/
/**
 * direct SIP endpoint forms
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 	public function sip($view_type="view", $id=null, $brand=null, $family=null, $model=null) {
		switch ($view_type) {
		case "edit":	
			if (!isset($id)){
				$this->summary_sip();
				return;
				} 
			elseif (!$this->is_guid($id)){
				$this->summary_sip();
				return;
				}
			else {
				$this->sip_edit($id, $brand, $family, $model);
				return;
			}
			break;
		case "add":
			if (!isset($id)){
				$this->choose_provider("sip");
				return;
				} 
			elseif ($this->is_guid($id)){
				$this->sip_view($id);
				return;
				}
			else {
				$this->sip_add($id); // Id parameter actually hold form_template because id parameter is excluded for add.
				return;
			}
			break;
		default:
			if (!isset($id)){
				$this->summary_sip();
				return;
				} 
			elseif (!$this->is_guid($id)){
				$this->summary_sip();
				return;
				}
			else {
				$this->sip_view($id, $brand, $family, $model);
				return;
			}
			break;	
		}
	}

	
/*******************************************************************************/
/**
 * Display a List of Trunk screen
 *
 * @access	public
 * @param	string
 * @return	string
 */	
	public function template($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$title = "Endpoint Template Summary";
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$limit = 20;
		$current_url = "endpoint/template";
		$menu_key = 'endpoint/summary';	// Identifies the main menu to be shown

		$this->load->model('endpoint_sip_template_model');
		$this->endpoint_sip_template_model->limit($limit, $offset);
		$this->endpoint_sip_template_model->order_by($sort_by, $sort_order);
		$rows_sip = $this->endpoint_sip_template_model->get_all();
		$num_rows_sip = $this->endpoint_sip_template_model->count_all();

		$this->twiggy->set('fields', array(
			'name' 		=> 'Time Zone',
			'abbr' 	=> 'Abbr',
			'utc' 	=> 'UTC',
			'hours'	=> 'GMT Offset'
			));

			
		//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	
		$this->twiggy->set('current_url', '/'.$current_url . '/');
			
		
		$this->twiggy->set('rows_sip', $rows_sip);
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}
	
	
	
/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function import() {
		$title = "Endpoint Import";
		$page_explanation = "Import endpoints using text files.  Enter one mac address per line.";
		$current_url = "endpoint/import";
		$menu_key = 'endpoint/summary';	// Identifies the main menu to be shown
		$success_message = "An endpoint has been added: ";  // Best to extend this to include the trunk name that has been added.


//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$this->load->model('phone_mac_model');
				$is_successful = true;
//				echo("&& " . $_POST["endpoint_import"] . " &&");
				$lines = explode("\n", $_POST["endpoint_import"]);
				foreach($lines as $item):
					if($this->is_mac($item)):
						$phone_type = $this->phone_mac_model->match($item);
						if(count($phone_type) > 0):
//							echo "<br/> is mac " . $item;
//							echo "<br/>" . $phone_type["brand"];
//							echo "<br/>" . $phone_type["family"];
//							echo "<br/>" . $phone_type["model"];

							if ($this->endpoint_sip_model->insert_free_text($item)):
								$this->add_message($success_message . $phone_type["phone_name"] . " (" . $this->format_mac($item) . ")");
							else:
								$is_successful = false;
								$fail_insert = "Unsuccessful: " . validation_errors();
								$this->add_message($fail_insert);
							endif;
						else:
								$is_successful = false;
								$fail_insert = "Unsuccessful: No Template Found";
								$this->add_message($fail_insert);
						endif;
					else:
						$is_successful = false;
						$fail_insert = "Unsuccessful: " . $item . " is not a MAC address";
						$this->add_message($fail_insert);
					endif;
				endforeach;
				if ($is_successful):
//					$this->persist_messages();
//					redirect('/endpoint/sip/');
//					return true;
				endif;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		endif;
//  ******************* END Commit DB Changes ****************************
		
		$twig_view = "trunk/import";
		
		$mac_list = array("00041345162C","C40ACBBCAD10","00041326C62B","000E08CCE5AC","00041326C62B","0004132E3AFE","00041345F3DB","0004133BDF75","00156513BCBF","000413430084");
		$mac_imploded = implode("\n", $mac_list);
//		$this->twiggy->set("mac_defaults", $mac_imploded);
		$this->twiggy->set('mac_defaults', $mac_imploded);
		
//		echo ($mac_imploded);
		
//		$this->load->model('endpoint_sip_template_model');
		
//		$type="";  // type comes from the sip provider type.  We will not use it here.
//		$sip_template_list = $this->endpoint_sip_template_model->get_by_key($type);

//		$id_template = 1;
//		if (count($sip_template_list) > 0):
//			$title = "Add Trunk: " . $sip_template_list[0]->name;
//			$id_template = $sip_template_list[0]->id;
//			$page_explanation = $sip_template_list[0]->description;
//		else :
//			$title = "Add Endpoint: Choose Provider";
//			$twig_view = "endpoint/choose_provider"; 
//		endif;
			
//		$form_value_list = $this->endpoint_sip_model->get_form_items();
//		$this->load->model('endpoint_sip_template_item_model');
//		$this->endpoint_sip_template_item_model->get_form_data($id_template, $form_value_list);


//		$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
		$this->twiggy->set("back_url", "/".	"trunk/sip");
		$this->twiggy->set("view_type", "add");

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}
	

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function format_mac($mac){
	return substr($mac, 0,2) . ":" . substr($mac, 2,2) . ":" . substr($mac, 4,2) . ":" . substr($mac, 6,2) . ":" . substr($mac, 8,2) . ":" . substr($mac, 10,2) ;
}

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	public function sip_view($id=null, $brand=null, $family=null, $model=null) {
		$title = "End Point: Generic";
		$page_explanation = "";
		$current_url = "endpoint/sip";
		$menu_key = 'endpoint/summary';	// Identifies the main menu to be shown
		
		$twig_view = $current_url;
		if (!isset($id)):
			// Same as summary
			$title = "End Point: SIP Overview";
			$twig_view = "endpoint/sip"; 
		elseif (!$this->is_guid($id)):
			// value supplied is not a guid
			$title = "End Point: SIP Overview";
			$twig_view = "endpoint/sip"; 
		else :
			//  Template currently just determines Title and page description but will add extra template fields in the future
			$this->load->model('endpoint_sip_template_model');
			$sip_template_list = $this->endpoint_sip_template_model->get_by_key($brand, $family, $model);
			if (count($sip_template_list) > 0):
			else:
				$sip_template_list = array(
				(object)array(
					"name"=>"Generic", 
					"description"=>"Here are the base data required to provision your phone.  For devices that have templates defined, additional parameters may be available", 
					"id"=>"")
				);
			endif;

			$title = "View End Point: " . $sip_template_list[0]->name;
			$twig_view = "endpoint/sip_cru";
			$page_explanation = $sip_template_list[0]->description;
			$id_template = $sip_template_list[0]->id;
	
			$this->load->model('endpoint_sip_identity_model');
			$form_value_list_children = $this->endpoint_sip_identity_model->get_form_items_all($id);		//gets multiple rows of form items based on a parent id
			$this->twiggy->set("fvl_child", $form_value_list_children);	// for update, create date			
			
			$form_item_list_children = $this->endpoint_sip_identity_model->get_form_data_all($id, $form_value_list_children);
			$this->twiggy->set("fil_child", $form_item_list_children);		// fil is the full field description + field values + field errors
	
			if(count($form_value_list_children) > 1) :
				$twig_view = "endpoint/sip_cru_w_child";
			else:
				$twig_view = "endpoint/sip_cru";
			endif;


			$form_value_list = $this->endpoint_sip_model->get_form_items($id);

		$this->endpoint_sip_model->get_form_data($id_template, $form_value_list);

			$this->twiggy->set("fvl", $form_value_list);	// for update, create date
			$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in advanced mode.
	
			$this->twiggy->set("edit_url", "/"."endpoint/sip/edit/".$id."/".$brand."/".$family."/".$model);
			$this->twiggy->set("back_url", "/".	"endpoint/sip");
			$this->twiggy->set("view_type", "view");
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}
	
	
	
	
	
	
	
	
	

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	public function sip_edit($id=null, $brand=null, $family=null, $model=null) {
		$success_message = "SIP End Point has been updated successfully.";  // Best to extend this to include the trunk name that has been added.
	 
	 
//  ******************* Commit DB Changes ****************************
		if ($this->input->server('REQUEST_METHOD') === 'POST'):
			if ($this->auth->check_auth_crud("")):
				$result = $this->endpoint_sip_model->update_advanced($id);
				if ($result):
					$this->add_message($success_message );
					$this->persist_messages();
					redirect('/endpoint/sip/');
				else:
					$fail_insert = "Unsuccessful: " . validation_errors();
					echo $fail_insert;
					$this->add_message($fail_insert);
				endif;
			else :
				$fail_permission = "Unsuccessful: Insufficient permissions";
				$this->add_message($fail_permission);
			endif;
		else:
		
			// Data was not posted - first time to the screen and no update necessary
		endif;		
//  ******************* END Commit DB Changes ****************************	 
	 
		$title = "End Point: Generic";
		$page_explanation = "";
		$current_url = "endpoint/sip";
		$menu_key = 'endpoint/summary';	// Identifies the main menu to be shown
		
		$twig_view = $current_url;
		if (!isset($id)):
			// Same as summary
			$title = "End Point: SIP Overview";
			$twig_view = "endpoint/sip"; 
		elseif (!$this->is_guid($id)):
			// value supplied is not a guid
			$title = "End Point: SIP Overview";
			$twig_view = "endpoint/sip"; 
		else :
			//  Template currently just determines Title and page description but will add extra template fields in the future
			$this->load->model('endpoint_sip_template_model');
			$sip_template_list = $this->endpoint_sip_template_model->get_by_key($brand, $family, $model);
			if (count($sip_template_list) > 0):
			else:
				$sip_template_list = array(
				(object)array(
					"name"=>"Generic", 
					"description"=>"Here are the base data required to provision your phone.  For devices that have templates defined, additional parameters may be available", 
					"id"=>"")
				);
			endif;

			$title = "Edit End Point: " . $sip_template_list[0]->name;
			$twig_view = "endpoint/sip_cru";
			$page_explanation = $sip_template_list[0]->description;
			$id_template = $sip_template_list[0]->id;
	
			$this->load->model('endpoint_sip_identity_model');
			$form_value_list_children = $this->endpoint_sip_identity_model->get_form_items_all($id);		//gets multiple rows of form items based on a parent id
			$this->twiggy->set("fvl_child", $form_value_list_children);	// for update, create date			
			
			$form_item_list_children = $this->endpoint_sip_identity_model->get_form_data_all($id, $form_value_list_children);
			$this->twiggy->set("fil_child", $form_item_list_children);		// fil is the full field description + field values + field errors
	
			if(count($form_value_list_children) > 1) :
				$twig_view = "endpoint/sip_cru_w_child";
			else:
				$twig_view = "endpoint/sip_cru";
			endif;




			$form_value_list = $this->endpoint_sip_model->get_form_items($id);
			$this->endpoint_sip_model->get_form_data($id_template, $form_value_list);

			$this->twiggy->set("fvl", $form_value_list);	// for update, create date
			$this->twiggy->set("id_template", $id_template);		//  This is for a hidden form field should only be set with an add.  id_template should not be changed on an edit and recommend the user deletes the record and recreates or edits in f mode.
	
			$this->twiggy->set("edit_url", "/"."endpoint/sip/edit/".$id."/".$brand."/".$family."/".$model);
			$this->twiggy->set("back_url", "/".	"endpoint/sip");
			$this->twiggy->set("view_type", "edit");
		endif;
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->twiggy->set('page_explanation', $page_explanation);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($twig_view)->display();
	}	
	
	
	
	
	

	
	

}
/* End of file endpoint.php */
/* Location: ./application/controllers/endpoint.php */