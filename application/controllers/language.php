<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language extends MY_Controller {

	public function index($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$this->summary($sort_by, $sort_order, $offset);
	}
	public function summary($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$title = "Language Summary";
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$this->load->model('language_model');
		$limit = 20;
		$current_url = "language/summary";
		$menu_key = 'reference/summary';	// Identifies the main menu to be shown

		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);

		$this->language_model->limit($limit, $offset);
		$this->language_model->order_by($sort_by, $sort_order);
		$rows = $this->language_model->get_all();
		$num_rows = $this->language_model->count_all();
		
		$this->twiggy->set('fields', array(
			'language' 		=> 'Language',
			'one' 	=> 'Abbr',
			'native'	=> 'Native'
			));

			
		//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	
		$this->twiggy->set('current_url', '/'.$current_url . '/');
			
		
		$this->twiggy->set('rows', $rows);
		
		$this->load->library('pagination');
		$config = array();
		$config['base_url'] = "/$current_url/$sort_by/$sort_order";
		$config['total_rows'] = $num_rows;  
		$config['per_page'] = $limit;
		$config['uri_segment'] = 5; 
		$this->pagination->initialize($config); 
	
		$pagination = $this->pagination->create_links();
		if(strlen($pagination) > 0)	{
			$this->twiggy->set('pagination', $pagination);	
		}
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->set_menus($menu_key, $current_url); 
		$this->twiggy->template($current_url)->display();
	}
}
/* End of file language.php */
/* Location: ./application/controllers/language.php */