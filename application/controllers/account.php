<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller {

    function __construct()
    {
        parent::__construct();
		$this->load->model('account_model');
	}



	public function index($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$this->summary($sort_by, $sort_order, $offset);
	}
	public function summary($sort_by = 'id', $sort_order='asc', $offset = 0) {
		$title = "Account Summary";
		$current_url = "auth/account_profile";
		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$limit = 20;
		
		$this->account_model->limit($limit, $offset);
		$this->account_model->order_by($sort_by, $sort_order);
		$rows = $this->account_model->get_all();
		$num_rows = $this->account_model->count_all();
		
		$this->twiggy->set('fields', array(
			'username' 		=> 'Username',
			'email' 	=> 'E-mail',
			'createdon'	=> 'Created'
			));

			
		//  Variables used for table headings and sorting
		$this->twiggy->set('offset', $offset);	
		$this->twiggy->set('sort_by', $sort_by);
		$this->twiggy->set('sort_order', $sort_order);	
		$this->twiggy->set('current_url', '/'.$current_url . '/');
			
		
		$this->twiggy->set('rows', $rows);
		
		$this->load->library('pagination');
		$config = array();
		$config['base_url'] = "/$current_url/$sort_by/$sort_order";
		$config['total_rows'] = $num_rows;  
		$config['per_page'] = $limit;
		$config['uri_segment'] = 5; 
		$this->pagination->initialize($config); 
	
		$pagination = $this->pagination->create_links();
		if(strlen($pagination) > 0)	{
			$this->twiggy->set('pagination', $pagination);	
		}
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->set('title', $title);
		$this->set_menus($menu_key, $current_url);
		$this->twiggy->template($current_url)->display();
	}
}
/* End of file account.php */
/* Location: ./application/controllers/account.php */