<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Auth {

	/**
	 * Check view for authorization or redirects to an unauthorized 401 view.
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function check_auth_view($permission)
	{
		if(strlen($permission) == 0) return;
		// $user_permissions = get from session;
		if (!array_key_exists($permission, $user_permissions)){
			header('HTTP/1.0 401 Unauthorized');
    		echo 'Text to send if user hits Cancel button';
    		exit;		
		}
	}


	/**
	 * Check create, read, update, or delete for authorization or redirects to an unauthorized 401 view.
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function check_auth_crud($permission)
	{
		if(strlen($permission) == 0) return true;
		// $user_permissions = get from session;
		if (!array_key_exists($permission, $user_permissions)){
			return false;
		} else {
			return true;
		}
	}
	
	
	function set_menus($menu_key, $menu_sub_key) {
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $this->session->userdata('id_menu_group') ? $this->session->userdata('id_menu_group') : 1);
		$this->twiggy->set('menu', $rows, true);

		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);

		$this->twiggy->set('menu_current', $menu_key);
		$this->twiggy->set('submenu_current', $menu_sub_key);
	}

	
	// ------------------------------------------------------------------------
	}

/* End of file Auth.php */