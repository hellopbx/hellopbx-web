<?php
/*
 * Sign_out Controller
 */
class Sign_out extends CI_Controller {
	
	/**
	 * Constructor
	 */
    function __construct()
    {
        parent::__construct();
		
		$id_menu_group = 2;		
		// Load the necessary stuff...
		$this->load->helper(array('language', 'url'));
		$this->load->config('auth/account');
		$this->load->language(array('general', 'auth/sign_out'));
        $this->load->library(array('auth/authentication'));

		if ($this->authentication->is_signed_in()) : 
			$this->twiggy->set('account', $account);
		endif;

		
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $id_menu_group);
		$this->twiggy->set('menu', $rows, true);



	}


	
	// --------------------------------------------------------------------
	
	/**
	 * Account sign out
	 *
	 * @access public
	 * @return void
	 */
	function index()
	{

		$current_url = 'auth/sign_out';
		$this->twiggy->set('menu_current', 'role/summary');
		$this->twiggy->set('submenu_current', $current_url);

		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);


		// Redirect signed out users to homepage
		if ( ! $this->authentication->is_signed_in()) redirect('');
		
		// Run sign out routine
		$this->authentication->sign_out();
		
		// Redirect to homepage
		if ( ! $this->config->item("sign_out_view_enabled")) redirect('');
		
		// Load sign out view
		$this->load->view('sign_out');
	}
	
}


/* End of file sign_out.php */
/* Location: ./application/modules/auth/controllers/sign_out.php */