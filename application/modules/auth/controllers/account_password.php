<?php
/*
 * Account_password Controller
 */
class Account_password extends CI_Controller {
	
	/**
	 * Constructor
	 */
    function __construct()
    {
        parent::__construct();

		$id_menu_group = 2;		
		$current_url = 'auth/account_password';
		$this->twiggy->set('menu_current', 'role/summary');
		$this->twiggy->set('submenu_current', $current_url);

		
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $id_menu_group);
		$this->twiggy->set('menu', $rows, true);

		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);

		// Load the necessary stuff...
		$this->load->config('auth/account');
		$this->load->helper(array('date', 'language', 'auth/ssl', 'url'));
        $this->load->library(array('auth/authentication', 'form_validation'));
		$this->load->model(array('auth/account_model'));
		$this->load->language(array('general', 'auth/account_password'));
		
		$template = "default";
		$this->twiggy->set('template', array(
			'name' 		=> $template,
			'template_path' 	=> "/application/themes/$template/_layouts/",
			'project_path'	=> '/',
			'css_path' => "/res/$template/css",
			'image_path' => "/res/$template/img/"
			), true);
		
		if ($this->authentication->is_signed_in()) : 
			$this->twiggy->set('account', $account);
		endif;

	}
	
	/**
	 * Account password
	 */
	function index()
	{
		// Enable SSL?
		maintain_ssl($this->config->item("ssl_enabled"));
		
		// Redirect unauthenticated users to signin page
		if ( ! $this->authentication->is_signed_in()) 
		{
			redirect('auth/sign_in/?continue='.urlencode(base_url().'auth/account_password'));
		}
		
		// Retrieve sign in user
		$data['account'] = $this->account_model->get_by_id($this->session->userdata('account_id'));
		
		// No access to users without a password
		if ( ! $data['account']->password) redirect('');
		
		### Setup form validation
		$this->form_validation->set_error_delimiters('<span class="field_error">', '</span>');
		$this->form_validation->set_rules(array(
			array('field'=>'password_new_password', 'label'=>'lang:password_new_password', 'rules'=>'trim|required|min_length[6]'),
			array('field'=>'password_retype_new_password', 'label'=>'lang:password_retype_new_password', 'rules'=>'trim|required|matches[password_new_password]')
		));
		
		### Run form validation
		if ($this->form_validation->run()) 
		{
			// Change user's password
			$this->account_model->update_password($data['account']->id, $this->input->post('password_new_password'));
			$this->session->set_flashdata('password_info', lang('password_password_has_been_changed'));
			redirect('auth/account_password');
		}
		
		$this->twiggy->set('password_info', $this->session->flashdata('password_info'));
	
		$this->twiggy->set('password_new_password', array(
			'value' => set_value('password_new_password'),
			'form_error' => form_error('password_new_password'), 
			'field_error' => ''
		));	
		
		$this->twiggy->set('password_retype_new_password', array(
			'value' => set_value('password_retype_new_password'),
			'form_error' => form_error('password_retype_new_password'), 
			'field_error' => ''
		));		
	
	
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->template('account_password')->display();
//		$this->load->view('auth/account_password', $data);
	}
	
}


/* End of file account_password.php */
/* Location: ./application/modules/auth/controllers/account_password.php */