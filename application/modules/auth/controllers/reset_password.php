<?php
/*
 * Reset_password Controller
 */
class Reset_password extends CI_Controller {
	
	/**
	 * Constructor
	 */
    function __construct()
    {
        parent::__construct();
		
		// Load the necessary stuff...
		$id_menu_group = 2;		
		$this->load->config('auth/account');
		$this->load->helper(array('date', 'language', 'auth/ssl', 'url'));
        $this->load->library(array('auth/authentication', 'auth/recaptcha', 'form_validation'));
		$this->load->model(array('auth/account_model'));
		$this->load->language(array('general', 'auth/reset_password'));
		
		// These should be moved to a central location
		$template = "default";
		$this->twiggy->set('template', array(
			'name' 		=> $template,
			'template_path' 	=> "/application/themes/$template/_layouts/",
			'project_path'	=> '/',
			'css_path' => "/res/$template/css",
			'image_path' => "/res/$template/img/"
			), true);

		
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $id_menu_group);
		$this->twiggy->set('menu', $rows, true);


		if ($this->authentication->is_signed_in()) : 
			$this->twiggy->set('account', $account);
		endif;

	}
	
	/**
	 * Reset password
	 */
	function index() 
	{
		$current_url = 'auth/reset_password';
		$this->twiggy->set('menu_current', 'role/summary');
		$this->twiggy->set('submenu_current', $current_url);

		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);
		
		$this->twiggy->set('current_url', current_url());
		$this->twiggy->set('uri_string', uri_string());
		
		$this->twiggy->set('query_string', (empty($_SERVER['QUERY_STRING'])?'':'?'.$_SERVER['QUERY_STRING']));
		
		
		// Enable SSL?
		maintain_ssl($this->config->item("ssl_enabled"));
		
		// Redirect signed in users to homepage
		if ($this->authentication->is_signed_in()) redirect('');
		
		// Check recaptcha
		$recaptcha_result = $this->recaptcha->check();
		
		// User has not passed recaptcha
		if ($recaptcha_result !== TRUE)
		{
			if ($this->input->post('recaptcha_challenge_field')) 
			{
//				$data['reset_password_recaptcha_error'] = $recaptcha_result ? lang('reset_password_recaptcha_incorrect') : lang('reset_password_recaptcha_required');
			$this->twiggy->set('reset_password_recaptcha_error', $recaptcha_result ? lang('reset_password_recaptcha_incorrect') : lang('reset_password_recaptcha_required'));
			}
			
			// Load recaptcha code
//			$data['recaptcha'] = $this->recaptcha->load($recaptcha_result, $this->config->item("ssl_enabled"));
			$this->twiggy->set('recaptcha', $this->recaptcha->load($recaptcha_result, $this->config->item("ssl_enabled")));
			
			// Load reset password captcha view
			
			$this->auth->check_auth_view("");	// Check for view permissions
			$this->twiggy->template('reset_password_unsuccessful')->display();
//			$this->load->view('reset_password_captcha', isset($data) ? $data : NULL);
			return;
		}
		
		// Get account by email
		if ($account = $this->account_model->get_by_id($this->input->get('id')))
		{
			// Check if reset password has expired
			if (now() < (strtotime($account->resetsenton) + $this->config->item("password_reset_expiration")))
			{
				// Check if token is valid
				if ($this->input->get('token') == sha1($account->id.strtotime($account->resetsenton).$this->config->item('password_reset_secret')))
				{
					// Remove reset sent on datetime
					$this->account_model->remove_reset_sent_datetime($account->id);
					
					// Upon sign in, redirect to change password page
					$this->session->set_userdata('sign_in_redirect', 'auth/account_password');
					
					// Run sign in routine
					$this->authentication->sign_in($account->id);
				}
			}
		}
		
		
		
	
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->template('reset_password_unsuccessful')->display();

		
		// Load reset password unsuccessful view
//		$this->load->view('reset_password_unsuccessful', isset($data) ? $data : NULL);
	
		
		
		
		
	}
	
}


/* End of file reset_password.php */
/* Location: ./application/modules/auth/controllers/reset_password.php */