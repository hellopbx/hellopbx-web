<?php
/*
 * Account_linked Controller
 */
class Account_linked extends CI_Controller {
	
	/**
	 * Constructor
	 */
    function __construct()
    {
        parent::__construct();
		
		$id_menu_group = 2;		
		
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $id_menu_group);
		$this->twiggy->set('menu', $rows, true);

		// Load the necessary stuff...
		$this->load->config('auth/account');
		$this->load->helper(array('language', 'auth/ssl', 'url'));
        $this->load->library(array('auth/authentication', 'form_validation'));
		$this->load->model(array('auth/account_model', 'auth/account_facebook_model', 'auth/account_twitter_model', 'auth/account_openid_model'));
		$this->load->language(array('general', 'auth/account_linked', 'auth/connect_third_party'));
		
			// These should be moved to a central location
		$template = "default";
		$this->twiggy->set('template', array(
			'name' 		=> $template,
			'template_path' 	=> "/application/themes/$template/_layouts/",
			'project_path'	=> '/',
			'css_path' => "/res/$template/css",
			'image_path' => "/res/$template/img/"
			), true);
		
		if ($this->authentication->is_signed_in()) : 
			$this->twiggy->set('account', $account);
		endif;
	}
	
	/**
	 * Linked accounts
	 */
	function index()
	{
		$current_url = 'auth/account_linked';
		$this->twiggy->set('menu_current', 'role/summary');
		$this->twiggy->set('submenu_current', $current_url);

		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);

		// Enable SSL?
		maintain_ssl($this->config->item("ssl_enabled"));
		
		// Redirect unauthenticated users to signin page
		if ( ! $this->authentication->is_signed_in()) 
		{
			redirect('auth/sign_in/?continue='.urlencode(base_url().'auth/account_linked'));
		}
		
		// Retrieve sign in user
		$data['account'] = $this->account_model->get_by_id($this->session->userdata('account_id'));
		
		// Delete a linked account
		if ($this->input->post('facebook_id') || $this->input->post('twitter_id') || $this->input->post('openid'))
		{
			if ($this->input->post('facebook_id')) $this->account_facebook_model->delete($this->input->post('facebook_id'));
			elseif ($this->input->post('twitter_id')) $this->account_twitter_model->delete($this->input->post('twitter_id'));
			elseif ($this->input->post('openid')) $this->account_openid_model->delete($this->input->post('openid'));
			$this->session->set_flashdata('linked_info', lang('linked_linked_account_deleted'));
			$this->twiggy->set('linked_info', lang('linked_linked_account_deleted'));
			redirect('auth/account_linked');
		}
		
		// Check for linked accounts
		$data['num_of_linked_accounts'] = 0;
		
		// Get Facebook accounts
		if ($data['facebook_links'] = $this->account_facebook_model->get_by_account_id($this->session->userdata('account_id')))
		{
			foreach ($data['facebook_links'] as $index => $facebook_link) 
			{
				$data['num_of_linked_accounts']++;
			}
		}
		
		// Get Twitter accounts
		if ($data['twitter_links'] = $this->account_twitter_model->get_by_account_id($this->session->userdata('account_id')))
		{
			$this->load->config('twitter');
			$this->load->helper('twitter');
			foreach ($data['twitter_links'] as $index => $twitter_link) 
			{
				$data['num_of_linked_accounts']++;
				$epiTwitter = new EpiTwitter($this->config->item('twitter_consumer_key'), $this->config->item('twitter_consumer_secret'), $twitter_link->oauth_token, $twitter_link->oauth_token_secret);
				$data['twitter_links'][$index]->twitter = $epiTwitter->get_usersShow(array('user_id' => $twitter_link->twitter_id));
			}
		}
		
		// Get OpenID accounts
		if ($data['openid_links'] = $this->account_openid_model->get_by_account_id($this->session->userdata('account_id'))) 
		{
			foreach ($data['openid_links'] as $index => $openid_link) 
			{
				if (strpos($openid_link->openid, 'google.com')) $data['openid_links'][$index]->provider = 'google';
				elseif (strpos($openid_link->openid, 'yahoo.com')) $data['openid_links'][$index]->provider = 'yahoo';
				elseif (strpos($openid_link->openid, 'myspace.com')) $data['openid_links'][$index]->provider = 'myspace';
				elseif (strpos($openid_link->openid, 'aol.com')) $data['openid_links'][$index]->provider = 'aol';
				else $data['openid_links'][$index]->provider = 'openid';
				
				$data['num_of_linked_accounts']++;
			}
		}
		
		

		$this->twiggy->set('num_of_linked_Accounts', $data['num_of_linked_accounts']);
		$this->twiggy->set('third_party_auth_providers', $this->config->item('third_party_auth_providers'));
  
		$this->twiggy->set('facebook_links', $data['facebook_links']);
		$this->twiggy->set('twitter_links', $data['twitter_links']);
		$this->twiggy->set('openid_links', $data['openid_links']);

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->template('account_linked')->display();
	//	$this->load->view('auth/account_linked', $data);
	}
	
}


/* End of file account_linked.php */
/* Location: ./application/modules/auth/controllers/account_linked.php */