<?php
/*
 * Sign_up Controller
 */
class Sign_up extends CI_Controller {
	
	/**
	 * Constructor
	 */
    function __construct()
    {
        parent::__construct();
		
		// Load the necessary stuff...
		$id_menu_group = 2;		
		$this->load->config('auth/account');
		$this->load->helper(array('language', 'auth/ssl', 'url'));
        $this->load->library(array('auth/authentication', 'auth/recaptcha', 'form_validation'));
		$this->load->model(array('auth/account_model', 'auth/account_details_model'));
		$this->load->language(array('general', 'auth/sign_up', 'auth/connect_third_party'));
		
		$template = "default";
		$this->twiggy->set('template', array(
			'name' 		=> $template,
			'template_path' 	=> "/application/themes/$template/_layouts/",
			'project_path'	=> '/',
			'css_path' => "/res/$template/css",
			'image_path' => "/res/$template/img/"
			), true);

		if ($this->authentication->is_signed_in()) : 
			$this->twiggy->set('account', $account);
		endif;

		
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $id_menu_group);
		$this->twiggy->set('menu', $rows, true);



	}
	
	/**
	 * Account sign up
	 *
	 * @access public
	 * @return void
	 */
	function index()
	{
		
		$current_url = 'auth/sign_up';
		$this->twiggy->set('menu_current', 'role/summary');
		$this->twiggy->set('submenu_current', $current_url);

		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);
		
		// Enable SSL?
		maintain_ssl($this->config->item("ssl_enabled"));
		
		// Redirect signed in users to homepage
		if ($this->authentication->is_signed_in()) redirect('');
		
		// Check recaptcha
		$recaptcha_result = $this->recaptcha->check();
		
		// Store recaptcha pass in session so that users only needs to complete captcha once
		if ($recaptcha_result === TRUE) $this->session->set_userdata('sign_up_recaptcha_pass', TRUE);
		
		// Setup form validation
		$this->form_validation->set_error_delimiters('<span class="field_error">', '</span>');
		$this->form_validation->set_rules(array(
			array('field'=>'sign_up_username', 'label'=>'lang:sign_up_username', 'rules'=>'trim|required|alpha_dash|min_length[2]|max_length[24]'),
			array('field'=>'sign_up_password', 'label'=>'lang:sign_up_password', 'rules'=>'trim|required|min_length[6]'),
			array('field'=>'sign_up_email', 'label'=>'lang:sign_up_email', 'rules'=>'trim|required|valid_email|max_length[160]')
		));
		
		
		$sign_up_username_error = '';
		$sign_up_email_error = '';
		$sign_up_recaptcha_error = '';
		
		// Run form validation
		if ($this->form_validation->run() === TRUE) 
		{
			// Check if user name is taken
			if ($this->username_check($this->input->post('sign_up_username')) === TRUE)
			{
				$sign_up_username_error = lang('sign_up_username_taken');
				$data['sign_up_username_error'] = lang('sign_up_username_taken');
			}
			// Check if email already exist
			elseif ($this->email_check($this->input->post('sign_up_email')) === TRUE)
			{
				$sign_up_email_error =  lang('sign_up_email_exist');
				$data['sign_up_email_error'] = lang('sign_up_email_exist');
			}
			// Either already pass recaptcha or just passed recaptcha
			elseif ( ! ($this->session->userdata('sign_up_recaptcha_pass') == TRUE || $recaptcha_result === TRUE) && $this->config->item("sign_up_recaptcha_enabled") === TRUE)
			{
				$sign_up_recaptcha_error  = $this->input->post('recaptcha_response_field') ? lang('sign_up_recaptcha_incorrect') : lang('sign_up_recaptcha_required');
				$data['sign_up_recaptcha_error'] = $this->input->post('recaptcha_response_field') ? lang('sign_up_recaptcha_incorrect') : lang('sign_up_recaptcha_required');
			}
			else 
			{
				// Remove recaptcha pass
				$this->session->unset_userdata('sign_up_recaptcha_pass');
				
				// Create user
				$user_id = $this->account_model->create($this->input->post('sign_up_username'), $this->input->post('sign_up_email'), $this->input->post('fffffffff'));
				
				// Add user details (auto detected country, language, timezone)
				$this->account_details_model->update($user_id);
				
				// Auto sign in?
				if ($this->config->item("sign_up_auto_sign_in"))
				{
					// Run sign in routine
					$this->authentication->sign_in($user_id);
				}
				redirect('auth/sign_in');
			}
		}
		
		$recaptcha = '';
		// Load recaptcha code
		if ($this->config->item("sign_up_recaptcha_enabled") === TRUE)
			if ($this->session->userdata('sign_up_recaptcha_pass') != TRUE) :
				$recaptcha = $this->recaptcha->load($recaptcha_result, $this->config->item("ssl_enabled")); 
				$data['recaptcha'] = $this->recaptcha->load($recaptcha_result, $this->config->item("ssl_enabled"));
			endif;
		
		
		
		// Load sign up view
//		$this->load->view('sign_up', isset($data) ? $data : NULL);


$this->twiggy->set('user', array(
		'name' => 'sign_up_username',
		'label' => 'Username', 
		'value' => 'my.name@email.com',
		'placeholder' => 'HelloPBX Username',
		'value' => set_value('sign_up_username'),
		'form_hint' => 'Your primary work email address.',
		'maxlength' => '24',
		'required' => 'required="required"',
		'form_error' => form_error('sign_up_username'), 
		'field_error' => $sign_up_username_error
	));


$this->twiggy->set('password', array(
		'name' => 'sign_up_password',
		'label' => 'Password', 
		'value' => '*********',
		'placeholder' => '**********',
		'value' => set_value('sign_up_password'),
		'form_hint' => 'Don\'t use something simple',
		'maxlength' => '24',
		'required' => 'required="required"',
		'form_error' => form_error('sign_up_password'), 
		'field_error' => ''
	));


$this->twiggy->set('email', array(
		'name' => 'sign_up_email',
		'label' => 'Email', 
		'value' => 'my.name@email.com',
		'placeholder' => 'Your email address',
		'value' => set_value('sign_up_email'),
		'form_hint' => 'Your primary work email address',
		'maxlength' => '24',
		'required' => 'required="required"',
		'form_error' => form_error('sign_up_email'), 
		'field_error' => $sign_up_email_error
	));


		$this->twiggy->set('uri_string', uri_string());
		$this->twiggy->set('form_post_url', '/'.'auth/sign_up');

		$this->twiggy->set('recaptcha', $recaptcha);
		$this->twiggy->set('sign_up_recaptcha_error', $sign_up_recaptcha_error);
		$this->twiggy->set('third_party_auth_providers', $this->config->item('third_party_auth_providers'));

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->template('sign_up')->display();		


	}
	
	/**
	 * Check if a username exist
	 *
	 * @access public
	 * @param string
	 * @return bool
	 */
	function username_check($username)
	{
		
		//dz This should be moved to a model (i.e. Fat Model, skinny controller
		return $this->account_model->get_by_username($username) ? TRUE : FALSE;
	}
	
	/**
	 * Check if an email exist
	 *
	 * @access public
	 * @param string
	 * @return bool
	 */
	function email_check($email)
	{
		return $this->account_model->get_by_email($email) ? TRUE : FALSE;
	}
	
}


/* End of file sign_up.php */
/* Location: ./application/modules/auth/controllers/sign_up.php */