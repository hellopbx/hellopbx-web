<?php
/*
 * Forgot_password Controller
 */
class Forgot_password extends CI_Controller {
	
	/**
	 * Constructor
	 */
    function __construct()
    {
        parent::__construct();
		
		// Load the necessary stuff...
		$id_menu_group = 2;		
		$this->load->config('auth/account');
		$this->load->helper(array('language', 'auth/ssl', 'url'));
        $this->load->library(array('auth/authentication', 'auth/recaptcha', 'form_validation'));
		$this->load->model(array('auth/account_model'));
		$this->load->language(array('general', 'auth/forgot_password'));
		
		$template = "default";
		$this->twiggy->set('template', array(
			'name' 		=> $template,
			'template_path' 	=> "/application/themes/$template/_layouts/",
			'project_path'	=> '/',
			'css_path' => "res/$template/css",
			'image_path' => "/res/$template/img/"
			), true);
		
		if ($this->authentication->is_signed_in()) : 
			$this->twiggy->set('account', $account);
		endif;

		
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $id_menu_group);
		$this->twiggy->set('menu', $rows, true);



	}
	
	/**
	 * Forgot password
	 */
	function index()
	{
		$current_url = 'auth/forgot_password';
		$this->twiggy->set('menu_current', 'role/summary');
		$this->twiggy->set('submenu_current', $current_url);

		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);


		// Enable SSL?
		maintain_ssl($this->config->item("ssl_enabled"));
		
		// Redirect signed in users to homepage
		if ($this->authentication->is_signed_in()) redirect('');
		
		// Check recaptcha
		$recaptcha_result = $this->recaptcha->check();
		
		// Store recaptcha pass in session so that users only needs to complete captcha once
		if ($recaptcha_result === TRUE) $this->session->set_userdata('forget_password_recaptcha_pass', TRUE);
		
		// Setup form validation
		$this->form_validation->set_error_delimiters('<span class="field_error">', '</span>');
		$this->form_validation->set_rules(array(
			array('field'=>'forgot_password_username_email', 'label'=>'lang:forgot_password_username_email', 'rules'=>'trim|required')
		));
	
				$forgot_password_username_email_error ="";
	
		// Run form validation
		if ($this->form_validation->run())
		{
			// User has neither already passed recaptcha nor just passed recaptcha
			if ($this->session->userdata('forget_password_recaptcha_pass') != TRUE && $recaptcha_result !== TRUE)
			{
				$this->twiggy->set('recaptcha_error', $this->input->post('recaptcha_response_field') ? lang('forgot_password_recaptcha_incorrect') : lang('forgot_password_recaptcha_required'));
//				$data['forgot_password_recaptcha_error'] = $this->input->post('recaptcha_response_field') ? lang('forgot_password_recaptcha_incorrect') : lang('forgot_password_recaptcha_required');
			}
			else
			{
				// Remove recaptcha pass
				$this->session->unset_userdata('forget_password_recaptcha_pass');
				
				$forgot_password_username_email_error = "";
				// Username does not exist
				if ( ! $account = $this->account_model->get_by_username_email($this->input->post('forgot_password_username_email')))
				{
					$forgot_password_username_email_error = lang('forgot_password_username_email_does_not_exist');
					$data['forgot_password_username_email_error'] = lang('forgot_password_username_email_does_not_exist');
				}
				// Does not manage password
				elseif ( ! $account->password)
				{
					$forgot_password_username_email_error = lang('forgot_password_does_not_manage_password');
					$data['forgot_password_username_email_error'] = lang('forgot_password_does_not_manage_password');
				}
				else
				{
					// Set reset datetime
					$time = $this->account_model->update_reset_sent_datetime($account->id);
					
					// Load email library
					$this->load->library('email');
					
					// Generate reset password url
					$password_reset_url = site_url('auth/reset_password?id='.$account->id.'&token='.sha1($account->id.$time.$this->config->item('password_reset_secret')));
					
					// Send reset password email
					$this->email->from($this->config->item('password_reset_email'), lang('reset_password_email_sender'));
					$this->email->to($account->email);
					$this->email->subject(lang('reset_password_email_subject'));
					$this->email->message($this->load->view('reset_password_email', array('username' => $account->username, 'password_reset_url' => anchor($password_reset_url, $password_reset_url)), TRUE));
					@$this->email->send();
					
					// Load reset password sent view
					$this->load->view('reset_password_sent', isset($data) ? $data : NULL);
					return;
				}
			}
		}
		
		// Load recaptcha code
		if ($this->session->userdata('forget_password_recaptcha_pass') != TRUE) {
	//		$data['recaptcha'] = $this->recaptcha->load($recaptcha_result, $this->config->item("ssl_enabled"));
			$this->twiggy->set('recaptcha', $this->recaptcha->load($recaptcha_result, $this->config->item("ssl_enabled")));
		}
	
			$this->twiggy->set('forgot_password_username_email', array(
				'value' => set_value('forgot_password_username_email'),
				'form_error' => form_error('forgot_password_username_email'), 
				'field_error' => $forgot_password_username_email_error
			));	
			
			$this->twiggy->set('forgot_password_username_email_error', $forgot_password_username_email_error);

	
		// Load forgot password view
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->template('forgot_password')->display();
		//$this->load->view('forgot_password', isset($data) ? $data : NULL);
	}
	
}

/* End of file forgot_password.php */
/* Location: ./application/modules/auth/controllers/forgot_password.php */