<?php
/*
 * Account_profile Controller
 */
class Account_profile extends CI_Controller {
	
	/**
	 * Constructor
	 */
    function __construct()
    {
        parent::__construct();
		
		$id_menu_group = 2;		
		// Load the necessary stuff...
		$this->load->config('auth/account');
		$this->load->helper(array('language', 'auth/ssl', 'url'));
        $this->load->library(array('auth/authentication', 'form_validation'));
		$this->load->model(array('auth/account_model', 'auth/account_details_model'));
		$this->load->language(array('general', 'auth/account_profile'));
		
		// These should be moved to a central location
		$template = "default";
		$this->twiggy->set('template', array(
			'name' 		=> $template,
			'template_path' 	=> "/application/themes/$template/_layouts/",
			'project_path'	=> '/',
			'css_path' => "/res/$template/css",
			'image_path' => "/res/$template/img/"
			), true);

		
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $id_menu_group);
		$this->twiggy->set('menu', $rows, true);

		if ($this->authentication->is_signed_in()) : 
			$this->twiggy->set('account', $account);
		endif;

	}
	
	/**
	 * Account profile
	 */
	function index($action = NULL)
	{
		$current_url = 'auth/account_profile';
		$this->twiggy->set('menu_current', 'role/summary');
		$this->twiggy->set('submenu_current', $current_url);

		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);


		// Enable SSL?
		maintain_ssl($this->config->item("ssl_enabled"));
		
		// Redirect unauthenticated users to signin page
		if ( ! $this->authentication->is_signed_in()) 
		{
			redirect('auth/sign_in/?continue='.urlencode(base_url().'auth/account_profile'));
		}
		
		// Retrieve sign in user
		$data['account'] = $this->account_model->get_by_id($this->session->userdata('account_id'));
		$data['account_details'] = $this->account_details_model->get_by_account_id($this->session->userdata('account_id'));
		
		// Delete profile picture
		if ($action == 'delete')
		{
			unlink(FCPATH.'resource/user/profile/'.$data['account_details']->picture);	// delete previous picture
			$this->account_details_model->update($data['account']->id, array('picture' => NULL));
			redirect('auth/account_profile');
		}
		
		// Setup form validation
		$this->form_validation->set_error_delimiters('<div class="field_error">', '</div>');
		$this->form_validation->set_rules(array(
			array('field'=>'profile_username', 'label'=>'lang:profile_username', 'rules'=>'trim|required|alpha_dash|min_length[2]|max_length[24]')
		));
		
		$profile_picture_error = "";
	
		$profile_username_error = "";
		// Run form validation
		if ($this->form_validation->run()) 
		{
			// If user is changing username and new username is already taken
			if (strtolower($this->input->post('profile_username')) != strtolower($data['account']->username) && $this->username_check($this->input->post('profile_username')) === TRUE)
			{
				$profile_username_error = lang('profile_username_taken');
				$data['profile_username_error'] = lang('profile_username_taken');
				$error = TRUE;
			}
			else
			{
				$data['account']->username = $this->input->post('profile_username');
				$this->account_model->update_username($data['account']->id, $this->input->post('profile_username'));
			}
			
			// If user has uploaded a file
			if ($_FILES['account_picture_upload']['error'] != 4) 
			{
				// Load file uploading library - http://codeigniter.com/user_guide/libraries/file_uploading.html
				$this->load->library('upload', array(
					'file_name' => md5($data['account']->id).'.jpg',
					'overwrite' => true,
					'upload_path' => FCPATH.'resource/user/profile',
					'allowed_types' => 'jpg|png|gif',
					'max_size' => '800' // kilobytes
				));
				
				/// Try to upload the file
				if ( ! $this->upload->do_upload('account_picture_upload')) 
				{
					$profile_picture_error = $this->upload->display_errors('', '');
					$data['profile_picture_error'] = $this->upload->display_errors('', '');
					$error = TRUE;
				}
				else 
				{
					// Get uploaded picture data
					$picture = $this->upload->data();
					
					// Create picture thumbnail - http://codeigniter.com/user_guide/libraries/image_lib.html
					$this->load->library('image_lib');
					$this->image_lib->clear();
					$this->image_lib->initialize(array(
						'image_library' => 'gd2',
						'source_image' => FCPATH.'resource/user/profile/'.$picture['file_name'],
						'new_image' => FCPATH.'resource/user/profile/pic_'.$picture['raw_name'].'.jpg',
						'maintain_ratio' => FALSE,
						'quality' => '100%',
						'width' => 100,
						'height' => 100
					));
					
					// Try resizing the picture
					if ( ! $this->image_lib->resize()) 
					{
						$profile_picture_error = $this->image_lib->display_errors();
						$data['profile_picture_error'] = $this->image_lib->display_errors();
						$error = TRUE;
					}
					else
					{
						$data['account_details']->picture = 'pic_'.$picture['raw_name'].'.jpg';
						$this->account_details_model->update($data['account']->id, array('picture' => $data['account_details']->picture));
					}
					
					// Delete original uploaded file
					unlink(FCPATH.'resource/user/profile/'.$picture['file_name']);
				}
			}
			
			if ( ! isset($error)) {
				$data['profile_info'] = lang('profile_updated');
			$this->twiggy->set('profile_info', lang('profile_updated'));
			}
		}
		
		// $this->load->view('auth/account_profile', $data);
		
//		$this->twiggy->set('recaptcha', $recaptcha);
		
		$this->twiggy->set('current_url', current_url());
		$this->twiggy->set('uri_string', uri_string());

		
		$this->twiggy->set('profile_username', array(
				'value' => set_value('profile_username'),
				'form_error' => form_error('profile_username'), 
				'field_error' => $profile_username_error
			));		

		$this->twiggy->set('account_picture_upload', array(
				'value' => set_value('sign_up_email'),
				'form_error' => form_error('account_picture_upload'), 
				'field_error' => ''
			));		
		
		$this->twiggy->set('profile_picture', array(
				'value' => set_value('profile_picture'),
				'form_error' => form_error('profile_picture'), 
				'field_error' => $profile_picture_error
			));		
		
		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->template('account_profile')->display();
		
	}
	
	/**
	 * Check if a username exist
	 *
	 * @access public
	 * @param string
	 * @return bool
	 */
	function username_check($username)
	{
		return $this->account_model->get_by_username($username) ? TRUE : FALSE;
	}
	
}


/* End of file account_profile.php */
/* Location: ./application/modules/auth/controllers/account_profile.php */