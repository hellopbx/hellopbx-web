<?php
/*
 * Sign_in Controller
 */
class Sign_in extends CI_Controller {
	
	/**
	 * Constructor
	 */
    function __construct()
    {
        parent::__construct();
		
		// Load the necessary stuff...
		$id_menu_group = 2;		
		$this->load->config('auth/account');
		$this->load->helper(array('language', 'auth/ssl', 'url'));
        $this->load->library(array('auth/authentication', 'auth/recaptcha', 'form_validation'));
		$this->load->model(array('auth/account_model'));
		$this->load->language(array('general', 'auth/sign_in', 'auth/connect_third_party'));
		
		// These should be moved to a central location
		$template = "default";
		$this->twiggy->set('template', array(
			'name' 		=> $template,
			'template_path' 	=> "/application/themes/$template/_layouts/",
			'project_path'	=> '/',
			'css_path' => "/res/$template/css",
			'image_path' => "/res/$template/img/"
			), true);
		
		$this->twiggy->register_function('');
	
		
		$this->load->model('menu_model');
		$this->menu_model->order_by('sort_order');
		$rows = $this->menu_model->get_many_by('id_menu_group', $id_menu_group);
		$this->twiggy->set('menu', $rows, true);


		if ($this->authentication->is_signed_in()) : 
			$this->twiggy->set('account', $account);
		endif;
	}
	
	/**
	 * Account sign in
	 *
	 * @access public
	 * @return void
	 */
	function index()
	{	
		$current_url = 'auth/sign_in';
		$this->twiggy->set('menu_current', 'role/summary');
		$this->twiggy->set('submenu_current', $current_url);

		$menu_key = 'auth/account_profile';	// Identifies the main menu to be shown
		$this->load->model('menu_sub_model');
		$this->menu_sub_model->order_by('sort_order');
		$rows = $this->menu_sub_model->get_many_by('key_parent', $menu_key);
		$this->twiggy->set('submenu', $rows);
	
	
		// Enable SSL?
		maintain_ssl($this->config->item("ssl_enabled"));
		
		// Redirect signed in users to homepage
		if ($this->authentication->is_signed_in()) redirect('');
		
		// Set default recaptcha pass
		$recaptcha_pass = $this->session->userdata('sign_in_failed_attempts') < $this->config->item('sign_in_recaptcha_offset') ? TRUE : FALSE;
		
		// Check recaptcha
		$recaptcha_result = $this->recaptcha->check();
		
		// Setup form validation
		$this->form_validation->set_error_delimiters('<span class="field_error">', '</span>');
		$this->form_validation->set_rules(array(
			array('field'=>'sign_in_username_email', 'label'=>'lang:sign_in_username_email', 'rules'=>'trim|required'),
			array('field'=>'sign_in_password', 'label'=>'lang:sign_in_password', 'rules'=>'trim|required')
		));
		
		$sign_in_error = '';
		$sign_in_username_email_error = '';
		$sign_in_recaptcha_error = '';
		// Run form validation
		if ($this->form_validation->run() === TRUE) 
		{
			// Get user by username / email
			if ( ! $user = $this->account_model->get_by_username_email($this->input->post('sign_in_username_email'))) 
			{
				// Username / email doesn't exist
				$sign_in_username_email_error =  lang('sign_in_username_email_does_not_exist');
				$data['sign_in_username_email_error'] = lang('sign_in_username_email_does_not_exist');
			}
			else
			{
				// Either don't need to pass recaptcha or just passed recaptcha
				if ( ! ($recaptcha_pass === TRUE || $recaptcha_result === TRUE) && $this->config->item("sign_in_recaptcha_enabled") === TRUE)
				{
					$sign_in_recaptcha_error = $this->input->post('recaptcha_response_field') ? lang('sign_in_recaptcha_incorrect') : lang('sign_in_recaptcha_required');
					$data['sign_in_recaptcha_error'] = $this->input->post('recaptcha_response_field') ? lang('sign_in_recaptcha_incorrect') : lang('sign_in_recaptcha_required');
				}
				else
				{
					// Check password
					if ( ! $this->authentication->check_password($user->password, $this->input->post('sign_in_password')))
					{
						// Increment sign in failed attempts
						$this->session->set_userdata('sign_in_failed_attempts', (int)$this->session->userdata('sign_in_failed_attempts')+1);
						$sign_in_error = lang('sign_in_combination_incorrect');
						$data['sign_in_error'] = lang('sign_in_combination_incorrect');
					}
					else
					{
						// Clear sign in fail counter
						$this->session->unset_userdata('sign_in_failed_attempts');
						
						// Run sign in routine
						$this->authentication->sign_in($user->id, $this->input->post('sign_in_remember'));
					}
				}
			}
		}
		
		// Load recaptcha code
		if ($this->config->item("sign_in_recaptcha_enabled") === TRUE) 
			if ($this->config->item('sign_in_recaptcha_offset') <= $this->session->userdata('sign_in_failed_attempts')) {
				$data['recaptcha'] = $this->recaptcha->load($recaptcha_result, $this->config->item("ssl_enabled"));
				$this->twiggy->set('recaptcha', $this->recaptcha->load($recaptcha_result, $this->config->item("ssl_enabled")));
			}
		// Load sign in view
//		$this->load->view('sign_in', isset($data) ? $data : NULL);
	
	$this->twiggy->set('uri_string', uri_string());

		$this->twiggy->set('sign_in_error', $sign_in_error);

		$this->twiggy->set('sign_in_username_email', array(
				'value' => set_value('sign_in_username_email'),
				'form_error' => form_error('sign_in_username_email'), 
				'field_error' => $sign_in_username_email_error
			));	
			
			$this->twiggy->set('sign_in_password', array(
				'value' => set_value('sign_in_password'),
				'form_error' => form_error('sign_in_password'), 
				'field_error' => $sign_in_error
			));	
			
			$this->twiggy->set('sign_in_remember', array(
				'value' => $this->input->post('sign_in_remember')
			));	

		$this->twiggy->set('continue', $this->input->get('continue')?'/?continue='.urlencode($this->input->get('continue')):'');
		$this->twiggy->set('sign_in_recaptcha_error', $sign_in_recaptcha_error);

		$this->twiggy->set('sign_in_page_name', lang('sign_in_page_name'));
		$this->twiggy->set('sign_in_third_party_heading', lang('sign_in_third_party_heading'));
		$this->twiggy->set('third_party_auth_providers', $this->config->item('third_party_auth_providers'));

		$this->auth->check_auth_view("");	// Check for view permissions
		$this->twiggy->template('sign_in')->display();
			
		
		
		
	}
	
}


/* End of file sign_in.php */
/* Location: ./application/modules/auth/controllers/sign_in.php */