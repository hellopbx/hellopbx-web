<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| reCAPTCHA
|--------------------------------------------------------------------------
|
| reCAPTCHA PHP Library - http://recaptcha.net/plugins/php/
|
| recaptcha_theme	'red' | 'white' | 'blackglass' | 'clean' | 'custom'
*/
$config['recaptcha_public_key']		= "6LeKX9kSAAAAAIZyB2y9GiYW_ZoGuHmWZOVl6eJm";
$config['recaptcha_private_key']		= "6LeKX9kSAAAAAIR1Ag-Q9svnJeLI4AMlTY-xy6hp";
$config['recaptcha_theme']				= "clean";


/* End of file recaptcha.php */
/* Location: ./application/modules/auth/config/recaptcha.php */