<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iptocountry_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_iptocountry";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."ref_iptocountry" (
"id" int8 DEFAULT nextval('ref_iptocountry_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"ip_from" int8 NOT NULL,
"ip_to" int8 NOT NULL,
"country_code" char(2) NOT NULL
)
WITH (OIDS=FALSE)EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file iptocountry_model.php */
/* Location: ./application/models/iptocountry_model.php */