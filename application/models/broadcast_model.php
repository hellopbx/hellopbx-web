<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broadcast_model extends MY_Model {

	public $rules;
	public $script_upload = "/broadcast/upload_content";
	public $script_check = "/broadcast/check_exists";


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_broadcast";
	$this->rules = $this->get_rules($this->get_template_base());
	

}

/*******************************************************************************/
/**
 * Delete an endpoint and all child table records.
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function delete($delete_id){
	$success = true;
	$this->load->model('broadcast_destination_model');
	$success = $this->broadcast_destination_model->delete_for_parent($delete_id);
	return  parent::delete($delete_id);
	}



/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function insert_advanced($id = NULL){

		if(empty($id)):
			$id = $this->new_guid();
		endif;
		$columns = $this->db->list_fields($this->_table);

		foreach ($_POST as $key => $value):
			if( in_array($key, $columns)):
				$data[$key] = empty($value) ? NULL : $value;
			endif;
		endforeach;

//		$new_guid = $this->new_guid();
		$data['id'] = $id;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = '[Incomplete in Code]';

		$this->validate = $this->rules;
		return $this->insert($data);
	}



/*******************************************************************************/
/**
 * Display a Trunk edit screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function update_advanced($id){
	$columns = $this->db->list_fields($this->_table);
	foreach ($_POST as $key => $value):
		if(substr($key, 0,1) != "_"):
			
			if($key != 'id'):
				if( in_array($key, $columns)):
					$data[$key] = empty($value) ? NULL : $value;
				endif;
			endif;
		endif;
		
	endforeach;
	
	$data["mac"] = trim(str_replace(array(":", "-", ""), "", $data["mac"]));

	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = '[Incomplete in Code]';

	$this->validate = $this->rules;
	$result = $this->update($id, $data);
	
	// Update endpoint_sip_identity
	
	$this->load->model('broadcast_destination_model');
	return $result and $this->broadcast_destination_model->update_advanced_children($id);
	}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function insert_free_text($line) {

	$this->load->model('phone_mac_model');
	$phone_type = $this->phone_mac_model->match($line);

	$data = array();
	$data['phone_name'] = $phone_type['phone_name'];
	$data['name'] = 'Unconfigured Phone';
	$data['description'] = 'Phone is loaded from a free text import';
	$data['mac'] = $line;

	$new_guid = $this->new_guid();
	$data['id'] = $new_guid;
	$data['created_at'] = date('Y-m-d H:i:s');
	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = '[Incomplete in Code]';

	$this->validate = $this->rules;
	
	$insert_result = $this->insert($data);

	if ($insert_result){
		$this->load->model('endpoint_sip_identity_model');
		$this->endpoint_sip_identity_model->insert_free_text($new_guid);
	}
	return $insert_result;
}



/*******************************************************************************/
/**
 * Adds get form data with form values
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_form_data($id_template, $form_value_list, $id, $type =NULL){
		//  This is normally dynamic based on endpoint_sip_template and should be added to this equation in the future
		$form_item_list = $this->get_template_base();
		$timestamp = time();
	
		foreach ($form_item_list as &$item):
	
			if($item->field_type == "file"):
				$item->timestamp = $timestamp;
				$item->timestamp_md5 = md5('unique_salt' . $timestamp);
				$item->script_check = $this->script_check . "/" . $id;
				

				if(strlen($form_value_list[$item->name]["value"]) > 0):
					$file_json = json_decode($form_value_list[$item->name]["value"]);
					$item->mime = $file_json->mime;
					$item->file_name = $file_json->file_name;
				endif;
				
				$item->valid_decoded = get_object_vars(json_decode($item->valid_values));
				
				switch($type){
					case "fax":
						$item->script_upload = $this->script_upload . "_fax/" . $id;
						break;
					case "voice":
						$item->script_upload = $this->script_upload . "_voice/" . $id;
						break;
					case "text";
						$item->script_upload = $this->script_upload . "_text/" . $id;
						break;
					default:
						$item->script_upload = $this->script_upload . "_all/" . $id;
						break;
				}
					

			endif;


			if($item->field_type == "list"):
				$item->valid_values_decoded = get_object_vars(json_decode($item->valid_values));
			endif;
			// set the value to 1) posted value, or 2) db value or 3) default value or 4) empty string
			
			$item->value = set_value($item->name, ($form_value_list[$item->name]["value"] != NULL ? $form_value_list[$item->name]["value"] : ($item->default_value != NULL ? $item->default_value : '')));
			$item->field_error = $form_value_list[$item->name]["field_error"];
			$item->field_error2 = $form_value_list[$item->name]["field_error2"];
		endforeach;
	
		$this->twiggy->set("fil", $form_item_list);
	}



/*******************************************************************************/
/**
 * Updates the content file path when a file such as a  pdf, tif is uploaded for a broadcast fax.
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function update_send_item_location($id, $file_name, $description = NULL){
	
	$data["send_item_location"] = $file_name;
	
	// Used for debugging
	if (!empty($description)):
		$data["description"] = $description;
	endif;

	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = '[Incomplete in Code]';

	$result = $this->update($id, $data);

}



/*******************************************************************************/
/**
 * Gets form fields
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 public function get_template_base(){
	$form_item_list = array (
     (object)array  (
            "name" => "name",
            "default_value" => "",
            "label" => "Job Name",
            "placeholder" => "February Product Announcement",
            "form_hint" => "A descriptive name for this broadcast job.",
            "max_size" => 50,
            "min_size" => 4,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "broadcast.name",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "description",
            "default_value" => "",
            "label" => "Description",
            "placeholder" => "",
            "form_hint" => "Long description of this job.",
            "max_size" => 250,
            "min_size" => 4,
            "is_required" => 1,
            "sort_order" => 20,
            "reference" => "broadcast.description",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 1,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "start_at",
            "default_value" => "",
            "label" => "Start Date",
            "placeholder" => "",
            "form_hint" => "Date and Time when the job should initiate",
            "max_size" => NULL,
            "min_size" => NULL,
            "is_required" => 0,
            "sort_order" => 10,
            "reference" => "broadcast.start_at",
            "field_type" => "datetime",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "channels_max",
            "default_value" => "",
            "label" => "Max Channels",
            "placeholder" => "",
            "form_hint" => "Maximum number of channels",
            "max_size" => NULL,
            "min_size" => NULL,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "broadcast.channels_max",
            "field_type" => "number",
            "greater_than" => 0,
            "less_than" => 100,
            "is_numeric" => 1,
            "is_unique" => NULL,
            "is_integer" => 1,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "broadcast_type",
            "default_value" => "",
            "label" => "Type",
            "placeholder" => "",
            "form_hint" => "The type of broadcast is for this material",
            "max_size" => NULL,
            "min_size" => NULL,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "broadcast.braodcast_type",
            "field_type" => "list",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => '{"fax":"Fax","audio":"Audio","text":"Text"}',
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "send_item_location",
            "default_value" => "",
            "label" => "Content to Send",
            "placeholder" => "",
            "form_hint" => "Use tiff files for fax, wav for Voice and txt for Text",
            "max_size" => NULL,
            "min_size" => NULL,
            "is_required" => 0,
            "sort_order" => 10,
            "reference" => "broadcast.send_item_location",
            "field_type" => "file",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        )
	);
	
	return $form_item_list;
	}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function Create_table(){
$schema_pgsql = <<<EOD


EOD;
	}





}
 
 /* End of file endpoint_sip_model_model.php */
/* Location: ./application/models/endpoint_sip_model.php */