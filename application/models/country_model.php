<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_country";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."ref_country" (
"id" int8 DEFAULT nextval('ref_country_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"alpha2" char(2) NOT NULL,
"alpha3" char(3) NOT NULL,
"numeric" varchar(3) NOT NULL,
"country" varchar(80) NOT NULL
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Account_model.php */
/* Location: ./application/models/account_model.php */