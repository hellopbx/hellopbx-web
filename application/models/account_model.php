<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_account";
}
public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."auth_account" (
"id" int8 DEFAULT nextval('auth_account_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"username" varchar(24) NOT NULL,
"email" varchar(160) NOT NULL,
"password" varchar(60),
"createdon" timestamp(6) NOT NULL,
"verifiedon" timestamp(6),
"lastsignedinon" timestamp(6),
"resetsenton" timestamp(6),
"deletedon" timestamp(6),
"suspendedon" timestamp(6)
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}

 /* End of file Account_model.php */
/* Location: ./application/models/account_model.php */