<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Broadcast_destination_model extends MY_Model {


public $rules;
 
/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_broadcast_destination";

 		$this->rules = array(
			array('field'=>'phone_name', 'label'=>'Phone Type', 'rules'=>'trim|required|min_length[6]'),
			array('field'=>'mac', 'label'=>'MAC Address', 'rules'=>'trim|min_length[12]|max_length[12]|unique[pbx_endpoint_sip.mac]'),
			array('field'=>'name', 'label'=>'Location', 'rules'=>'trim|min_length[6]|max_length[50]|required'),
			array('field'=>'description', 'label'=>'Emergency Address', 'rules'=>'trim|min_length[6]|max_length[250]|required')
		);


}

/*******************************************************************************/
/**
 * Delete an endpoint and all child table records.
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function delete($delete_id){
	$success = true;
	$this->load->model('endpoint_sip_identity_model');
	$success = $this->endpoint_sip_identity_model->delete_for_endpoint($delete_id);
	parent::delete($delete_id);
	}
	
	
	
/*******************************************************************************/
/**
 * Delete all identities for an Endpoint
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function delete_for_parent($id_parent) {
		$this->db->where('id_parent', $id_parent);
		$this->db->delete($this->_table);
	}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function insert_advanced(){


		$columns = $this->db->list_fields($this->_table);

		foreach ($_POST as $key => $value):
			if( in_array($key, $columns)):
				$data[$key] = empty($value) ? NULL : $value;
			endif;
		endforeach;

		$new_guid = $this->new_guid();
		$data['id'] = $new_guid;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = '[Incomplete in Code]';

		$this->validate = $this->rules;
		return $this->insert($data);
	}



/*******************************************************************************/
/**
 * Display a Trunk edit screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function update_advanced($id){
	$columns = $this->db->list_fields($this->_table);
	foreach ($_POST as $key => $value):
		if(substr($key, 0,1) != "_"):
			
			if($key != 'id'):
				if( in_array($key, $columns)):
					$data[$key] = empty($value) ? NULL : $value;
				endif;
			endif;
		endif;
		
	endforeach;
	
	$data["mac"] = trim(str_replace(array(":", "-", ""), "", $data["mac"]));

	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = '[Incomplete in Code]';

	$this->validate = $this->rules;
	$result = $this->update($id, $data);
	
	// Update endpoint_sip_identity
	
	$this->load->model('endpoint_sip_identity_model');
	return $result and $this->endpoint_sip_identity_model->update_advanced_children($id);
	}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function insert_free_text($line) {

	$this->load->model('phone_mac_model');
	$phone_type = $this->phone_mac_model->match($line);

	$data = array();
	$data['phone_name'] = $phone_type['phone_name'];
	$data['name'] = 'Unconfigured Phone';
	$data['description'] = 'Phone is loaded from a free text import';
	$data['mac'] = $line;

	$new_guid = $this->new_guid();
	$data['id'] = $new_guid;
	$data['created_at'] = date('Y-m-d H:i:s');
	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = '[Incomplete in Code]';

	$this->validate = $this->rules;
	
	$insert_result = $this->insert($data);

	if ($insert_result){
		$this->load->model('endpoint_sip_identity_model');
		$this->endpoint_sip_identity_model->insert_free_text($new_guid);
	}
	return $insert_result;
}



/*******************************************************************************/
/**
 * Adds get form data with form values
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_form_data($id_template, $form_value_list){
		//  This is normally dynamic based on endpoint_sip_template and should be added to this equation in the future
		$form_item_list = $this->get_template_base();
		foreach ($form_item_list as &$item):
			$item->valid_values = get_object_vars(json_decode($item->valid_values));
			// set the value to 1) posted value, or 2) db value or 3) default value or 4) empty string
			$item->value = set_value($item->name, ($form_value_list[$item->name]["value"] != NULL ? $form_value_list[$item->name]["value"] : ($item->default_value != NULL ? $item->default_value : '')));
			$item->field_error = $form_value_list[$item->name]["field_error"];
			$item->field_error2 = $form_value_list[$item->name]["field_error2"];
	endforeach;
	
		$this->twiggy->set("fil", $form_item_list);
	}





/*******************************************************************************/
/**
 * Gets form fields
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 public function get_template_base(){
	$template = array (
     (object)array  (
            "name" => "name",
            "default_value" => "",
            "label" => "Location",
            "placeholder" => "Desk Phone",
            "form_hint" => "Your descriptive name for this phone.  It could be desk phone, cell phone, or home phone",
            "max_size" => 30,
            "min_size" => 6,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip.name",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "description",
            "default_value" => "",
            "label" => "Emergency Address",
            "placeholder" => "2020 Center Street, Calgary, AB",
            "form_hint" => "Use actual address for Emergency purposes",
            "max_size" => 250,
            "min_size" => 6,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip.description",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 1,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "phone_name",
            "default_value" => "",
            "label" => "Phone Type",
            "placeholder" => "Snom 821",
            "form_hint" => "Descriptive name for this phone hardware or software.  Usually automatic",
            "max_size" => 30,
            "min_size" => 6,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip.phone_name",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "mac",
            "default_value" => "",
            "label" => "MAC Address",
            "placeholder" => "00:AA:BB:CC:DD:EE",
            "form_hint" => "Mac address for your hardware voice.  If your device is software, this can be left blank.",
            "max_size" => 12,
            "min_size" => 12,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip.mac",
            "field_type" => "mac",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => "pbx_endpoint_sip.mac",
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        )
	);
	
	return $template;
	}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "pbx_endpoint_sip" (
	"id" uuid NOT NULL,
	"created_at" timestamp(6) NOT NULL DEFAULT now(),
	"updated_at" timestamp(6) NOT NULL DEFAULT now(),
	"updated_by" varchar(50) NOT NULL DEFAULT 'Not Set'::character varying,
	"phone_name" varchar(250) NOT NULL,
	"mac" char(12),
	"name" varchar(50) NOT NULL,
	"description" varchar(250) NOT NULL
;)

EOD;
	}





}
 
 /* End of file endpoint_sip_model_model.php */
/* Location: ./application/models/endpoint_sip_model.php */