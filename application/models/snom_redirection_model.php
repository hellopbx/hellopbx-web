<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Snom_redirection_model extends MY_Model {

protected $username = "helia";
protected $password = "loh1rohcaoPe";
public $provisioning_url;

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */
 public function __construct() { 
 	$xmlrpc_server_url = "https://" . $this->username . ":" . $this->password . "@provisioning.snom.com:8083/xmlrpc";
	parent::__construct();
	$this->load->library('xmlrpc');
	$this->xmlrpc->server($xmlrpc_server_url, 8083);
}

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */
public function register_phone($mac){
	$this->xmlrpc->method('redirect.registerPhone');

	$request = array($mac, $provisioning_url);
	$this->xmlrpc->request($request);

	if ( ! $this->xmlrpc->send_request()):
	    echo $this->xmlrpc->display_error();
	endif;
}


/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */
public function deregister_phone($mac) {
	$this->xmlrpc->method('redirect.deregisterPhone');

	$request = array($mac);
	$this->xmlrpc->request($request);

	if ( ! $this->xmlrpc->send_request()):
    	echo $this->xmlrpc->display_error();
	endif;
}

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */
public function list_phones($phone_type){
	$this->xmlrpc->method("redirect.listPhones");

	$request = array($phone_type, "None");

//	$request = array($phone_type, $provisioning_url);
	$this->xmlrpc->request($request);

	if ( ! $this->xmlrpc->send_request()):
	    echo $this->xmlrpc->display_error();
	endif;
}

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */
public function check_phone($mac){
	$this->xmlrpc->method('redirect.checkPhone');
	$request = array($mac);
	$this->xmlrpc->request($request);

	if ( ! $this->xmlrpc->send_request()):
	    echo $this->xmlrpc->display_error();
	endif;
}

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */
public function register_phone_list($mac_list){
	$this->xmlrpc->method("redirect.registerPhoneList");

	$request = array($mac_list, $this->provisioning_url);
	$this->xmlrpc->request($request);

	if ( ! $this->xmlrpc->send_request()):
	    echo $this->xmlrpc->display_error();
	endif;
}

/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */
public function deregister_phone_list($mac_list){
	$this->xmlrpc->method("redirect.deregisterPhoneList");

	$request = array($mac_list);
	$this->xmlrpc->request($request);

	if ( ! $this->xmlrpc->send_request()):
	    echo $this->xmlrpc->display_error();
	endif;
}


/*******************************************************************************/
/**
 * Display a Trunk view screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */

	function makeRequest($request=null,$output=true)
	{
			if(!is_array($request) or empty($request) ) $request = array(); 
			
			//performs the request
			$this->xmlrpc->request($request);
			
			//handles the request
			if ( ! $this->xmlrpc->send_request())
			{
					echo $this->xmlrpc->display_error();
			}
			else
			{
					$res = $this->xmlrpc->display_response();
					if($output)
					{
							echo '<pre>';
				print_r($res);
							echo '</pre>';
					} else {
							return $res;
					}
			}
	}

}
 
 /* End of file Snom_redirection_model.php */
/* Location: ./application/models/Snom_redirection_model.php */