<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Endpoint_pstn_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_endpoint_pstn";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."pbx_endpoint_pstn" (
"id" uuid NOT NULL primary key,
"profile_name" text,
"pstn_number" text,
"dialplan" text,
"context" text,
"enabled" text,
"description" text
)
WITH (OIDS=FALSE)

EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}

}
 
 /* End of file Account_model.php */
/* Location: ./application/models/Endpoint_pstn_model.php */