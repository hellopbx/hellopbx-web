<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provision_sip_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_provision_sip";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."pbx_provision_sip" (
"id" uuid NOT NULL PRIMARY KEY,
"mac_address" text,
"label" text,
"vendor" text,
"model" text,
"firmware_version" text,
"provision_enable" text,
"template" text,
"username" text,
"password" text,
"time_zone" text,
"description" text
)
WITH (OIDS=FALSE);
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}

}
 
 /* End of file provision_sip_model.php */
/* Location: ./application/models/provision_sip_model.php */