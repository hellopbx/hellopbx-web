<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_details_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_account";
}
public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."auth_account_details" (
"id" int8 DEFAULT nextval('auths_account_details_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"account_id" int8 NOT NULL,
"fullname" varchar(160) DEFAULT NULL::character varying,
"firstname" varchar(80) DEFAULT NULL::character varying,
"lastname" varchar(80) DEFAULT NULL::character varying,
"dateofbirth" date,
"gender" char(1) DEFAULT NULL::bpchar,
"postalcode" varchar(40) DEFAULT NULL::character varying,
"country" char(2) DEFAULT NULL::bpchar,
"language" char(2) DEFAULT NULL::bpchar,
"timezone" varchar(40) DEFAULT NULL::character varying,
"picture" varchar(240) DEFAULT NULL::character varying
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}

 /* End of file Account_details_model.php */
/* Location: ./application/models/account_details_model.php */