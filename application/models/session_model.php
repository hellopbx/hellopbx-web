<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Session_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "ci_session";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "ci_session" (
"id" int8 DEFAULT nextval('ci_session_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"session_id" varchar(40) NOT NULL,
"ip_address" varchar(45) NOT NULL,
"user_agent" varchar(255) NOT NULL,
"last_activity" int4 NOT NULL,
"user_data" text NOT NULL
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file session_model.php */
/* Location: ./application/models/session_model.php */