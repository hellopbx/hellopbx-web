<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Zoneinfo_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_zoneinfo";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."ref_zoneinfo" (
"id" int8 DEFAULT nextval('ref_zoneinfo_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"zoneinfo" varchar(40) NOT NULL,
"offset" varchar(16) DEFAULT NULL::character varying,
"summer" varchar(16) DEFAULT NULL::character varying,
"country" char(2) NOT NULL
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Zoneinfo_model.php */
/* Location: ./application/models/zoneinfo_model.php */