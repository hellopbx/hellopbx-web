<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_sub_role_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "menu_sub_role";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."menu_sub_role" (
"id" int4 NOT NULL PRIMARY KEY,
"id_menu_sub" int4,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"updated_by" varchar(100)
)
WITH (OIDS=FALSE)

EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Account_model.php */
/* Location: ./application/models/menu_sub_model.php */