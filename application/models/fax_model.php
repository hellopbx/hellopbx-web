<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fax_model extends MY_Model {

public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_currency";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."ref_currency" (
"id" int8 DEFAULT nextval('ref_currency_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"alpha" char(3) NOT NULL,
"numeric" varchar(3) DEFAULT NULL::character varying,
"currency" varchar(80) NOT NULL
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Currency_model.php */
/* Location: ./application/models/currency_model.php */