<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Dial plan item types for things like extensions, features, auto attendants
class Dpitem_type_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_dpitem_step";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE pbx_dpitem_type (
ref uuid PRIMARY KEY,
create_at timestamp,
updated_at timestamp,
updated_by varchar(100),
name varchar(50),
description varchar(200),
);
EOD;


$schema_sqllite = <<<EOD
CREATE TABLE pbx_dpitem_step (
ref uuid PRIMARY KEY,
ref_dpitem_uuid,
create_at timestamp,
updated_at timestamp,
updated_by varchar(100),
tag text,
type text,
data text,
break text,
inline text,
group numeric,
sort_order numeric);
EOD;

$default_data = <<<EOD

EOD;
	}
}
 
 /* End of file Dpitem_type_model.php */
/* Location: ./application/models/Dpitem_type_model.php */