<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trunk_sip_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_trunk_sip";
}


public function update_advanced($id){
	
				$rules = array(
			array('field'=>'gateway', 'label'=>'Gateway', 'rules'=>'trim|required|min_length[6]'),
			array('field'=>'username', 'label'=>'Username', 'rules'=>'trim|min_length[6]'),
			array('field'=>'password', 'label'=>'Password', 'rules'=>'trim|min_length[6]'),
			array('field'=>'distinct_to', 'label'=>'"Distinct To"', 'rules'=>'trim|min_length[6]'),
			array('field'=>'auth_username', 'label'=>'Auth Username', 'rules'=>'trim|min_length[6]'),
			array('field'=>'realm', 'label'=>'Realm', 'rules'=>'trim|min_length[6]'),
			array('field'=>'from_user', 'label'=>'From User', 'rules'=>'trim|min_length[6]'),
			array('field'=>'from_domain', 'label'=>'From Domain', 'rules'=>'trim|min_length[6]'),
			array('field'=>'proxy', 'label'=>'Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'register_proxy', 'label'=>'Register Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'outbound_proxy', 'label'=>'Outbound Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'expire_seconds', 'label'=>'Expiry Seconds', 'rules'=>'trim|integer'),
			array('field'=>'register', 'label'=>'Register', 'rules'=>'trim|min_length[6]'),
			array('field'=>'register_transport', 'label'=>'Register Transport', 'rules'=>'trim|min_length[6]'),
			array('field'=>'retry_seconds', 'label'=>'Retry Seconds', 'rules'=>'trim|valid_ip'),
			array('field'=>'extension', 'label'=>'Extension', 'rules'=>'trim|min_length[6]'),
			array('field'=>'ping', 'label'=>'Ping', 'rules'=>'trim|valid_ip'),
			array('field'=>'caller_id_in_from', 'label'=>'Caller ID in From', 'rules'=>'trim|min_length[6]'),
			array('field'=>'supress_cng', 'label'=>'Supress Cng', 'rules'=>'trim|min_length[6]'),
			array('field'=>'sip_cid_type', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'extension_in_contact', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'context', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'profile', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'enabled', 'label'=>'', 'rules'=>'required'),
			array('field'=>'description', 'label'=>'', 'rules'=>'trim|required|min_length[6]')
		);
		//);
	
	
		
		$columns = $this->db->list_fields($this->_table);

		foreach ($_POST as $key => $value):
			if($key != 'id'):
			if( in_array($key, $columns)):
				$data[$key] = empty($value) ? NULL : $value;
				endif;
			endif;
		endforeach;


		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = '[Incomplete in Code]';

		$this->validate = $rules;
		return $this->update($id, $data);

	
	
	
	
	}


public function insert_advanced(){
			$rules = array(
			array('field'=>'gateway', 'label'=>'Gateway', 'rules'=>'trim|required|min_length[6]|is_unique[pbx_trunk_sip.gateway]'),
			array('field'=>'username', 'label'=>'Username', 'rules'=>'trim|min_length[6]'),
			array('field'=>'password', 'label'=>'Password', 'rules'=>'trim|min_length[6]'),
			array('field'=>'distinct_to', 'label'=>'"Distinct To"', 'rules'=>'trim|min_length[6]'),
			array('field'=>'auth_username', 'label'=>'Auth Username', 'rules'=>'trim|min_length[6]'),
			array('field'=>'realm', 'label'=>'Realm', 'rules'=>'trim|min_length[6]'),
			array('field'=>'from_user', 'label'=>'From User', 'rules'=>'trim|min_length[6]'),
			array('field'=>'from_domain', 'label'=>'From Domain', 'rules'=>'trim|min_length[6]'),
			array('field'=>'proxy', 'label'=>'Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'register_proxy', 'label'=>'Register Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'outbound_proxy', 'label'=>'Outbound Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'expire_seconds', 'label'=>'Expiry Seconds', 'rules'=>'trim|integer'),
			array('field'=>'register', 'label'=>'Register', 'rules'=>'trim|min_length[6]'),
			array('field'=>'register_transport', 'label'=>'Register Transport', 'rules'=>'trim|min_length[6]'),
			array('field'=>'retry_seconds', 'label'=>'Retry Seconds', 'rules'=>'trim|valid_ip'),
			array('field'=>'extension', 'label'=>'Extension', 'rules'=>'trim|min_length[6]'),
			array('field'=>'ping', 'label'=>'Ping', 'rules'=>'trim|valid_ip'),
			array('field'=>'caller_id_in_from', 'label'=>'Caller ID in From', 'rules'=>'trim|min_length[6]'),
			array('field'=>'supress_cng', 'label'=>'Supress Cng', 'rules'=>'trim|min_length[6]'),
			array('field'=>'sip_cid_type', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'extension_in_contact', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'context', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'profile', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'enabled', 'label'=>'', 'rules'=>'trim'),
			array('field'=>'description', 'label'=>'', 'rules'=>'trim|required|min_length[6]')
		);
		//);

		$new_guid = $this->new_guid();
		
		// This is how its done in the auth module.  "If not null, use post value else use db value, else use ''"
// $this->input->post('settings_dob_year') ? $this->input->post('settings_dob_year') : (isset($account_details->dob_year) ? $account_details->dob_year : '')

		$columns = $this->db->list_fields($this->_table);

		foreach ($_POST as $key => $value):
			if( in_array($key, $columns)){
				$data[$key] = empty($value) ? NULL : $value;
				
	//			$data[$key] = $value;
			}
		endforeach;

//print_r($data);

		$data['id'] = $new_guid;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = '[Incomplete in Code]';

		$this->validate = $rules;
		return $this->insert($data);
	}







public function Create_table(){
	$schema_pgsql = <<<EOD
CREATE TABLE "public"."pbx_trunk_sip" (
"id" uuid NOT NULL,
"created_at" timestamp(6) NOT NULL,
"updated_at" timestamp(6) NOT NULL,
"updated_by" varchar(100) NOT NULL,
"is_template" bool NOT NULL,
"gateway" varchar(250),
"username" varchar(250),
"password" varchar(250),
"distinct_to" varchar(250),
"auth_username" varchar(250),
"realm" varchar(250),
"from_user" varchar(250),
"from_domain" varchar(250),
"proxy" varchar(250),
"register_proxy" varchar(250),
"outbound_proxy" varchar(250),
"expire_seconds" numeric,
"register" varchar(250),
"register_transport" varchar(250),
"retry_seconds" numeric,
"extension" varchar(250),
"ping" varchar(250),
"caller_id_in_from" varchar(250),
"supress_cng" varchar(250),
"sip_cid_type" varchar(250),
"extension_in_contact" varchar(250),
"context" varchar(250),
"profile" varchar(250),
"enabled" varchar(250),
"description" varchar(250)
)
WITH (OIDS=FALSE)
EOD;


	$schema_sqllite = <<<EOD
	EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file trunk_sip_model.php */
/* Location: ./application/models/trunk_sip_model.php */