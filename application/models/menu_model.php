<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "www_menu";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."www_menu" (
"id" int4 NOT NULL,
"created_at" timestamp(6) NOT NULL,
"updated_at" timestamp(6) NOT NULL,
"updated_by" timestamp(6) NOT NULL,
"name" varchar(50) NOT NULL,
"key" varchar(250) NOT NULL,
"sort_order" int2
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Menu_model.php */
/* Location: ./application/models/menu_model.php */