<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//  Dialplan Items are instructions identified by a call string and have multiple steps associated (dpitem_step)  to perform functionality
class Dp_item_model extends MY_Model {

public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_dp_item";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE pbx_dp_item (
	ref uuid PRIMARY KEY,
	ref_dialplan uuid,
	ref_dpitem_type uuid,
	create_at timestamp,
	updated_at timestamp,
	updated_by varchar(100),
	context text,
	name text,
	number text,
	continue text,
	sort_order numeric,
	enabled text,
	description text);
EOD;


$schema_sqllite = <<<EOD
CREATE TABLE pbx_dp_item (
ref text PRIMARY KEY,
ref_dialplan text,
create_at timestamp,
updated_at timestamp,
updated_by varchar(100),
context text,
name text,
number text,
continue text,
order numeric,
enabled text,
description text);
EOD;

$default_data = <<<EOD

EOD;
	}



}
 
 /* End of file dp_item.php */
/* Location: ./application/models/account/dp_item_model.php */