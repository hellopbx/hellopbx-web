<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_language";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."ref_language" (
"id" int8 DEFAULT nextval('ref_language_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"one" char(2) NOT NULL,
"two" char(3) NOT NULL,
"language" varchar(120) NOT NULL,
"native" varchar(80) DEFAULT NULL::character varying
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Account_model.php */
/* Location: ./application/models/language_model.php */