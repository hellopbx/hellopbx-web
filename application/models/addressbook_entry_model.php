<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Addressbook_entry_model extends MY_Model {
 
public $rules;
 
/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_addressbook_entry";
 		$this->rules = array(
			array('field'=>'name', 'label'=>'Phone entry name', 'rules'=>'trim|min_length[6]|max_length[50]|required'),
			array('field'=>'pstn', 'label'=>'Phone entry number', 'rules'=>'trim|min_length[11]|max_length[11]|required')
		);
	}
	
	
	
	
/*******************************************************************************/
/**
 * Display a Trunk edit screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function update_advanced($id){
	$columns = $this->db->list_fields($this->_table);
	foreach ($_POST as $key => $value):
		if(substr($key, 0,1) != "_"):
			
			if($key != 'id'):
				if( in_array($key, $columns)):
					$data[$key] = empty($value) ? NULL : $value;
				endif;
			endif;
		endif;
		
	endforeach;
	
	$data["pstn"] = $this->clean_nanpa($data["pstn"]);

	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = '[Incomplete in Code]';

	$this->validate = $this->rules;
	return $this->update($id, $data);
	
	}

/*******************************************************************************/
/**
 * Gets form fields
 *
 * @access	public
 * @param	stringget_form_data
 * @return	string
 */	
 public function get_template_base(){
	$template = array (
     (object)array  (
            "name" => "name",
            "default_value" => "",
            "label" => "Entry Name",
            "placeholder" => "Helia Support Number",
            "form_hint" => "Add here a short description of the phone number.",
            "max_size" => 30,
            "min_size" => 6,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "addressbook_entry.name",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "pstn",
            "default_value" => "",
            "label" => "PSTN Number",
            "placeholder" => "(403) 668-7895",
            "form_hint" => "Phone number for this address entry",
            "max_size" => 14,
            "min_size" => 10,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "addressbook_entry.pstn",
            "field_type" => "tel",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 1,
            "is_unique" => NULL,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        )
	);
	
	return $template;
	}



/*******************************************************************************/
/**
 * Adds get form data with form values
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_form_data($id_template, $form_value_list){
		//  This is normally dynamic based on endpoint_sip_template and should be added to this equation in the future
		$form_item_list = $this->get_template_base();
		foreach ($form_item_list as &$item):
			$item->valid_values = get_object_vars(json_decode($item->valid_values));
			// set the value to 1) posted value, or 2) db value or 3) default value or 4) empty string
			$item->value = set_value($item->name, ($form_value_list[$item->name]["value"] != NULL ? $form_value_list[$item->name]["value"] : ($item->default_value != NULL ? $item->default_value : '')));
			$item->field_error = $form_value_list[$item->name]["field_error"];
			$item->field_error2 = $form_value_list[$item->name]["field_error2"];
	endforeach;
	
		$this->twiggy->set("fil", $form_item_list);
	}



/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function insert_free_text($nanpa, $name) {

	$data = array();
	$data['name'] = $name;
	$data['pstn'] = $nanpa;

	$new_guid = $this->new_guid();
	$data['id'] = $new_guid;
	$data['created_at'] = date('Y-m-d H:i:s');
	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = '[Incomplete in Code]';

	$this->validate = $this->rules;
	
	$insert_result = $this->insert($data);

	return $insert_result;
}
	
	
}
 
 /* End of file Addressbook_entry_model.php */
/* Location: ./application/models/addressbook_entry_model.php */