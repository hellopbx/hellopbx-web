<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Endpoint_sip_identity_model extends MY_Model {

/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_endpoint_sip_identity";
	
	$this->rules = array(
		array('field'=>'phone_name', 'label'=>'Gateway', 'rules'=>'trim|required|min_length[6]'),
		array('field'=>'mac', 'label'=>'Username', 'rules'=>'trim|min_length[12]|is_unique[pbx_endpoint_sip_model.mac'),
		array('field'=>'name', 'label'=>'Password', 'rules'=>'trim|min_length[6]'),
		array('field'=>'description', 'label'=>'', 'rules'=>'trim|required|min_length[6]')
	);
}



/*******************************************************************************/
/**
 * Adds get form data with form values
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_form_data2($id, $form_value_list){
		//  This is normally dynamic based on endpoint_sip_template and should be added to this equation in the future
		$form_item_list = $this->get_template_base();
		
		foreach ($form_item_list as &$item):
			$item->valid_values = get_object_vars(json_decode($item->valid_values));
			// set the value to 1) posted value, or 2) db value or 3) default value or 4) empty string
			$item->value = set_value($item->name, ($form_value_list[$item->name]["value"] != NULL ? $form_value_list[$item->name]["value"] : ($item->default_value != NULL ? $item->default_value : '')));
			$item->field_error = $form_value_list[$item->name]["field_error"];
			$item->field_error2 = $form_value_list[$item->name]["field_error2"];
	endforeach;
	return $form_item_list;
	
//		$this->twiggy->set("fil", $form_item_list);
	}

/*******************************************************************************/
/**
 * Gets form fields
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_template_base(){
	$template = array (
     (object)array  (
            "name" => "line_number",
            "default_value" => "1",
            "label" => "Line",
            "placeholder" => "1",
            "form_hint" => "Enter the line number associated with this account. ",
            "max_size" => NULL,
            "min_size" => NULL,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.line_number",
            "field_type" => "list",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 1,
            "is_unique" => 1,
            "is_integer" => 1,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => '{"1":"1","2":"2","3":"3","4":"4","5":"5","6":"6","7":"7"}',
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "user_active",
            "default_value" => "t",
            "label" => "Enabled",
            "placeholder" => "",
            "form_hint" => "enabled",
            "max_size" => 30,
            "min_size" => 6,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.user_active",
            "field_type" => "yes_no",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 1,
            "is_unique" => 1,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "user_realname",
            "default_value" => "HelloPBX",
            "label" => "Display Name",
            "placeholder" => "Snom 821",
            "form_hint" => "Usually your name",
            "max_size" => 30,
            "min_size" => 6,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.user_realname",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => 1,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "user_name",
            "default_value" => "",
            "label" => "Username",
            "placeholder" => "2jkl34*#@",
            "form_hint" => "Security username for your phone account.  Make sure this is very complex and is kept secret",
            "max_size" => 40,
            "min_size" => 20,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.user_name",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => 1,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "user_pass",
            "default_value" => "",
            "label" => "Password",
            "placeholder" => "2jkl34*#@",
            "form_hint" => "Security password for your phone account. Make sure this is very complex and is kept secret",
            "max_size" => 40,
            "min_size" => 30,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.user_pass",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => 1,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "user_host",
            "default_value" => "",
            "label" => "Server",
            "placeholder" => "voice.hellopbx.net",
            "form_hint" => "The hostname of your phone server.  For accounts on this server, this can be left blank",
            "max_size" => 40,
            "min_size" => 6,
            "is_required" => 0,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.user_host",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => 1,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "user_pname",
            "default_value" => "",
            "label" => "Auth Username",
            "placeholder" => "",
            "form_hint" => "This is not required for HelloPBX and should be left blank.  If your registering an account for a different sip account, this may be required.",
            "max_size" => 30,
            "min_size" => 6,
            "is_required" => 0,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.user_pname",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => 1,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "user_mailbox",
            "default_value" => "",
            "label" => "Mailbox",
            "placeholder" => "",
            "form_hint" => "For FusionPBX this should be left blank. You may require this when registering accounts for a custom sip account.",
            "max_size" => 8,
            "min_size" => 3,
            "is_required" => 0,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.user_mailbox",
            "field_type" => "text",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => 1,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => "{}",
            "is_readonly" => 0
        ),
     (object)array  (
            "name" => "context",
            "default_value" => "",
            "label" => "Context",
            "placeholder" => "",
            "form_hint" => "This is the security group of this phone. Be very careful as this can lead to security issues and very expensive monetary repercussions",
            "max_size" => 30,
            "min_size" => 6,
            "is_required" => 1,
            "sort_order" => 10,
            "reference" => "endpoint_sip_identity.context",
            "field_type" => "list",
            "greater_than" => NULL,
            "less_than" => NULL,
            "is_numeric" => 0,
            "is_unique" => 1,
            "is_integer" => 0,
            "valid_email" => 0,
            "valid_ip" => 0,
            "valid_values" => '{"trusted":"Internal", "config":"Un-configured", "pstn":"Mobile Phone", "common":"Common"}',
            "is_readonly" => 0
        )
	);
	

	//  trusted is a dedicated desk extension in a non-public location
	//  config is a brand new phone who's location is not know and cannot be trusted
	//  pstn is a PSTN phone such as a cell phone
	//  common is a phone that is deployed in a common area and can be used by strangers.
	
	return $template;
	}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function insert_free_text($id_parent) {

		$data['id_parent'] = $id_parent;
		$data['line_number'] = 1;
		$data['user_active'] = 'true';
		$data['user_realname'] = "HelloPBX";
		$data['user_name'] = $this->new_random_string();
		$data['user_pass'] = $this->new_random_string();
		
		$new_guid = $this->new_guid();
		$data['id'] = $new_guid;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = '[Incomplete in Code]';

//		$this->validate = $this->rules;
		
		return $this->insert($data);
	}
	
/*******************************************************************************/
/**
 * Delete all identities for an Endpoint
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function delete_for_endpoint($id_parent) {
		$this->db->where('id_parent', $id_parent);
		$this->db->delete($this->_table);
	}




/*******************************************************************************/
/**
 * Gets non-static values to be displayed on the form such as field values and field errors.  
 *  The _all suffix gets multiple rows for a table style edit mode
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_form_items_all($id_parent = NULL, $default_values = NULL, $form_items = NULL){
	$this->db->where('id_parent', $id_parent);
	$rows = $this->get_all();
	foreach ($rows as $row):
		$form_value_list_children[$row->id] = $this->get_form_items($row->id, NULL, $default_values, $form_items);
	endforeach;
//	print_r($form_value_list_children);
	return $form_value_list_children;
}

/*******************************************************************************/
/**
 * Adds get form data with form values.  This is for data with multiple rows showing
 *  The format of multi row data is...
 *  _guid_fieldname
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_form_data_all($id, $form_value_list_children){
	//  This is normally dynamic based on endpoint_sip_template and should be added to this equation in the future
	
	foreach ($form_value_list_children as $key => &$form_value_list):
		$form_item_list_children[$key] = $this->get_template_base();
		foreach ($form_item_list_children[$key] as &$item):
			$item->valid_values_decoded = get_object_vars(json_decode($item->valid_values));
				// set the value to 1) posted value, or 2) db value or 3) default value or 4) empty string

			$form_field_name = "_" . $form_value_list["id"]["value"] . "_" . $item->name;
			$item->name_multi = $form_field_name;
			$item->value = set_value($form_field_name, ($form_value_list[$item->name]["value"] != NULL ? $form_value_list[$item->name]["value"] : ($item->default_value != NULL ? $item->default_value : '')));
			$item->field_error = $form_value_list[$item->name]["field_error"];
			$item->field_error2 = $form_value_list[$item->name]["field_error2"];
		endforeach;
	endforeach;
	return $form_item_list_children;
	}


/*******************************************************************************/
/**
 * Display a Trunk edit screen for sip
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function update_advanced_children($id_parent){
	$columns = $this->db->list_fields($this->_table);

	foreach ($_POST as $key => $value):
		if(substr($key, 0,1) == "_"):
			$field_name = substr($key, 38);
			$id = substr($key, 1, 36);
		
			if($field_name != 'id'):
				if( in_array($field_name, $columns)):
					$data[$field_name] = empty($value) ? NULL : $value;
				endif;
			endif;
		endif;
	endforeach;

	$data['updated_at'] = date('Y-m-d H:i:s');
	$data['updated_by'] = '[Incomplete in Code]';

	$this->validate = $this->rules;
	$result =  $this->update($id, $data);

	return $result;
}



/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function Create_table(){
$schema_pgsql = <<<EOD
EOD;
	}
}






 
 /* End of file endpoint_sip_identity_model.php */
/* Location: ./application/models/endpoint_sip_identity_model.php */