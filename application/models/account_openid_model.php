<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_openid_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_account_openid";
}
public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."auth_account_openid" (
"id" int8 DEFAULT nextval('auth_account_openid_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"openid" varchar(240) NOT NULL,
"account_id" int8 NOT NULL,
"linkedon" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}

 /* End of file Account_openid_model.php */
/* Location: ./application/models/account_openid_model.php */