<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Call_History_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_cdr";
}

public function get_all() {
	parent::order_by('start_stamp', 'asc');
	parent::get_all();
	}


public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."pbx_cdr" (
"uuid" uuid NOT NULL,
"domain_uuid" uuid,
"domain_name" text,
"accountcode" text,
"direction" text,
"default_language" text,
"context" text,
"xml_cdr" text,
"caller_id_name" text,
"caller_id_number" text,
"destination_number" text,
"start_epoch" numeric,
"start_stamp" timestamp(6),
"answer_stamp" timestamp(6),
"answer_epoch" numeric,
"end_epoch" numeric,
"end_stamp" text,
"duration" numeric,
"mduration" numeric,
"billsec" numeric,
"billmsec" numeric,
"bridge_uuid" text,
"read_codec" text,
"read_rate" text,
"write_codec" text,
"write_rate" text,
"remote_media_ip" text,
"network_addr" text,
"recording_file" text,
"leg" char(1),
"pdd_ms" numeric,
"last_app" text,
"last_arg" text,
"cc_side" text,
"cc_member_uuid" uuid,
"cc_queue_joined_epoch" text,
"cc_queue" text,
"cc_member_session_uuid" uuid,
"cc_agent" text,
"cc_agent_type" text,
"waitsec" numeric,
"conference_name" text,
"conference_uuid" uuid,
"conference_member_id" text,
"digits_dialed" text,
"hangup_cause" text,
"hangup_cause_q850" numeric,
"sip_hangup_disposition" text
)
WITH (OIDS=FALSE)
;
CREATE INDEX "index_billsec" ON "public"."pbx_cdr" USING btree ("billsec");
CREATE INDEX "index_caller_id_name" ON "public"."pbx_cdr" USING btree ("caller_id_name");
CREATE INDEX "index_destination_number" ON "public"."pbx_cdr" USING btree ("destination_number");
CREATE INDEX "index_duration" ON "public"."pbx_cdr" USING btree ("duration");
CREATE INDEX "index_hangup_cause" ON "public"."pbx_cdr" USING btree ("hangup_cause");
CREATE INDEX "index_start_stamp" ON "public"."pbx_cdr" USING btree ("start_stamp");

EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Cdr_model.php */
/* Location: ./application/models/cdr_model.php */