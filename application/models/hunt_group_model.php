<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hunt_group_model extends MY_Model {

public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_hunt_group";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE pbx_hunt_group (
ref uuid,
created_at uuid,
updated_at uuid,
updated_by varchar(100),
name text,
extension text,
context text,
strategy text,
timeout_sec numeric,
timeout_app text,
timeout_data text,
enabled text,
description text
);
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD

EOD;
	}



}
 
/* End of file hunt_group_model.php */
/* Location: ./application/models/hunt_group_model.php */