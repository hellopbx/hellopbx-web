<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dialplan_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_dialplan";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."pbx_dialplan" (
"ref" uuid NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"updated_by" varchar(100),
"context" varchar(250),
"name" varchar(250),
"number" varchar(250),
"continue" varchar(250),
"sort_order" numeric,
"enabled" varchar(5),
"description" text
)
WITH (OIDS=FALSE)

EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
INSERT INTO "public"."pbx_dialplan" VALUES ('05aa8d71-6670-4dc5-8fca-d8241300d90f', null, null, null, 'features', 'att_xfer', '', '', '310', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('0e6b99bd-b31a-42ca-bc3a-05baed1e8db7', null, null, null, 'default', 'group-intercept', '*8', '', '230', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('0fef023c-830d-4223-bd92-9850e76e98b3', null, null, null 'default', 'hot-desk-logout', '*073', '', '475', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', null, null, null 'default', 'local_extension', '27', '', '999', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('16ab6217-2678-42b6-b1ee-928b4583c06d', null, null, null, 'features', 'is_transfer', '', '', '330', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('1aa67da5-b433-4da5-92a4-fe101ce83981', null, null, null, 'default', 'directory', '*411', '', '430', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('1cf3b52f-b2ae-4c9e-acd7-dc7c0050a3da', null, null, null, 'default', 'call_privacy', '*67', '', '270', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('21249d07-23d9-4f02-9721-f1ea069f3053', null, null, null, 'default', 'wake-up', '*925', '', '440', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('235c4f0f-89e3-4b56-b774-17bc59c459fe', null, null, null, 'features', 'please_hold', '100109', '', '350', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('270a6a42-b935-45db-b590-ea796c3c72a9', null, null, null, 'default', 'freeswitch_conference', '*9888 8888 1616 3232', '', '410', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('2b3c1387-3d15-410b-8c1b-7fc93a5ce9ff', null, null, null, 'default', 'operator-forward', '*000', '', '485', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('2d5a79ba-6c37-4469-9cb2-5aa8215f21c1', null, null, null, 'default', 'recordings', '*732', '', '400', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('3387a202-8514-4962-ab65-9dc5ac7d8ee5', null, null, null, 'default', 'vmain_user', '*97', '', '330', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('3664e3ce-5edb-4a62-9e9c-6cbfd7b98c03', null, null, null, 'default', 'eavesdrop', '*8827', '', '260', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('3e19e19c-063e-4419-b3a2-a9489171d87d', null, null, null, 'features', 'is_secure', null, 'true', '370', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('420b4db6-968c-477e-8847-ed4e8637afc7', null, null, null, 'default', 'hot-desk-login', '*072', '', '470', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('4a70a5f6-b90b-4dce-8383-4a8730f6bce0', null, null, null, 'default', 'global-intercept', '*886', '', '220', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('4cbfa942-09d4-483a-af15-304e43c007cd', null, null, null, 'default', 'milliwatt', '*9197', '', '360', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('69ea4816-ee09-4f6a-b189-33ee8a651e51', null, null, null, 'default', 'send_to_voicemail', '*9927', '', '310', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('70c38661-1be7-4356-a342-61d6560cea8b', null, null, null, 'default', 'vmain', ' *4000 *98', '', '320', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('73d82710-60af-4357-b020-5ccfcd92cde5', null, null, null, 'default', 'extension-intercom', '*827', '', '300', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('790b09f2-1cd9-4a99-ba5d-72ebe64d93d8', null, null, null, 'default', 'delay_echo', '*9195', '', '340', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('7d01fd40-6b11-4d2d-acea-235e3022a0d2', null, null, null, 'default', 'do-not-disturb', '*363', '', '490', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('8c8c73be-8fa2-4efb-b31c-f75cf8574b39', null, null, null, 'features', 'xfer_vm', '', '', '320', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('8f3aa313-b289-40d2-99c6-b8b732622a81', null, null, null, 'default', 'echo', '*9196', '', '350', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', null, null, null, 'default', 'hold_music', '*9664', '', '380', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', null, null, null, 'default', 'park_in', '5900', '', '450', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('9e3d0cac-ac62-4047-87da-ec6ce05351d7', null, null, null, 'features', 'is_zrtp_secure', null, 'true', '360', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('a5f44e47-1955-408f-8358-7fa7fdec8c10', null, null, null, 'features', 'cf', '', '', '340', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('ab96d39f-0993-47b3-b2a8-afe545847ab5', null, null, null, 'default', 'page', '*724', '', '245', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('bfe091cb-8ba4-46df-9307-5dec0cb61a56', null, null, null, 'default', 'park_out', '5901 5902 5903', '', '455', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('c38c0264-d4ca-48cd-b7ea-06146d6546ee', null, null, null, 'default', 'tone_stream', '*9198', '', '370', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', null, null, null, 'default', 'park_slots', '5901 5902 5903', '', '460', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('d8a7a503-f367-4a8d-b02a-9c98e8069792', null, null, null, 'default', 'intercept-ext', '**', '', '290', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('df2bd417-6769-4507-9fc7-faaebc5568d0', null, null, null, 'default', 'global', null, 'true', '250', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('df987ef4-72cb-4245-9f06-3c286e9eac5d', null, null, null, 'default', 'call_return', '*69 ', '', '280', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('e7ed819d-75a5-467b-9c59-9a5c9072909e', null, null, null, 'features', 'dx', '', '', '300', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('eb0f5903-cfbb-46e6-9e33-be12813a485c', null, null, null, 'default', 'call-direction', null, 'true', '100', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('ef87c228-b2d4-4b13-86e9-07eac484ce6a', null, null, null, 'default', 'operator', '0 ', 'false', '480', 'true', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('f456fd0e-b90f-40af-917f-59bddfb563b5', null, null, null, 'default', 'disa', '*3472', '', '420', 'false', '');
INSERT INTO "public"."pbx_dialplan" VALUES ('f5d56a09-e397-4971-8623-717daccf8b63', null, null, null, 'default', 'redial', ' *870', '', '240', 'true', '');
EOD;
}


}
 
 /* End of file Account_model.php */
/* Location: ./application/models/account_model.php */