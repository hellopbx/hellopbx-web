<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Phone_mac_model extends MY_Model {

public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_phone_mac";
}



/*******************************************************************************/
/**
 * Locates a template based on mac address.
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function match($mac){
		$this->order_by('sort_order', 'desc');
		$rows = $this->get_all();
		foreach($rows as $row):	
			if (preg_match($row->mac, strtolower($mac) ) ):
				$phone = array(
					"brand" => $row->brand,
					"family" => $row->family,
					"model" => $row->model,
					"phone_name" => $row->phone_name, 
					"phone_picture_url" => $row->phone_picture_url
				);
				return $phone;
		endif;
	endforeach;
	return array();
}

public function Create_table(){
$schema_pgsql = <<<EOD
EOD;

$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Phone_mac_model.php */
/* Location: ./application/models/phone_mac_model.php */