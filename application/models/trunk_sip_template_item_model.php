<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trunk_sip_template_item_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_trunk_sip_template_item";
}

public function get_form_data($id, $form_value_list){
		$this->db->where('id_template', $id);
		$this->order_by('sort_order', 'asc');
		$form_item_list = $this->get_all();
		
		foreach ($form_item_list as &$item):
			$item->valid_values = get_object_vars(json_decode($item->valid_values));
			// set the value to 1) posted value, or 2) db value or 3) default value or 4) empty string
			$item->value = set_value($item->name, ($form_value_list[$item->name]["value"] != NULL ? $form_value_list[$item->name]["value"] : ($item->default_value != NULL ? $item->default_value : '')));
			$item->field_error = $form_value_list[$item->name]["field_error"];
			$item->field_error2 = $form_value_list[$item->name]["field_error2"];
	endforeach;
		$this->twiggy->set("fil", $form_item_list);
	}



public function Create_table(){
	$schema_pgsql = <<<EOD
EOD;

$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file trunk_sip_model.php */
/* Location: ./application/models/trunk_sip_model.php */