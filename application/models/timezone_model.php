<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timezone_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_timezone";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."ref_timezone" (
"id" int4 DEFAULT nextval('ref_timezone_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"abbr" varchar(8) NOT NULL,
"name" varchar(80) NOT NULL,
"utc" varchar(18) NOT NULL,
"hours" int2 NOT NULL
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Timezone_model.php */
/* Location: ./application/models/timezone_model.php */