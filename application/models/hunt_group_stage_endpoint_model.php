<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//  Dialplan Items are instructions identified by a call string and have multiple steps associated (dpitem_step)  to perform functionality
class Dp_item_model extends MY_Model {

public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_hunt_group_stage_endpoint";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE pbx_hunt_group_stage_endpoint (
ref uuid,
ref_hunt_group_stage uuid,
created_at timestamp,
updated_at timestamp,
updated_by varchar(100),
ref_endpoint_sip uuid,
ref_endpoint_xmpp uuid,
ref_endpoint_pstn uuid
);
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD

EOD;
	}



}
 
 /* End of file hunt_group_stage_endpoint.php */
/* Location: ./application/models/hunt_group_stage_endpoint_model.php */