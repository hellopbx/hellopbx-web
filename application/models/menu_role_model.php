<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_role_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "menu_main_role";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."menu_main_role" (
"id" int4 NOT NULL,
"id_menu_main" int4 not null,
"created_at" timestamp(6) NOT NULL,
"updated_at" timestamp(6) NOT NULL,
"updated_by" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Menu_model.php */
/* Location: ./application/models/menu_model.php */