<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_role";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."auth_role" (
"id" int8 DEFAULT nextval('auth_acl_role_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"name" varchar(80) NOT NULL,
"description" varchar(160) NOT NULL,
"suspendedon" timestamp(6)
)
WITH (OIDS=FALSE)

EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
INSERT INTO "public"."auth_role" VALUES ('2', null, null, null, 'superadmin', 'Super Administrator Group', null);
INSERT INTO "public"."auth_role" VALUES ('3', null, null, null, 'admin', 'Administrator Group', null);
INSERT INTO "public"."auth_role" VALUES ('4', null, null, null, 'user', 'User Group', null);
INSERT INTO "public"."auth_role" VALUES ('5', null, null, null, 'public', 'Public Group', null);
INSERT INTO "public"."auth_role" VALUES ('6', null, null, null, 'agent', 'Call Center Agent Group', null);
EOD;
}


}
 
 /* End of file role_model.php */
/* Location: ./application/models/role_model.php */