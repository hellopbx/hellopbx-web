<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_sub_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "www_menu_sub";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."www_menu_sub" (
"id" int4 DEFAULT nextval('www_menu_sub_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"updated_by" varchar(100),
"name" varchar(50) NOT NULL,
"key" varchar(250) NOT NULL,
"key_parent" varchar(250) NOT NULL,
"sort_order" int4
)
WITH (OIDS=FALSE)

EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Account_model.php */
/* Location: ./application/models/menu_sub_model.php */