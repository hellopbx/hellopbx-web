<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_permission";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "auth_permission" (
"id" int8 DEFAULT nextval('a3m_acl_permission_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"key" varchar(80) NOT NULL,
"description" varchar(160) DEFAULT NULL::character varying,
"suspendedon" timestamp(6) DEFAULT NULL::timestamp without time zone
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
INSERT INTO "public"."auth_permission" VALUES ('1', null, null, null, 'active_queues_add', 'active_queues_add', null);
INSERT INTO "public"."auth_permission" VALUES ('2', null, null, null, 'active_queues_delete', 'active_queues_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('3', null, null, null, 'active_queues_edit', 'active_queues_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('4', null, null, null, 'active_queues_view', 'active_queues_view', null);
INSERT INTO "public"."auth_permission" VALUES ('5', null, null, null, 'adminer', 'adminer', null);
INSERT INTO "public"."auth_permission" VALUES ('6', null, null, null, 'app_add', 'app_add', null);
INSERT INTO "public"."auth_permission" VALUES ('7', null, null, null, 'app_delete', 'app_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('8', null, null, null, 'app_edit', 'app_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('9', null, null, null, 'app_view', 'app_view', null);
INSERT INTO "public"."auth_permission" VALUES ('10', null, null, null, 'call_broadcast_add', 'call_broadcast_add', null);
INSERT INTO "public"."auth_permission" VALUES ('11', null, null, null, 'call_broadcast_delete', 'call_broadcast_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('12', null, null, null, 'call_broadcast_edit', 'call_broadcast_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('13', null, null, null, 'call_broadcast_send', 'call_broadcast_send', null);
INSERT INTO "public"."auth_permission" VALUES ('14', null, null, null, 'call_broadcast_view', 'call_broadcast_view', null);
INSERT INTO "public"."auth_permission" VALUES ('15', null, null, null, 'call_center_active_view', 'call_center_active_view', null);
INSERT INTO "public"."auth_permission" VALUES ('16', null, null, null, 'call_center_agents_add', 'call_center_agents_add', null);
INSERT INTO "public"."auth_permission" VALUES ('17', null, null, null, 'call_center_agents_delete', 'call_center_agents_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('18', null, null, null, 'call_center_agents_edit', 'call_center_agents_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('19', null, null, null, 'call_center_agents_view', 'call_center_agents_view', null);
INSERT INTO "public"."auth_permission" VALUES ('20', null, null, null, 'call_center_queues_add', 'call_center_queues_add', null);
INSERT INTO "public"."auth_permission" VALUES ('21', null, null, null, 'call_center_queues_delete', 'call_center_queues_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('22', null, null, null, 'call_center_queues_edit', 'call_center_queues_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('23', null, null, null, 'call_center_queues_view', 'call_center_queues_view', null);
INSERT INTO "public"."auth_permission" VALUES ('24', null, null, null, 'call_center_tiers_add', 'call_center_tiers_add', null);
INSERT INTO "public"."auth_permission" VALUES ('25', null, null, null, 'call_center_tiers_delete', 'call_center_tiers_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('26', null, null, null, 'call_center_tiers_edit', 'call_center_tiers_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('27', null, null, null, 'call_center_tiers_view', 'call_center_tiers_view', null);
INSERT INTO "public"."auth_permission" VALUES ('28', null, null, null, 'call_forward', 'call_forward', null);
INSERT INTO "public"."auth_permission" VALUES ('29', null, null, null, 'calls_active_hangup', 'calls_active_hangup', null);
INSERT INTO "public"."auth_permission" VALUES ('30', null, null, null, 'calls_active_park', 'calls_active_park', null);
INSERT INTO "public"."auth_permission" VALUES ('31', null, null, null, 'calls_active_rec', 'calls_active_rec', null);
INSERT INTO "public"."auth_permission" VALUES ('32', null, null, null, 'calls_active_transfer', 'calls_active_transfer', null);
INSERT INTO "public"."auth_permission" VALUES ('33', null, null, null, 'calls_active_view', 'calls_active_view', null);
INSERT INTO "public"."auth_permission" VALUES ('34', null, null, null, 'click_to_call_call', 'click_to_call_call', null);
INSERT INTO "public"."auth_permission" VALUES ('35', null, null, null, 'click_to_call_view', 'click_to_call_view', null);
INSERT INTO "public"."auth_permission" VALUES ('36', null, null, null, 'conference_add', 'conference_add', null);
INSERT INTO "public"."auth_permission" VALUES ('37', null, null, null, 'conference_delete', 'conference_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('38', null, null, null, 'conference_edit', 'conference_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('39', null, null, null, 'conferences_active_advanced_view', 'conferences_active_advanced_view', null);
INSERT INTO "public"."auth_permission" VALUES ('40', null, null, null, 'conferences_active_deaf', 'conferences_active_deaf', null);
INSERT INTO "public"."auth_permission" VALUES ('41', null, null, null, 'conferences_active_energy', 'conferences_active_energy', null);
INSERT INTO "public"."auth_permission" VALUES ('42', null, null, null, 'conferences_active_gain', 'conferences_active_gain', null);
INSERT INTO "public"."auth_permission" VALUES ('43', null, null, null, 'conferences_active_kick', 'conferences_active_kick', null);
INSERT INTO "public"."auth_permission" VALUES ('44', null, null, null, 'conferences_active_lock', 'conferences_active_lock', null);
INSERT INTO "public"."auth_permission" VALUES ('45', null, null, null, 'conferences_active_mute', 'conferences_active_mute', null);
INSERT INTO "public"."auth_permission" VALUES ('46', null, null, null, 'conferences_active_record', 'conferences_active_record', null);
INSERT INTO "public"."auth_permission" VALUES ('47', null, null, null, 'conferences_active_view', 'conferences_active_view', null);
INSERT INTO "public"."auth_permission" VALUES ('48', null, null, null, 'conferences_active_volume', 'conferences_active_volume', null);
INSERT INTO "public"."auth_permission" VALUES ('49', null, null, null, 'conference_view', 'conference_view', null);
INSERT INTO "public"."auth_permission" VALUES ('50', null, null, null, 'contacts_add', 'contacts_add', null);
INSERT INTO "public"."auth_permission" VALUES ('51', null, null, null, 'contacts_delete', 'contacts_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('52', null, null, null, 'contacts_edit', 'contacts_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('53', null, null, null, 'contacts_view', 'contacts_view', null);
INSERT INTO "public"."auth_permission" VALUES ('54', null, null, null, 'content_add', 'content_add', null);
INSERT INTO "public"."auth_permission" VALUES ('55', null, null, null, 'content_delete', 'content_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('56', null, null, null, 'content_edit', 'content_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('57', null, null, null, 'content_view', 'content_view', null);
INSERT INTO "public"."auth_permission" VALUES ('58', null, null, null, 'database_add', 'database_add', null);
INSERT INTO "public"."auth_permission" VALUES ('59', null, null, null, 'database_delete', 'database_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('60', null, null, null, 'database_edit', 'database_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('61', null, null, null, 'database_view', 'database_view', null);
INSERT INTO "public"."auth_permission" VALUES ('62', null, null, null, 'default_setting_add', 'default_setting_add', null);
INSERT INTO "public"."auth_permission" VALUES ('63', null, null, null, 'default_setting_delete', 'default_setting_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('64', null, null, null, 'default_setting_edit', 'default_setting_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('65', null, null, null, 'default_setting_view', 'default_setting_view', null);
INSERT INTO "public"."auth_permission" VALUES ('66', null, null, null, 'destination_add', 'destination_add', null);
INSERT INTO "public"."auth_permission" VALUES ('67', null, null, null, 'destination_delete', 'destination_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('68', null, null, null, 'destination_edit', 'destination_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('69', null, null, null, 'destination_view', 'destination_view', null);
INSERT INTO "public"."auth_permission" VALUES ('70', null, null, null, 'dialplan_add', 'dialplan_add', null);
INSERT INTO "public"."auth_permission" VALUES ('71', null, null, null, 'dialplan_advanced_edit', 'dialplan_advanced_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('72', null, null, null, 'dialplan_advanced_view', 'dialplan_advanced_view', null);
INSERT INTO "public"."auth_permission" VALUES ('73', null, null, null, 'dialplan_delete', 'dialplan_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('74', null, null, null, 'dialplan_edit', 'dialplan_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('75', null, null, null, 'dialplan_view', 'dialplan_view', null);
INSERT INTO "public"."auth_permission" VALUES ('76', null, null, null, 'domain_add', 'domain_add', null);
INSERT INTO "public"."auth_permission" VALUES ('77', null, null, null, 'domain_delete', 'domain_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('78', null, null, null, 'domain_edit', 'domain_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('79', null, null, null, 'domain_select', 'domain_select', null);
INSERT INTO "public"."auth_permission" VALUES ('80', null, null, null, 'domain_setting_add', 'domain_setting_add', null);
INSERT INTO "public"."auth_permission" VALUES ('81', null, null, null, 'domain_setting_delete', 'domain_setting_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('82', null, null, null, 'domain_setting_edit', 'domain_setting_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('83', null, null, null, 'domain_setting_view', 'domain_setting_view', null);
INSERT INTO "public"."auth_permission" VALUES ('84', null, null, null, 'domain_view', 'domain_view', null);
INSERT INTO "public"."auth_permission" VALUES ('85', null, null, null, 'do_not_disturb', 'do_not_disturb', null);
INSERT INTO "public"."auth_permission" VALUES ('86', null, null, null, 'exec_command_line', 'exec_command_line', null);
INSERT INTO "public"."auth_permission" VALUES ('87', null, null, null, 'exec_php_command', 'exec_php_command', null);
INSERT INTO "public"."auth_permission" VALUES ('88', null, null, null, 'exec_switch', 'exec_switch', null);
INSERT INTO "public"."auth_permission" VALUES ('89', null, null, null, 'extension_add', 'extension_add', null);
INSERT INTO "public"."auth_permission" VALUES ('90', null, null, null, 'extension_delete', 'extension_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('91', null, null, null, 'extension_edit', 'extension_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('92', null, null, null, 'extensions_active_assigned_view', 'extensions_active_assigned_view', null);
INSERT INTO "public"."auth_permission" VALUES ('93', null, null, null, 'extensions_active_hangup', 'extensions_active_hangup', null);
INSERT INTO "public"."auth_permission" VALUES ('94', null, null, null, 'extensions_active_list_view', 'extensions_active_list_view', null);
INSERT INTO "public"."auth_permission" VALUES ('95', null, null, null, 'extensions_active_park', 'extensions_active_park', null);
INSERT INTO "public"."auth_permission" VALUES ('96', null, null, null, 'extensions_active_rec', 'extensions_active_rec', null);
INSERT INTO "public"."auth_permission" VALUES ('97', null, null, null, 'extensions_active_transfer', 'extensions_active_transfer', null);
INSERT INTO "public"."auth_permission" VALUES ('98', null, null, null, 'extensions_active_view', 'extensions_active_view', null);
INSERT INTO "public"."auth_permission" VALUES ('99', null, null, null, 'extension_toll', 'extension_toll', null);
INSERT INTO "public"."auth_permission" VALUES ('100', null, null, null, 'extension_view', 'extension_view', null);
INSERT INTO "public"."auth_permission" VALUES ('101', null, null, null, 'fax_extension_add', 'fax_extension_add', null);
INSERT INTO "public"."auth_permission" VALUES ('102', null, null, null, 'fax_extension_delete', 'fax_extension_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('103', null, null, null, 'fax_extension_edit', 'fax_extension_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('104', null, null, null, 'fax_extension_view', 'fax_extension_view', null);
INSERT INTO "public"."auth_permission" VALUES ('105', null, null, null, 'fax_inbox_delete', 'fax_inbox_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('106', null, null, null, 'fax_inbox_view', 'fax_inbox_view', null);
INSERT INTO "public"."auth_permission" VALUES ('107', null, null, null, 'fax_send', 'fax_send', null);
INSERT INTO "public"."auth_permission" VALUES ('108', null, null, null, 'fax_sent_delete', 'fax_sent_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('109', null, null, null, 'fax_sent_view', 'fax_sent_view', null);
INSERT INTO "public"."auth_permission" VALUES ('110', null, null, null, 'fifo_add', 'fifo_add', null);
INSERT INTO "public"."auth_permission" VALUES ('111', null, null, null, 'fifo_delete', 'fifo_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('112', null, null, null, 'fifo_edit', 'fifo_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('113', null, null, null, 'fifo_view', 'fifo_view', null);
INSERT INTO "public"."auth_permission" VALUES ('114', null, null, null, 'follow_me', 'follow_me', null);
INSERT INTO "public"."auth_permission" VALUES ('115', null, null, null, 'gateways_add', 'gateways_add', null);
INSERT INTO "public"."auth_permission" VALUES ('116', null, null, null, 'gateways_delete', 'gateways_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('117', null, null, null, 'gateways_edit', 'gateways_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('118', null, null, null, 'gateways_view', 'gateways_view', null);
INSERT INTO "public"."auth_permission" VALUES ('119', null, null, null, 'grammar_save', 'grammar_save', null);
INSERT INTO "public"."auth_permission" VALUES ('120', null, null, null, 'grammar_view', 'grammar_view', null);
INSERT INTO "public"."auth_permission" VALUES ('121', null, null, null, 'group_add', 'group_add', null);
INSERT INTO "public"."auth_permission" VALUES ('122', null, null, null, 'group_delete', 'group_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('123', null, null, null, 'group_edit', 'group_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('124', null, null, null, 'group_member_add', 'group_member_add', null);
INSERT INTO "public"."auth_permission" VALUES ('125', null, null, null, 'group_member_delete', 'group_member_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('126', null, null, null, 'group_member_view', 'group_member_view', null);
INSERT INTO "public"."auth_permission" VALUES ('127', null, null, null, 'group_permissions', 'group_permissions', null);
INSERT INTO "public"."auth_permission" VALUES ('128', null, null, null, 'group_view', 'group_view', null);
INSERT INTO "public"."auth_permission" VALUES ('129', null, null, null, 'hot_desk_add', 'hot_desk_add', null);
INSERT INTO "public"."auth_permission" VALUES ('130', null, null, null, 'hot_desk_delete', 'hot_desk_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('131', null, null, null, 'hot_desk_edit', 'hot_desk_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('132', null, null, null, 'hot_desk_view', 'hot_desk_view', null);
INSERT INTO "public"."auth_permission" VALUES ('133', null, null, null, 'hunt_group_add', 'hunt_group_add', null);
INSERT INTO "public"."auth_permission" VALUES ('134', null, null, null, 'hunt_group_delete', 'hunt_group_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('135', null, null, null, 'hunt_group_edit', 'hunt_group_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('136', null, null, null, 'hunt_group_view', 'hunt_group_view', null);
INSERT INTO "public"."auth_permission" VALUES ('137', null, null, null, 'inbound_route_add', 'inbound_route_add', null);
INSERT INTO "public"."auth_permission" VALUES ('138', null, null, null, 'inbound_route_copy', 'inbound_route_copy', null);
INSERT INTO "public"."auth_permission" VALUES ('139', null, null, null, 'inbound_route_delete', 'inbound_route_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('140', null, null, null, 'inbound_route_edit', 'inbound_route_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('141', null, null, null, 'inbound_route_view', 'inbound_route_view', null);
INSERT INTO "public"."auth_permission" VALUES ('142', null, null, null, 'ivr_menu_add', 'ivr_menu_add', null);
INSERT INTO "public"."auth_permission" VALUES ('143', null, null, null, 'ivr_menu_delete', 'ivr_menu_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('144', null, null, null, 'ivr_menu_edit', 'ivr_menu_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('145', null, null, null, 'ivr_menu_view', 'ivr_menu_view', null);
INSERT INTO "public"."auth_permission" VALUES ('146', null, null, null, 'log_download', 'log_download', null);
INSERT INTO "public"."auth_permission" VALUES ('147', null, null, null, 'log_path_view', 'log_path_view', null);
INSERT INTO "public"."auth_permission" VALUES ('148', null, null, null, 'log_view', 'log_view', null);
INSERT INTO "public"."auth_permission" VALUES ('149', null, null, null, 'menu_add', 'menu_add', null);
INSERT INTO "public"."auth_permission" VALUES ('150', null, null, null, 'menu_delete', 'menu_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('151', null, null, null, 'menu_edit', 'menu_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('152', null, null, null, 'menu_restore', 'menu_restore', null);
INSERT INTO "public"."auth_permission" VALUES ('153', null, null, null, 'menu_view', 'menu_view', null);
INSERT INTO "public"."auth_permission" VALUES ('154', null, null, null, 'modules_add', 'modules_add', null);
INSERT INTO "public"."auth_permission" VALUES ('155', null, null, null, 'modules_delete', 'modules_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('156', null, null, null, 'modules_edit', 'modules_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('157', null, null, null, 'modules_view', 'modules_view', null);
INSERT INTO "public"."auth_permission" VALUES ('158', null, null, null, 'music_on_hold_add', 'music_on_hold_add', null);
INSERT INTO "public"."auth_permission" VALUES ('159', null, null, null, 'music_on_hold_delete', 'music_on_hold_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('160', null, null, null, 'music_on_hold_view', 'music_on_hold_view', null);
INSERT INTO "public"."auth_permission" VALUES ('161', null, null, null, 'outbound_route_add', 'outbound_route_add', null);
INSERT INTO "public"."auth_permission" VALUES ('162', null, null, null, 'outbound_route_any_gateway', 'outbound_route_any_gateway', null);
INSERT INTO "public"."auth_permission" VALUES ('163', null, null, null, 'outbound_route_copy', 'outbound_route_copy', null);
INSERT INTO "public"."auth_permission" VALUES ('164', null, null, null, 'outbound_route_delete', 'outbound_route_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('165', null, null, null, 'outbound_route_edit', 'outbound_route_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('166', null, null, null, 'outbound_route_view', 'outbound_route_view', null);
INSERT INTO "public"."auth_permission" VALUES ('167', null, null, null, 'phone_add', 'phone_add', null);
INSERT INTO "public"."auth_permission" VALUES ('168', null, null, null, 'phone_delete', 'phone_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('169', null, null, null, 'phone_edit', 'phone_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('170', null, null, null, 'phone_view', 'phone_view', null);
INSERT INTO "public"."auth_permission" VALUES ('171', null, null, null, 'php_editor_save', 'php_editor_save', null);
INSERT INTO "public"."auth_permission" VALUES ('172', null, null, null, 'php_editor_view', 'php_editor_view', null);
INSERT INTO "public"."auth_permission" VALUES ('173', null, null, null, 'provision_editor_save', 'provision_editor_save', null);
INSERT INTO "public"."auth_permission" VALUES ('174', null, null, null, 'provision_editor_view', 'provision_editor_view', null);
INSERT INTO "public"."auth_permission" VALUES ('175', null, null, null, 'recordings_add', 'recordings_add', null);
INSERT INTO "public"."auth_permission" VALUES ('176', null, null, null, 'recordings_delete', 'recordings_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('177', null, null, null, 'recordings_download', 'recordings_download', null);
INSERT INTO "public"."auth_permission" VALUES ('178', null, null, null, 'recordings_edit', 'recordings_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('179', null, null, null, 'recordings_play', 'recordings_play', null);
INSERT INTO "public"."auth_permission" VALUES ('180', null, null, null, 'recordings_upload', 'recordings_upload', null);
INSERT INTO "public"."auth_permission" VALUES ('181', null, null, null, 'recordings_view', 'recordings_view', null);
INSERT INTO "public"."auth_permission" VALUES ('182', null, null, null, 'registrations_all', 'registrations_all', null);
INSERT INTO "public"."auth_permission" VALUES ('183', null, null, null, 'registrations_domain', 'registrations_domain', null);
INSERT INTO "public"."auth_permission" VALUES ('184', null, null, null, 'ring_group_add', 'ring_group_add', null);
INSERT INTO "public"."auth_permission" VALUES ('185', null, null, null, 'ring_group_delete', 'ring_group_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('186', null, null, null, 'ring_group_edit', 'ring_group_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('187', null, null, null, 'ring_group_view', 'ring_group_view', null);
INSERT INTO "public"."auth_permission" VALUES ('188', null, null, null, 'script_editor_save', 'script_editor_save', null);
INSERT INTO "public"."auth_permission" VALUES ('189', null, null, null, 'script_editor_view', 'script_editor_view', null);
INSERT INTO "public"."auth_permission" VALUES ('190', null, null, null, 'services_add', 'services_add', null);
INSERT INTO "public"."auth_permission" VALUES ('191', null, null, null, 'services_delete', 'services_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('192', null, null, null, 'services_edit', 'services_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('193', null, null, null, 'services_view', 'services_view', null);
INSERT INTO "public"."auth_permission" VALUES ('194', null, null, null, 'settings_edit', 'settings_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('195', null, null, null, 'settings_view', 'settings_view', null);
INSERT INTO "public"."auth_permission" VALUES ('196', null, null, null, 'sip_profile_add', 'sip_profile_add', null);
INSERT INTO "public"."auth_permission" VALUES ('197', null, null, null, 'sip_profile_delete', 'sip_profile_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('198', null, null, null, 'sip_profile_edit', 'sip_profile_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('199', null, null, null, 'sip_profile_view', 'sip_profile_view', null);
INSERT INTO "public"."auth_permission" VALUES ('200', null, null, null, 'sip_status_switch_status', 'sip_status_switch_status', null);
INSERT INTO "public"."auth_permission" VALUES ('201', null, null, null, 'sql_query_backup', 'sql_query_backup', null);
INSERT INTO "public"."auth_permission" VALUES ('202', null, null, null, 'sql_query_execute', 'sql_query_execute', null);
INSERT INTO "public"."auth_permission" VALUES ('203', null, null, null, 'system_status_sofia_status', 'system_status_sofia_status', null);
INSERT INTO "public"."auth_permission" VALUES ('204', null, null, null, 'system_status_sofia_status_profile', 'system_status_sofia_status_profile', null);
INSERT INTO "public"."auth_permission" VALUES ('205', null, null, null, 'system_view_backup', 'system_view_backup', null);
INSERT INTO "public"."auth_permission" VALUES ('206', null, null, null, 'system_view_cpu', 'system_view_cpu', null);
INSERT INTO "public"."auth_permission" VALUES ('207', null, null, null, 'system_view_hdd', 'system_view_hdd', null);
INSERT INTO "public"."auth_permission" VALUES ('208', null, null, null, 'system_view_info', 'system_view_info', null);
INSERT INTO "public"."auth_permission" VALUES ('209', null, null, null, 'system_view_ram', 'system_view_ram', null);
INSERT INTO "public"."auth_permission" VALUES ('210', null, null, null, 'time_conditions_add', 'time_conditions_add', null);
INSERT INTO "public"."auth_permission" VALUES ('211', null, null, null, 'time_conditions_delete', 'time_conditions_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('212', null, null, null, 'time_conditions_edit', 'time_conditions_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('213', null, null, null, 'time_conditions_view', 'time_conditions_view', null);
INSERT INTO "public"."auth_permission" VALUES ('214', null, null, null, 'traffic_graph_view', 'traffic_graph_view', null);
INSERT INTO "public"."auth_permission" VALUES ('215', null, null, null, 'upgrade_schema', 'upgrade_schema', null);
INSERT INTO "public"."auth_permission" VALUES ('216', null, null, null, 'user_account_settings_edit', 'user_account_settings_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('217', null, null, null, 'user_account_settings_view', 'user_account_settings_view', null);
INSERT INTO "public"."auth_permission" VALUES ('218', null, null, null, 'user_add', 'user_add', null);
INSERT INTO "public"."auth_permission" VALUES ('219', null, null, null, 'user_delete', 'user_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('220', null, null, null, 'user_edit', 'user_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('221', null, null, null, 'user_view', 'user_view', null);
INSERT INTO "public"."auth_permission" VALUES ('222', null, null, null, 'variables_add', 'variables_add', null);
INSERT INTO "public"."auth_permission" VALUES ('223', null, null, null, 'variables_delete', 'variables_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('224', null, null, null, 'variables_edit', 'variables_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('225', null, null, null, 'variables_view', 'variables_view', null);
INSERT INTO "public"."auth_permission" VALUES ('226', null, null, null, 'virtual_tables_add', 'virtual_tables_add', null);
INSERT INTO "public"."auth_permission" VALUES ('227', null, null, null, 'virtual_tables_data_add', 'virtual_tables_data_add', null);
INSERT INTO "public"."auth_permission" VALUES ('228', null, null, null, 'virtual_tables_data_delete', 'virtual_tables_data_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('229', null, null, null, 'virtual_tables_data_edit', 'virtual_tables_data_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('230', null, null, null, 'virtual_tables_data_view', 'virtual_tables_data_view', null);
INSERT INTO "public"."auth_permission" VALUES ('231', null, null, null, 'virtual_tables_delete', 'virtual_tables_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('232', null, null, null, 'virtual_tables_edit', 'virtual_tables_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('233', null, null, null, 'virtual_tables_view', 'virtual_tables_view', null);
INSERT INTO "public"."auth_permission" VALUES ('234', null, null, null, 'voicemail_delete', 'voicemail_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('235', null, null, null, 'voicemail_edit', 'voicemail_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('236', null, null, null, 'voicemail_greetings_add', 'voicemail_greetings_add', null);
INSERT INTO "public"."auth_permission" VALUES ('237', null, null, null, 'voicemail_greetings_delete', 'voicemail_greetings_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('238', null, null, null, 'voicemail_greetings_download', 'voicemail_greetings_download', null);
INSERT INTO "public"."auth_permission" VALUES ('239', null, null, null, 'voicemail_greetings_edit', 'voicemail_greetings_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('240', null, null, null, 'voicemail_greetings_play', 'voicemail_greetings_play', null);
INSERT INTO "public"."auth_permission" VALUES ('241', null, null, null, 'voicemail_greetings_upload', 'voicemail_greetings_upload', null);
INSERT INTO "public"."auth_permission" VALUES ('242', null, null, null, 'voicemail_greetings_view', 'voicemail_greetings_view', null);
INSERT INTO "public"."auth_permission" VALUES ('243', null, null, null, 'voicemail_status_delete', 'voicemail_status_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('244', null, null, null, 'voicemail_status_view', 'voicemail_status_view', null);
INSERT INTO "public"."auth_permission" VALUES ('245', null, null, null, 'voicemail_view', 'voicemail_view', null);
INSERT INTO "public"."auth_permission" VALUES ('246', null, null, null, 'xml_cdr_view', 'xml_cdr_view', null);
INSERT INTO "public"."auth_permission" VALUES ('247', null, null, null, 'xml_editor_save', 'xml_editor_save', null);
INSERT INTO "public"."auth_permission" VALUES ('248', null, null, null, 'xml_editor_view', 'xml_editor_view', null);
INSERT INTO "public"."auth_permission" VALUES ('249', null, null, null, 'xmpp_add', 'xmpp_add', null);
INSERT INTO "public"."auth_permission" VALUES ('250', null, null, null, 'xmpp_delete', 'xmpp_delete', null);
INSERT INTO "public"."auth_permission" VALUES ('251', null, null, null, 'xmpp_edit', 'xmpp_edit', null);
INSERT INTO "public"."auth_permission" VALUES ('252', null, null, null, 'xmpp_view', 'xmpp_view', null);
EOD;
}

}
 
 /* End of file Account_model.php */
/* Location: ./application/models/permission_model.php */