<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Endpoint_xmpp_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_endpoint_xmpp";
	}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."pbx_endpoint_xmpp" (
"id" uuid NOT NULL primary key,
"profile_name" text,
"username" text,
"password" text,
"dialplan" text,
"context" text,
"rtp_ip" text,
"ext_rtp_ip" text,
"auto_login" text,
"sasl_type" text,
"xmpp_server" text,
"tls_enable" text,
"usr_rtp_timer" text,
"default_exten" text,
"vad" text,
"avatar" text,
"candidate_acl" text,
"local_network_acl" text,
"enabled" text,
"description" text
)
WITH (OIDS=FALSE)

EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file Account_model.php */
/* Location: ./application/models/endpoint_xmpp_model.php */