<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_config";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."pbx_config" (
"ref" uuid NOT NULL PRIMARY KEY,
"created_at" timestamp(6),
"updated_at" timestamp(6),
"updated_by" varchar(100),
"key" varchar(50) NOT NULL,
"value" varchar(255) NOT NULL
)

WITH (OIDS=FALSE)

EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}
	

}
 
 /* End of file config_model.php */
/* Location: ./application/models/config_model.php */