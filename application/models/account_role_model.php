<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_role_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_account_role";
}
public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "auth_account_role" (
"id" int8 DEFAULT nextval('auth_account_role_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"account_id" int8 NOT NULL,
"role_id" int8 NOT NULL
)
WITH (OIDS=FALSE)
EOD;

$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}

 /* End of file Account_role_model.php */
/* Location: ./application/models/account_role_model.php */