<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Endpoint_sip_template_model extends MY_Model {
 
/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function __construct() { 
	parent::__construct();
 	$this->_table = "ref_endpoint_sip_template";
}

/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_by_key($brand, $family=null, $model=null){
	
//		$this->_set_where(array("view_key" => $key));
		$this->db->where('brand', $brand);
		$this->db->where('family', $family);
		$this->db->where('model', $model);				
		return $this->get_all();

}

/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_template($brand, $family=NULL, $model=NULL){
	
		$this->db->where('brand', $brand);
		$this->db->where('family', $family);
		$this->db->where('model', $model);
		$this->db->where('is_enabled', 'true');
		$this->order_by('model', 'asc');
		return $this->get_all();
	
	}



/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function get_available(){
		$this->db->where('is_enabled', 'true');
		$this->db->where('is_hardware', 'false');
		$this->order_by('name', 'asc');
		return $this->get_all();
	}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function generate_validate_rules($id){
	
	$rules = array(
			array('field'=>'gateway', 'label'=>'Gateway', 'rules'=>'trim|required|min_length[6]'),
			array('field'=>'username', 'label'=>'Username', 'rules'=>'trim|min_length[6]'),
			array('field'=>'password', 'label'=>'Password', 'rules'=>'trim|min_length[6]'),
			array('field'=>'distinct_to', 'label'=>'"Distinct To"', 'rules'=>'trim|min_length[6]'),
			array('field'=>'auth_username', 'label'=>'Auth Username', 'rules'=>'trim|min_length[6]'),
			array('field'=>'realm', 'label'=>'Realm', 'rules'=>'trim|min_length[6]'),
			array('field'=>'from_user', 'label'=>'From User', 'rules'=>'trim|min_length[6]'),
			array('field'=>'from_domain', 'label'=>'From Domain', 'rules'=>'trim|min_length[6]'),
			array('field'=>'proxy', 'label'=>'Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'register_proxy', 'label'=>'Register Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'outbound_proxy', 'label'=>'Outbound Proxy', 'rules'=>'trim|valid_ip'),
			array('field'=>'expire_seconds', 'label'=>'Expiry Seconds', 'rules'=>'trim|integer'),
			array('field'=>'register', 'label'=>'Register', 'rules'=>'trim|min_length[6]'),
			array('field'=>'register_transport', 'label'=>'Register Transport', 'rules'=>'trim|min_length[6]'),
			array('field'=>'retry_seconds', 'label'=>'Retry Seconds', 'rules'=>'trim|valid_ip'),
			array('field'=>'extension', 'label'=>'Extension', 'rules'=>'trim|min_length[6]'),
			array('field'=>'ping', 'label'=>'Ping', 'rules'=>'trim|valid_ip'),
			array('field'=>'caller_id_in_from', 'label'=>'Caller ID in From', 'rules'=>'trim|min_length[6]'),
			array('field'=>'supress_cng', 'label'=>'Supress Cng', 'rules'=>'trim|min_length[6]'),
			array('field'=>'sip_cid_type', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'extension_in_contact', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'context', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'profile', 'label'=>'', 'rules'=>'trim|min_length[6]'),
			array('field'=>'enabled', 'label'=>'', 'rules'=>'required'),
			array('field'=>'description', 'label'=>'', 'rules'=>'trim|required|min_length[6]')
		);
}


/*******************************************************************************/
/**
 * Display a form for importing text files
 *
 * @access	public
 * @param	string
 * @return	string
 */	
public function Create_table(){
	$schema_pgsql = <<<EOD
CREATE TABLE "public"."pbx_trunk_sip" (
"id" uuid NOT NULL,
"created_at" timestamp(6) NOT NULL,
"updated_at" timestamp(6) NOT NULL,
"updated_by" varchar(100) NOT NULL,
"is_template" bool NOT NULL,
"gateway" varchar(250),
"username" varchar(250),
"password" varchar(250),
"distinct_to" varchar(250),
"auth_username" varchar(250),
"realm" varchar(250),
"from_user" varchar(250),
"from_domain" varchar(250),
"proxy" varchar(250),
"register_proxy" varchar(250),
"outbound_proxy" varchar(250),
"expire_seconds" numeric,
"register" varchar(250),
"register_transport" varchar(250),
"retry_seconds" numeric,
"extension" varchar(250),
"ping" varchar(250),
"caller_id_in_from" varchar(250),
"supress_cng" varchar(250),
"sip_cid_type" varchar(250),
"extension_in_contact" varchar(250),
"context" varchar(250),
"profile" varchar(250),
"enabled" varchar(250),
"description" varchar(250)
)
WITH (OIDS=FALSE)
EOD;


	$schema_sqllite = <<<EOD
	EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file trunk_sip_model.php */
/* Location: ./application/models/trunk_sip_model.php */