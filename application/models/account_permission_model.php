<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_permission_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_account_permission";
}
public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."auth_account_permission" (
"id" int8 DEFAULT nextval('rel_account_permission_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"account_id" int8 NOT NULL,
"permission_id" int8 NOT NULL
)
WITH (OIDS=FALSE)


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}

 /* End of file Account_permission_model.php */
/* Location: ./application/models/account_permission_model.php */