<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Endpoint_group_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_endpoint_group";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE pbx_endpoint_group (
ref uuid,
create_at timestamp,
updated_at timestamp,
updated_by varchar(100),
sip_profile_name varchar(100),
sip_profile_description varchar(250)) ;
EOD;


$schema_sqllite = <<<EOD
CREATE TABLE pbx_endpoint_group (
ref text,
create_at timestamp,
updated_at timestamp,
updated_by varchar(100),
sip_profile_name varchar(100),
sip_profile_description varchar(250));
EOD;

$default_data = <<<EOD

EOD;
	}
}
 
 /* End of file Endpoint_group_model.php */
/* Location: ./application/models/Endpoint_group_model.php */