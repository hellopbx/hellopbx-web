<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//  Dialplan Items are instructions identified by a call string and have multiple steps associated (dpitem_step)  to perform functionality
class Hunt_group_stage_model extends MY_Model {

public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_hunt_group_stage";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE pbx_hunt_group_stage (
ref uuid,
ref_hunt_group uuid,
stage numeric,
created_at uuid,
updated_at uuid,
updated_by varchar(100),
domain_uuid uuid,
ring_group_uuid uuid,
extension_uuid uuid);
EOD;


$schema_sqllite = <<<EOD
CREATE TABLE pbx_dp_item (
ref text PRIMARY KEY,
ref_dialplan text,
create_at timestamp,
updated_at timestamp,
updated_by varchar(100),
context text,
name text,
number text,
continue text,
order numeric,
enabled text,
description text);
EOD;

$default_data = <<<EOD

EOD;
	}



}
 
 /* End of file hunt_group_stage_item.php */
/* Location: ./application/models/hunt_group_stage_model.php */