<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dpitem_step_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "pbx_dpitem_step";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE pbx_dpitem_step (
ref uuid PRIMARY KEY,
ref_dpitem uuid,
create_at timestamp,
updated_at timestamp,
updated_by varchar(100),
tag text,
type text,
data text,
break text,
inline text,
group numeric,
sort_order numeric);
EOD;


$schema_sqllite = <<<EOD
CREATE TABLE pbx_dpitem_step (
ref uuid PRIMARY KEY,
ref_dpitem_uuid,
create_at timestamp,
updated_at timestamp,
updated_by varchar(100),
tag text,
type text,
data text,
break text,
inline text,
group numeric,
sort_order numeric);
EOD;

$default_data = <<<EOD
INSERT INTO "public"."pbx_dialplan_step" VALUES ('f5d56a09-e397-4971-8623-717daccf8b63', '03e3baef-be96-41dc-bc86-619812711462', 'action', 'transfer', '${hash(select/${domain_name}-last_dial/${caller_id_number})}', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8c8c73be-8fa2-4efb-b31c-f75cf8574b39', '096ea9a2-fe07-4e72-b03d-edd00e09b40c', 'action', 'transfer', '-bleg *99${digits} XML ${transfer_context}', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '0a497ee1-5175-4d65-ba0d-4cb57cf34e3b', 'action', 'bridge', 'user/${user_data(${destination_number}@${domain_name} attr id)}@${domain_name}', null, null, '0', '80', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3387a202-8514-4962-ab65-9dc5ac7d8ee5', '0b22d9c6-a2da-4d35-9273-019f7268c1fb', 'action', 'sleep', '1000', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('21249d07-23d9-4f02-9721-f1ea069f3053', '0c24e3e7-3e9f-42ab-98cf-9d9b7f18f64f', 'action', 'lua', 'wakeup.lua', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('c38c0264-d4ca-48cd-b7ea-06146d6546ee', '0d642f18-3904-4252-ba63-87a7efb1c94c', 'action', 'playback', 'tone_stream://path=${base_dir}/conf/tetris.ttml;loops=10', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', '0da238ef-ef36-4081-996f-57bb45ba8d26', 'action', 'set', 'park_announce=true', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', '0e792676-912e-4393-86d1-adf4bae6f79f', 'condition', '${sip_has_crypto}', '^(AES_CM_128_HMAC_SHA1_32|AES_CM_128_HMAC_SHA1_80)$', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('4cbfa942-09d4-483a-af15-304e43c007cd', '1011a6e0-9d37-4e58-950e-c49009c3027c', 'condition', 'destination_number', '^\*9197$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', '1390dafa-0e1d-49a4-82df-ed849f4db744', 'action', 'set', 'park_announce=true', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3664e3ce-5edb-4a62-9e9c-6cbfd7b98c03', '13ab0d05-9bf7-44b5-a067-c6202c56bf75', 'action', 'eavesdrop', '${hash(select/${domain_name}-spymap/$1)}', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('16ab6217-2678-42b6-b1ee-928b4583c06d', '1434bda2-1029-446e-a46a-9acd6974397a', 'condition', 'destination_number', '^is_transfer$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('eb0f5903-cfbb-46e6-9e33-be12813a485c', '1854dfd4-2e55-4448-923a-37374a68c743', 'condition', '${call_direction}', '^(inbound|outbound|local)$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '18777c7f-5d53-4694-b604-dc44e8a4e604', 'action', 'bind_meta_app', '2 ab s record_session::$${recordings_dir}/archive/${strftime(%Y)}/${strftime(%b)}/${strftime(%d)}/${uuid}.wav', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '190c2f44-4809-4198-abcd-6c1e6ac276f0', 'action', 'set', 'call_timeout=30', null, null, '0', '45', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('f456fd0e-b90f-40af-917f-59bddfb563b5', '1a062d79-5f56-45b9-8335-586b23d0c678', 'condition', 'destination_number', '^\*(3472)$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('c38c0264-d4ca-48cd-b7ea-06146d6546ee', '1add615a-bcae-4643-b2b7-73b6e228f20a', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('73d82710-60af-4357-b020-5ccfcd92cde5', '1c5b0c1e-7521-451a-b4b3-aef42b4e3d7e', 'action', 'set', 'extension_list=$1', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', '1d0c1752-39a6-425c-bf4b-db5fdb05064c', 'action', 'lua', 'park.lua', null, null, '0', '40', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('05aa8d71-6670-4dc5-8fca-d8241300d90f', '1d51d571-c560-4338-a442-d7b86225701f', 'action', 'read', '2 6 ''tone_stream://%(10000,0,350,440)'' digits 30000 #', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3e19e19c-063e-4419-b3a2-a9489171d87d', '1d70fd34-621d-4a3f-be42-ef0adbebed9a', 'condition', '${sip_secure_media_confirmed}', '^true$', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '1e2b172d-6a1c-4478-8576-f8dfc18f0730', 'action', 'set', 'continue_on_fail=true', null, null, '0', '55', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2d5a79ba-6c37-4469-9cb2-5aa8215f21c1', '1f8b4f6f-1c83-4b8b-875e-91a3b4879154', 'condition', 'destination_number', '^\*(732)$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('a5f44e47-1955-408f-8358-7fa7fdec8c10', '20410c08-278d-4fb5-8d88-babb39ef27ae', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ef87c228-b2d4-4b13-86e9-07eac484ce6a', '21343afa-5f1b-48cf-9736-f651daecc140', 'action', 'set', 'domain_name=default', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ab96d39f-0993-47b3-b2a8-afe545847ab5', '219174df-d632-4e41-9583-bf06c5f2d9b9', 'action', 'set', 'caller_id_number=', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('1cf3b52f-b2ae-4c9e-acd7-dc7c0050a3da', '220b4d91-ee54-4462-9da7-685a1cc83e0b', 'condition', 'destination_number', '^\*67(\d+)$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '2329463b-1e69-46d6-b363-7f1e741d892c', 'action', 'bind_meta_app', '4 b s execute_extension::att_xfer XML features', null, null, '0', '40', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('d8a7a503-f367-4a8d-b02a-9c98e8069792', '240005f8-acbf-477b-aaae-d2de5a73ce70', 'action', 'sleep', '2000', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('235c4f0f-89e3-4b56-b774-17bc59c459fe', '24952fa0-4cfb-4a98-8486-a1e35774d595', 'action', 'playback', 'ivr/ivr-hold_connect_call.wav', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', '253569c1-e103-483b-af12-3fde24928f18', 'condition', 'destination_number', '(^5901$|^5902$|^5903$)', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('73d82710-60af-4357-b020-5ccfcd92cde5', '2b08dfa2-4703-48b3-8451-e27d0b451956', 'action', 'lua', 'page.lua', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('d8a7a503-f367-4a8d-b02a-9c98e8069792', '2eeaeff3-de32-4aea-b143-59c0fc3c80bd', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('21249d07-23d9-4f02-9721-f1ea069f3053', '34b11b45-2fd8-455d-adb9-8896a3465cf0', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('e7ed819d-75a5-467b-9c59-9a5c9072909e', '35e13373-49fa-482f-8b2b-13756f6ae9cf', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('69ea4816-ee09-4f6a-b189-33ee8a651e51', '377fe21a-d009-4517-a82c-77dc43757f4c', 'action', 'export', 'dialed_extension=$1', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('0fef023c-830d-4223-bd92-9850e76e98b3', '3a96edb2-cf13-44bb-bac1-7444ff64bafc', 'action', 'lua', 'dial_string.lua', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2d5a79ba-6c37-4469-9cb2-5aa8215f21c1', '3ae808bd-d97c-4f9c-8537-c73e9f002b16', 'action', 'set', 'pin_number=74509161', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8c8c73be-8fa2-4efb-b31c-f75cf8574b39', '3b5652d8-0459-4dd5-9707-5b91ab92f0a5', 'action', 'export', 'domain_name=${transfer_context}', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8c8c73be-8fa2-4efb-b31c-f75cf8574b39', '3c2b0cb3-cd30-4039-8383-0e74464c7a24', 'action', 'set', 'origination_cancel_key=#', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('05aa8d71-6670-4dc5-8fca-d8241300d90f', '3cb5efbd-a4c4-416e-9e11-7800e6f8c2b0', 'action', 'set', 'origination_cancel_key=#', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ef87c228-b2d4-4b13-86e9-07eac484ce6a', '415615ed-bfa9-4f8c-962c-5b279fdbca49', 'condition', 'destination_number', '^0$|^operator$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('235c4f0f-89e3-4b56-b774-17bc59c459fe', '41d55002-cbbe-419a-9786-78b0925640fc', 'action', 'answer', '', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3e19e19c-063e-4419-b3a2-a9489171d87d', '424e51ac-a1ee-4b76-b243-dec741c901d3', 'action', 'sleep', '1000', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('1aa67da5-b433-4da5-92a4-fe101ce83981', '42a7536b-fbc2-4745-8fa4-a2abf7d59480', 'action', 'directory', 'default ${domain_name} ${domain_name}', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('69ea4816-ee09-4f6a-b189-33ee8a651e51', '42b7d29b-cfb2-4bfd-a0bf-1e755d108744', 'action', 'voicemail', 'default ${domain_name} ${dialed_extension}', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('235c4f0f-89e3-4b56-b774-17bc59c459fe', '43044ca3-39d0-4731-bba8-e23370177030', 'action', 'transfer', '$1 XML default', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '43e94d20-ded2-4d59-b721-816aae20fa2d', 'action', 'hash', 'insert/${domain_name}-call_return/${dialed_extension}/${caller_id_number}', null, null, '0', '60', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('4cbfa942-09d4-483a-af15-304e43c007cd', '45a9c9c6-2ea1-4863-aa11-7362221f67ab', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2b3c1387-3d15-410b-8c1b-7fc93a5ce9ff', '45fa2e8c-277d-46cc-a459-5fc210401ff2', 'action', 'set', 'dial_string=loopback/operator/default/XML', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', '47da7cb1-3ec8-4cbd-8672-f483c85d1a5d', 'action', 'set', 'park_music=$${hold_music}', null, null, '0', '35', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('21249d07-23d9-4f02-9721-f1ea069f3053', '47e64f98-8a02-416e-bdf8-d8da8e31c5f0', 'action', 'set', 'time_zone_offset=-7', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('16ab6217-2678-42b6-b1ee-928b4583c06d', '488610bc-7af5-4df2-a357-90cef6a052a7', 'action', 'transfer', '-aleg ${digits} XML default', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ab96d39f-0993-47b3-b2a8-afe545847ab5', '48a2efd3-f336-470e-829a-35951dcc6d0c', 'action', 'set', 'extension_list=1001-1003,1005', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('bfe091cb-8ba4-46df-9307-5dec0cb61a56', '49464de5-89fc-4675-9718-8eeda4a1e218', 'action', 'set', 'park_direction=out', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('d8a7a503-f367-4a8d-b02a-9c98e8069792', '49bdd368-b148-414f-b765-c95769cd8fa9', 'condition', 'destination_number', '^\*\*(\d+)$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('0e6b99bd-b31a-42ca-bc3a-05baed1e8db7', '49dd059d-09eb-4a3e-b699-74f2dc5544a4', 'action', 'sleep', '2000', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('7d01fd40-6b11-4d2d-acea-235e3022a0d2', '4b6c833d-ef65-43b6-a532-de292ad9745e', 'action', 'lua', 'dial_string.lua', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '4b950b71-034a-44d4-999a-3fcbef484a93', 'action', 'export', 'dialed_extension=$1', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('f456fd0e-b90f-40af-917f-59bddfb563b5', '4d230022-91df-4054-a00b-cdd988d1f8b3', 'action', 'lua', 'disa.lua', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('e7ed819d-75a5-467b-9c59-9a5c9072909e', '4d445ea0-98a8-4f11-bd1b-2869ac2eca80', 'condition', 'destination_number', '^dx$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2b3c1387-3d15-410b-8c1b-7fc93a5ce9ff', '4e38e7d6-9566-40da-b950-071a88855a31', 'action', 'set', 'extension=true', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('70c38661-1be7-4356-a342-61d6560cea8b', '4ef50bd9-66e4-46a3-9eae-3d5a8790b7d3', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', '4f899f21-749c-495d-a278-ec24b3b2e723', 'action', 'set', 'park_timeout_destination=1000', null, null, '0', '35', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('70c38661-1be7-4356-a342-61d6560cea8b', '50104f93-174b-4f6f-a0d0-803755b53af4', 'action', 'voicemail', 'check default ${domain_name}', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', '5098d1b6-3254-4812-a3f4-b4ba2231ec6d', 'action', 'set', 'park_timeout_destination=1000', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8c8c73be-8fa2-4efb-b31c-f75cf8574b39', '516fdc47-3d60-48f0-96ba-7552feb3e3d7', 'action', 'set', 'domain_name=${transfer_context}', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('a5f44e47-1955-408f-8358-7fa7fdec8c10', '5261c630-800f-4204-8cf3-6d407528a861', 'condition', 'destination_number', '^cf$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('420b4db6-968c-477e-8847-ed4e8637afc7', '527aab2d-2c10-4b12-b2f9-cbacd45dc33c', 'action', 'set', 'direction=in', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('7d01fd40-6b11-4d2d-acea-235e3022a0d2', '534f44ec-ae51-4cec-8bfd-e731a97e40fa', 'action', 'set', 'direction=both', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('9e3d0cac-ac62-4047-87da-ec6ce05351d7', '555afd24-664f-42f3-a9e1-6a292116233d', 'anti-action', 'eval', 'not_secure', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('73d82710-60af-4357-b020-5ccfcd92cde5', '56af1b55-d689-4697-aaa3-e18642dff209', 'action', 'set', 'mute=true', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('9e3d0cac-ac62-4047-87da-ec6ce05351d7', '56c8a671-098e-4b23-abe3-7176ad0e2c4d', 'condition', '${zrtp_secure_media_confirmed}', '^true$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '56eb0be3-d10b-47f5-bbe4-74a5e62aedaa', 'action', 'set', 'dialed_extension=$1', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('69ea4816-ee09-4f6a-b189-33ee8a651e51', '5864628d-70b4-43b2-9395-2ef3174f31a8', 'condition', 'destination_number', '^\*99(\d{2,7})$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('0e6b99bd-b31a-42ca-bc3a-05baed1e8db7', '58c5cd0a-ac8a-40fe-ab14-fdb402e08e35', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('c38c0264-d4ca-48cd-b7ea-06146d6546ee', '59c884de-9062-4feb-8011-719c46cc757c', 'condition', 'destination_number', '^\*9198$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('70c38661-1be7-4356-a342-61d6560cea8b', '59fc86bd-12c0-4a15-9f07-f745611ac232', 'action', 'sleep', '1000', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ef87c228-b2d4-4b13-86e9-07eac484ce6a', '5a69e214-f438-4209-8cf9-0872612d43ab', 'action', 'bind_meta_app', '4 ab s execute_extension::att_xfer XML features', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8f3aa313-b289-40d2-99c6-b8b732622a81', '5aab13aa-fa22-4dda-8ec3-ba9e54cdd251', 'action', 'echo', '', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('4a70a5f6-b90b-4dce-8383-4a8730f6bce0', '5b29f1d9-d6f0-4fc1-be1a-2b74f34ac4f9', 'action', 'intercept', '${hash(select/${domain_name}-last_dial/global)}', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('0fef023c-830d-4223-bd92-9850e76e98b3', '5b42f89c-854d-4462-9eee-eef128421b25', 'action', 'set', 'direction=out', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', '5c6ee6ca-f128-42e5-84ae-7651d5dd3af7', 'anti-action', 'playback', '$${hold_music}', null, null, '0', '50', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8f3aa313-b289-40d2-99c6-b8b732622a81', '61b5bb7e-2b71-427e-9d6a-c2e0461a9fce', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '6588003d-9f44-4f90-b396-5284221968d6', 'action', 'bind_meta_app', '1 b s execute_extension::dx XML features', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3664e3ce-5edb-4a62-9e9c-6cbfd7b98c03', '664e1fc6-c7ab-40ea-a78a-a25482b52a59', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', '6703fe8f-7d13-4662-9640-44e63918723a', 'action', 'voicemail', 'default ${domain_name} ${dialed_extension}', null, null, '0', '95', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('05aa8d71-6670-4dc5-8fca-d8241300d90f', '67bc3329-325f-4409-b935-1ad30b93bb94', 'action', 'set', 'domain_name=${transfer_context}', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', '68230ce3-f4c5-42f4-b3fd-fe86b059ea3e', 'action', 'set', 'park_extension=5901', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('270a6a42-b935-45db-b590-ea796c3c72a9', '68403c19-4035-42e1-ab49-2fd1248ba6fc', 'action', 'export', 'hold_music=silence', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2d5a79ba-6c37-4469-9cb2-5aa8215f21c1', '6904996b-15ff-4ccd-a6a3-f737b0815229', 'action', 'lua', 'recordings.lua', null, null, '0', '35', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('bfe091cb-8ba4-46df-9307-5dec0cb61a56', '6947ec3b-5e36-4011-aa60-771ca92b3d65', 'action', 'set', 'park_extension=$1', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('69ea4816-ee09-4f6a-b189-33ee8a651e51', '6a274bdd-7c9b-4a73-aede-1cf0e29a0583', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('f456fd0e-b90f-40af-917f-59bddfb563b5', '6b8c6520-896b-49aa-bd3f-9b6d76a76d18', 'action', 'set', 'pin_number=27527662', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ab96d39f-0993-47b3-b2a8-afe545847ab5', '6d1ca47b-f437-4af1-9cf5-f7a1720c628f', 'action', 'set', 'caller_id_name=Page', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('05aa8d71-6670-4dc5-8fca-d8241300d90f', '71f7506a-8115-4af8-a1b3-a94178a28668', 'action', 'att_xfer', 'user/${digits}@${transfer_context}', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2b3c1387-3d15-410b-8c1b-7fc93a5ce9ff', '72856aab-340b-49b3-8e25-0598fa4a4737', 'condition', 'destination_number', '^\*000$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('1cf3b52f-b2ae-4c9e-acd7-dc7c0050a3da', '73f0d7df-4d22-4247-baa6-3ae0f5d1ef29', 'action', 'set', 'privacy=yes', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('f5d56a09-e397-4971-8623-717daccf8b63', '76117dc7-15dc-4fe6-a24e-fed6b1de84c3', 'condition', 'destination_number', '^(redial|\*870)$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df2bd417-6769-4507-9fc7-faaebc5568d0', '766f750a-6f46-4385-bf97-52941efbf501', 'action', 'hash', 'insert/${domain_name}-spymap/${caller_id_number}/${uuid}', null, null, '1', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2d5a79ba-6c37-4469-9cb2-5aa8215f21c1', '77bf29e8-f193-4bff-9bff-d2240437dc6b', 'action', 'set', 'recording_prefix=recording', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('790b09f2-1cd9-4a99-ba5d-72ebe64d93d8', '792954a6-3a38-4799-801f-c8be12cccb06', 'condition', 'destination_number', '^\*9195$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('4a70a5f6-b90b-4dce-8383-4a8730f6bce0', '799293b8-eb51-4d8d-8bc9-089fa093a5c9', 'condition', 'destination_number', '^\*886$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2d5a79ba-6c37-4469-9cb2-5aa8215f21c1', '7a6ec7e5-250a-4b7e-be73-c1f80c55e4c5', 'action', 'set', 'recordings_dir=/usr/local/freeswitch/recordings', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', '7a8db2da-022b-41e3-8c78-58ce970c80bb', 'action', 'playback', '$${hold_music}', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('d8a7a503-f367-4a8d-b02a-9c98e8069792', '7c8fae53-0467-4c0f-9de5-bc37414a5a4f', 'action', 'intercept', '${hash(select/${domain_name}-last_dial_ext/$1)}', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', '7d8e653f-6fe0-4a32-9aa4-ad4652426253', 'condition', 'destination_number', '^\*9664$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df987ef4-72cb-4245-9f06-3c286e9eac5d', '7f92b338-79ad-4f0a-a239-358135366cc8', 'action', 'transfer', '${hash(select/${domain_name}-call_return/${caller_id_number})}', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3387a202-8514-4962-ab65-9dc5ac7d8ee5', '80194814-5689-48ff-a24b-3f5ece629ea5', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', '81e79a44-7d36-4175-a2ed-de1ca73ac03d', 'action', 'execute_extension', 'is_secure XML features', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3e19e19c-063e-4419-b3a2-a9489171d87d', '8433c8f8-997a-4225-b4ed-2af096fe414c', 'condition', '${sip_via_protocol}', 'tls', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2b3c1387-3d15-410b-8c1b-7fc93a5ce9ff', '8453e737-c35d-4d79-bf5f-6d4cf3c4874e', 'action', 'lua', 'dial_string.lua', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3664e3ce-5edb-4a62-9e9c-6cbfd7b98c03', '84b531f5-9169-499c-9352-7bc10dcc139d', 'condition', 'destination_number', '^\*88(\d{2,7})$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('a5f44e47-1955-408f-8358-7fa7fdec8c10', '86e39c98-ac49-4056-99dd-651938facc5f', 'action', 'transfer', '-both 30${dialed_extension:2} XML default', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('bfe091cb-8ba4-46df-9307-5dec0cb61a56', '86f9da38-b665-482f-b1cb-9b2e4b02056a', 'condition', 'destination_number', '(^5901$|^5902$|^5903$)', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', '8bcb277f-7e94-4392-b041-782bef4bf927', 'condition', 'destination_number', '^5900$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('7d01fd40-6b11-4d2d-acea-235e3022a0d2', '8de86ba6-b5c4-49eb-8dd8-045805d5200e', 'action', 'set', 'dial_string=loopback/*99{v_unique_id)/default/XML', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('f456fd0e-b90f-40af-917f-59bddfb563b5', '8fe65732-5c7b-4e41-8af7-feb94d37dc1e', 'action', 'set', 'dialplan_context=default', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('270a6a42-b935-45db-b590-ea796c3c72a9', '9077e302-55fa-41be-9072-4469fdd38dbc', 'action', 'bridge', 'sofia/${use_profile}/$1@conference.freeswitch.org', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('0e6b99bd-b31a-42ca-bc3a-05baed1e8db7', '94943371-fb2f-45e5-a599-8e68f43c2ff5', 'condition', 'destination_number', '^\*8$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df2bd417-6769-4507-9fc7-faaebc5568d0', '96a7a873-ed1a-4595-9560-85a13b7adb54', 'action', 'hash', 'insert/${domain_name}-last_dial/${caller_id_number}/${destination_number}', null, null, '1', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('0fef023c-830d-4223-bd92-9850e76e98b3', '96a7fa35-130b-4edc-b745-3fb1a1004e06', 'condition', 'destination_number', '^\*073$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df2bd417-6769-4507-9fc7-faaebc5568d0', '97cdef7b-b0e9-46e1-b5a3-226ced41ec5a', 'action', 'info', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ef87c228-b2d4-4b13-86e9-07eac484ce6a', '9a0492e6-b406-46a8-9624-15ef43ec5dec', 'action', 'bind_meta_app', '5 ab s execute_extension::xfer_vm XML features', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df2bd417-6769-4507-9fc7-faaebc5568d0', '9e017989-df8f-4812-b246-e1d585e6e246', 'condition', '', '', null, null, '1', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('1cf3b52f-b2ae-4c9e-acd7-dc7c0050a3da', '9e681a77-e7f8-492a-b09e-e73a1f09d2a7', 'action', 'privacy', 'full', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ab96d39f-0993-47b3-b2a8-afe545847ab5', '9efb5bb7-aa68-4b60-a1ec-0444ac606ce6', 'condition', 'destination_number', '^\*724$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2d5a79ba-6c37-4469-9cb2-5aa8215f21c1', '9fc9be03-853d-49f1-8b73-ea7cb7e4a8ac', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('70c38661-1be7-4356-a342-61d6560cea8b', 'a07463b2-5eed-471f-9315-88175e1b9855', 'condition', 'destination_number', '^vmain$|^\*4000$|^\*98$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2b3c1387-3d15-410b-8c1b-7fc93a5ce9ff', 'a195e3ef-583c-4b1d-908d-cc6ff46f42ec', 'action', 'set', 'direction=both', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('235c4f0f-89e3-4b56-b774-17bc59c459fe', 'a230da1f-6824-49b1-b088-dff34842c23a', 'action', 'sleep', '1500', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('1cf3b52f-b2ae-4c9e-acd7-dc7c0050a3da', 'a2d1d6e6-d42d-4d7e-bfaa-7dc061d5927c', 'action', 'transfer', '$1 XML default', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', 'a3858c62-0855-4e8f-98ba-893b1978a9c5', 'action', 'set', 'park_direction=in', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', 'a3e01cde-ed8e-44e5-8ee8-1507c45b68b6', 'action', 'set', 'park_timeout_seconds=250', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3387a202-8514-4962-ab65-9dc5ac7d8ee5', 'a5000879-7b2b-41f8-bbd6-8c155af80369', 'condition', 'destination_number', '^\*97$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', 'a631de9d-e779-4afb-9ce2-266568570d12', 'anti-action', 'playback', 'silence_stream://2000', null, null, '0', '40', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('9e3d0cac-ac62-4047-87da-ec6ce05351d7', 'a964798b-000b-496e-85fb-521b2e43fdd7', 'action', 'sleep', '1000', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3e19e19c-063e-4419-b3a2-a9489171d87d', 'a96e98e3-2d2f-4324-b011-89092257fac2', 'action', 'playback', 'misc/call_secured.wav', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('9e3d0cac-ac62-4047-87da-ec6ce05351d7', 'ac641b1a-8348-48c2-ab47-4413f3d7ec6e', 'action', 'playback', 'misc/call_secured.wav', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('235c4f0f-89e3-4b56-b774-17bc59c459fe', 'af537589-d385-40f3-9eda-a573eb1ece3d', 'action', 'set', 'transfer_ringback=$${hold_music}', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('270a6a42-b935-45db-b590-ea796c3c72a9', 'afdd4157-430d-41f8-8b4d-05e1ec8bf2a7', 'condition', 'destination_number', '^\*9(888|8888|1616|3232)$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'b09d53b9-f69c-4df2-857e-8f65bca47075', 'action', 'set', 'hangup_after_bridge=true', null, null, '0', '50', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('69ea4816-ee09-4f6a-b189-33ee8a651e51', 'b35ee88a-5cca-40c6-9acc-24769d53447f', 'action', 'set', 'dialed_extension=$1', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ef87c228-b2d4-4b13-86e9-07eac484ce6a', 'b3e82cb9-cb61-4a6e-b944-3bd96ecab1f0', 'action', 'transfer', '1001 XML default', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'b78e799d-a6c1-4811-8a48-7732beea3de1', 'condition', 'destination_number', '(^\d{2,7}$)', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', 'ba6fb271-9de6-437e-8339-afa6267ff581', 'action', 'set', 'park_range=3', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('7d01fd40-6b11-4d2d-acea-235e3022a0d2', 'bc1eb2fc-d69e-4c8e-895a-245ac496b8ca', 'action', 'set', 'extension=true', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', 'bf2ef34e-52b0-4a69-9a1b-a840d9a88ef6', 'action', 'set', 'park_music=$${hold_music}', null, null, '0', '40', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3e19e19c-063e-4419-b3a2-a9489171d87d', 'c16e22c8-4b6c-4a86-9911-bc8f321385af', 'anti-action', 'eval', 'not_secure', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('69ea4816-ee09-4f6a-b189-33ee8a651e51', 'c4447c20-d0e5-413a-84f6-343e1edcdabe', 'action', 'sleep', '1000', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ab96d39f-0993-47b3-b2a8-afe545847ab5', 'c5540431-f438-44a7-ad4b-e06296a0393c', 'action', 'lua', 'page.lua', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('4cbfa942-09d4-483a-af15-304e43c007cd', 'c5a29276-b142-42be-a5d1-d3efa84d3476', 'action', 'playback', 'tone_stream://%(251,0,1004);loops=-1', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('7d01fd40-6b11-4d2d-acea-235e3022a0d2', 'c6fac654-7d2b-4f7a-85ac-5d1040c35581', 'condition', 'destination_number', '^\*363$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', 'c7fa9474-4dd2-4fcb-8dbe-c8734196e969', 'anti-action', 'set', 'zrtp_secure_media=true', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'cbb96b30-9d8f-4f6b-a1a8-02c7712c883f', 'action', 'answer', '', null, null, '0', '85', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('4a70a5f6-b90b-4dce-8383-4a8730f6bce0', 'cd3fb455-9269-42bd-a4fc-f9bea6c69c5a', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('16ab6217-2678-42b6-b1ee-928b4583c06d', 'cd482890-81eb-48a3-a924-47284fb1faab', 'condition', '${digits}', '^(\d+)$', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('bfe091cb-8ba4-46df-9307-5dec0cb61a56', 'cd5c27ec-8ee2-4665-93b0-0b2be06501c3', 'action', 'lua', 'park.lua', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', 'cd6435f4-d24b-4533-98e5-b759c0b8d694', 'action', 'answer', '', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('e7ed819d-75a5-467b-9c59-9a5c9072909e', 'cdd33a1f-f873-49a9-a7c7-d14df40454d5', 'action', 'execute_extension', 'is_transfer XML features', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('420b4db6-968c-477e-8847-ed4e8637afc7', 'd124beec-d88d-4260-9fa5-e4b3881441b7', 'action', 'lua', 'dial_string.lua', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('420b4db6-968c-477e-8847-ed4e8637afc7', 'd1613fca-fb28-4180-bed9-69e01884c125', 'condition', 'destination_number', '^\*072$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('21249d07-23d9-4f02-9721-f1ea069f3053', 'd332e136-958e-4267-b8db-11e5d48ef5d2', 'condition', 'destination_number', '^\*(925)$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('235c4f0f-89e3-4b56-b774-17bc59c459fe', 'd36c820d-0414-482d-b674-b5c77e2c06bc', 'condition', 'destination_number', '^(10[01][0-9])$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'd4e0cf7e-821d-44a6-a20e-3807ef05e6f4', 'action', 'hash', 'insert/${domain_name}-last_dial_ext/${dialed_extension}/${uuid}', null, null, '0', '65', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df987ef4-72cb-4245-9f06-3c286e9eac5d', 'd5f4ec9e-dc74-48c3-b4f3-19d680469398', 'condition', 'destination_number', '^\*69$|^lcr$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', 'd6ce3864-6031-4d59-95b0-c1cb07728615', 'anti-action', 'answer', '', null, null, '0', '35', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8c8c73be-8fa2-4efb-b31c-f75cf8574b39', 'de99947b-15bf-4341-b17a-bfc24de9d715', 'action', 'read', '2 6 ''tone_stream://%(10000,0,350,440)'' digits 30000 #', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('21249d07-23d9-4f02-9721-f1ea069f3053', 'defa13b0-5150-49f7-b25f-170856251aa5', 'action', 'set', 'pin_number=91556556', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('e7ed819d-75a5-467b-9c59-9a5c9072909e', 'e04fae9d-86c2-4e81-9b6b-58fa184d1348', 'action', 'read', '11 11 ''tone_stream://%(10000,0,350,440)'' digits 5000 #', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8f3aa313-b289-40d2-99c6-b8b732622a81', 'e0b55aa8-0547-4e69-b38b-4d585b35320b', 'condition', 'destination_number', '^\*9196$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('f456fd0e-b90f-40af-917f-59bddfb563b5', 'e3a8d726-4eb2-4396-85f9-454e5a7b0ba8', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('3387a202-8514-4962-ab65-9dc5ac7d8ee5', 'e6ee274c-a7ff-4971-a800-656d2c558e5b', 'action', 'voicemail', 'check default ${domain_name} ${caller_id_number}', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'e76e2690-3a56-435f-82b8-513d5fda7f9a', 'action', 'bind_meta_app', '3 b s execute_extension::cf XML features', null, null, '0', '35', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df2bd417-6769-4507-9fc7-faaebc5568d0', 'e7772cd9-c378-4e1f-8cd8-8e0166e55175', 'action', 'set', 'RFC2822_DATE=${strftime(%a, %d %b %Y %T %z)}', null, null, '1', '40', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', 'e8846c61-e485-497c-9203-e3afed48c3b6', 'action', 'set', 'park_extension=$1', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('cc4c056a-a159-4456-838b-58e5bdcced77', 'e9d4308d-eb18-4717-ab43-90058bf70f81', 'action', 'set', 'park_direction=both', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df2bd417-6769-4507-9fc7-faaebc5568d0', 'ea799082-7820-4693-ae09-a0bc0c4631ab', 'action', 'hash', 'insert/${domain_name}-last_dial/global/${uuid}', null, null, '1', '35', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('eb0f5903-cfbb-46e6-9e33-be12813a485c', 'ea9274f0-a8b2-466e-9a79-4d2e5c19160d', 'anti-action', 'set', 'call_direction=local', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', 'eb0decca-55c2-4e13-a76f-1b7594071c42', 'action', 'set', 'park_timeout_seconds=70', null, null, '0', '30', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('16ab6217-2678-42b6-b1ee-928b4583c06d', 'ebe055a3-93e3-4e6f-9880-e750f09a3171', 'anti-action', 'eval', 'cancel transfer', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('1cf3b52f-b2ae-4c9e-acd7-dc7c0050a3da', 'ec59710c-54e5-4a59-adc5-ee044caaf49b', 'action', 'set', 'sip_h_Privacy=id', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('790b09f2-1cd9-4a99-ba5d-72ebe64d93d8', 'eeb4f4c1-91fc-49df-81a3-914fa516f16f', 'action', 'answer', '', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8ff7105c-f393-4ba9-b9b4-0d4cf2ed274c', 'f0f28dc8-eb8d-4209-acb8-eadcaa640620', 'anti-action', 'execute_extension', 'is_zrtp_secure XML features', null, null, '0', '45', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ab96d39f-0993-47b3-b2a8-afe545847ab5', 'f2de1550-2ecf-4de6-a197-e64f83fe6e9b', 'action', 'set', 'pin_number=', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('926b9082-b1bc-45bd-9694-74e9a7abbd43', 'f380963d-de41-4374-9156-74f523f8b2d7', 'action', 'lua', 'park.lua', null, null, '0', '45', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'f44ae968-d497-48b9-9afc-d36c6940d66a', 'action', 'set', 'called_party_call_group=${user_data(${dialed_extension}@${domain_name} var call_group)}', null, null, '0', '70', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('73d82710-60af-4357-b020-5ccfcd92cde5', 'f54d8b58-5313-4891-8033-205a0fffccb4', 'action', 'set', 'pin_number=', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'f5fb6758-5ab2-4adc-8645-859450f11ae8', 'action', 'hash', 'insert/${domain_name}-last_dial/${called_party_call_group}/${uuid}', null, null, '0', '75', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('05aa8d71-6670-4dc5-8fca-d8241300d90f', 'f60d2520-f954-419b-a577-e6a7e992bf33', 'condition', 'destination_number', '^att_xfer$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('df2bd417-6769-4507-9fc7-faaebc5568d0', 'f6f9dfaf-d8ab-489a-910e-47d5b31b73d7', 'condition', '${call_debug}', '^true$', 'never', null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('790b09f2-1cd9-4a99-ba5d-72ebe64d93d8', 'f796e38d-e5ef-4bc9-b182-c54249066267', 'action', 'delay_echo', '5000', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('ef87c228-b2d4-4b13-86e9-07eac484ce6a', 'f8b38a14-3fec-4ef0-abf3-baa0aca903dc', 'action', 'export', 'transfer_context=default', null, null, '0', '10', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'f922fcee-ce30-4b4c-92c8-66233b268be9', 'action', 'sleep', '1000', null, null, '0', '90', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('0e6b99bd-b31a-42ca-bc3a-05baed1e8db7', 'fa476f20-35e7-488d-9ce9-29e280bdfebe', 'action', 'intercept', '${hash(select/${domain_name}-last_dial/${call_group})}', null, null, '0', '15', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('73d82710-60af-4357-b020-5ccfcd92cde5', 'faaa6e0f-581e-4313-97d0-5ce6594a1fde', 'condition', 'destination_number', '^\*8(\d{2,7})$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('1aa67da5-b433-4da5-92a4-fe101ce83981', 'fc3ea98f-414d-449e-acb8-d7a5a0a63b0a', 'condition', 'destination_number', '^\*411$', null, null, '0', '5', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('159931e9-d444-432e-961e-f6f7603c8ccc', 'fcb98c0c-3645-450a-9c24-15f220684e38', 'action', 'limit', 'hash ${domain_name} $1 ${limit_max} ${limit_destination}', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('4a70a5f6-b90b-4dce-8383-4a8730f6bce0', 'fcdcf5ba-0831-4627-ba51-dbbb0fcce521', 'action', 'sleep', '2000', null, null, '0', '20', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('2d5a79ba-6c37-4469-9cb2-5aa8215f21c1', 'fd5acca4-99df-473a-b9d3-2e3abfc652de', 'action', 'set', 'recording_slots=true', null, null, '0', '25', null, null, null);
INSERT INTO "public"."pbx_dialplan_step" VALUES ('8c8c73be-8fa2-4efb-b31c-f75cf8574b39', 'ffcf6334-dd0f-4266-b91d-9652f48f724d', 'condition', 'destination_number', '^xfer_vm$', null, null, '0', '5', null, null, null);
EOD;
	}


}
 
 /* End of file Dpitem_step_model.php */
/* Location: ./application/models/Dpitem_step_model.php */