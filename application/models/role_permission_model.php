<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_permission_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_role_permission";
}

public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."auth_role_permission" (
"id" int8 DEFAULT nextval('auth_rel_role_permission_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"role_id" int8 NOT NULL,
"permission_id" int8 NOT NULL
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}
 
 /* End of file role_model.php */
/* Location: ./application/models/role_model.php */