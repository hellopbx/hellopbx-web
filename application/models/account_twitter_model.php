<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_twitter_model extends MY_Model {
 
public function __construct() { 
	parent::__construct();
 	$this->_table = "auth_account_twitter";
}
public function Create_table(){
$schema_pgsql = <<<EOD
CREATE TABLE "public"."auth_account_twitter" (
"id" int8 DEFAULT nextval('auth_account_twitter_id_seq'::regclass) NOT NULL,
"created_at" timestamp(6),
"update_at" timestamp(6),
"updated_by" varchar(30),
"account_id" int8 NOT NULL,
"twitter_id" int8 NOT NULL,
"oauth_token" varchar(80) NOT NULL,
"oauth_token_secret" varchar(80) NOT NULL,
"linkedon" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)
EOD;


$schema_sqllite = <<<EOD
EOD;

$default_data = <<<EOD
EOD;
}


}

 /* End of file Account_details_model.php */
/* Location: ./application/models/account_details_model.php */